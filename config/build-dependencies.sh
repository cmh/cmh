#!/usr/bin/env bash

TOPDIR=$(cd `dirname $0`/.. ; pwd)

# This script may be used to provide values for the variables
# PKG_INSTALLTREE PKG_SOURCETREE PKG_BUILDTREE PKG_TOPDIR
if [ -f "`dirname $0`/local.sh" ] ; then
    . "`dirname $0`/local.sh"
fi

# IMPORTANT: In order to use this script in order to provide support
# libraries for an out-of-source build, it is wise to override the
# PKG_TOPDIR (otherwise, we download and install the stuff in the source
# tree, which isn't quite appropriate).
: ${PKG_TOPDIR=$TOPDIR}
: ${PKG_INSTALLTREE=$PKG_TOPDIR/cmh-deps}
: ${PKG_SOURCETREE=$PKG_TOPDIR/cmh-deps.src}
: ${PKG_BUILDTREE=$PKG_TOPDIR/cmh-deps.build}
: ${per_pkg_install=""}

set -e
set -x

GMP_VERSION=6.0.0a
MPFR_VERSION=3.1.2
MPC_VERSION=1.0.2
MPFRCX_VERSION=0.4.2
LIBFPLLL_VERSION=4.0.4
PARI_VERSION=2.7.1

cd $TOPDIR


if ! [ -d "$PKG_INSTALLTREE" ] ; then mkdir -p "$PKG_INSTALLTREE" ; fi
if ! [ -d "$PKG_SOURCETREE" ] ; then mkdir -p "$PKG_SOURCETREE" ; fi
if ! [ -d "$PKG_BUILDTREE" ] ; then mkdir -p "$PKG_BUILDTREE" ; fi

# The default action is to rebuild everything, but with -k we keep the
# built trees.
if [ "$1" != "-k" ] ; then
    rm -rf "$PKG_BUILDTREE"
    mkdir "$PKG_BUILDTREE"
fi

# the pari building process calls tex if it happens to be available.
# This, in turn, interacts with stdin apparently. Disgusting, really.
# Should not get in our way any longer now that we do "make gp" for pari
# and not make all, but closing stdin surely can't harm.
exec </dev/null

compile() {
    target=all
    pkgname="$1" ; shift
    myprefix="$PKG_INSTALLTREE/"
    if [ "$per_pkg_install" ] ; then
	s=$pkgname
        case $s in
            gmp*) s="`basename $s a`" ;;
        esac
        myprefix="$PKG_INSTALLTREE/$s/"
    fi
    if [[ $pkgname =~ ^pari- ]] ; then
        # Pari seems to be hopelessly bound to in-source build
        rsync -a $PKG_SOURCETREE/$pkgname/ $PKG_BUILDTREE/$pkgname/
        cd $PKG_BUILDTREE/$pkgname/
        ./Configure --prefix=$myprefix/ "$@"
        target=gp
        find . -name 'Makefile' | xargs grep -l ^install-doc:: | xargs -n 1 perl -pe '/^install-doc::/ && do { print; print "\n"; print "disabled-"; };' -i
    else
        cd $PKG_BUILDTREE
        mkdir -p $pkgname
        cd $pkgname

    case $pkgname in
        gmp*) pkgname="`basename $pkgname a`" ;;
    esac
        $PKG_SOURCETREE/$pkgname/configure --prefix=$myprefix/ "$@"
    fi
    make -j `grep -c ^processor /proc/cpuinfo` $target
    pkg_base=`echo $pkgname | cut -d- -f1`
    eval "${pkg_base}_prefix=$myprefix"
    make install
    if [ -f config.log ] ; then cp -f config.log $myprefix/ ; fi
    cd $TOPDIR
}

do_autoreconf() {
    if [ -f configure.in ] || [ -f configure.ac ] ; then
        outdated_autoconf=$(find . -name Makefile.in | while read x ; do y=${x%%in}am ; if [ -e $y ] && [ $y -nt $x ] ; then echo $x  ; fi  ; done)
        if [ "$outdated_autoconf" ] ; then
            echo "Outdated autoconf files: $outdated_autoconf" >&2
            autoreconf -i >&2
        elif [ -f Makefile.in ] ; then
            if ! (find . \( -type d -a -name '.*' -a -prune \) -o \( -type f -a -newer Makefile.in -a -print \) | grep -v autom4te.cache/ | grep .) ; then
                echo "Makefile.in seems up to date, skipping autoreconf" >&2
                return
            fi
        fi
        autoreconf -i >&2
    fi
}

update() {
    cd "$1"
    if [ -d .svn ] ; then
        svn -q update >&2
        svnversion .
    elif [ -d .git ] ; then
        # git-svn trees may be updated only if we do have git-svn !
        if [ -f /usr/lib/git-core/git-svn ] ; then
            git svn rebase >&2
        fi
        # git svn find-rev HEAD
        # The following does not rely on git-svn be present.
        git log HEAD | grep git-svn-id | head -1 | cut -d@ -f2| cut -d\ -f1
    fi
    do_autoreconf
    cd $TOPDIR
}

checkout() {
    url="$1" ; shift
    name="`basename $url`"
    if ! [ -d "$name" ] ; then
        case "$url" in
            git-svn*)
                mkdir $name
                cd $name
                svnurl=${url#git-}
                git svn init --stdlayout $svnurl
                git svn fetch
                cd ..
                ;;
            svn*)
                svn co $url/trunk $name
                ;;
            *)
                echo "Bad url to check out: $url" >&2
                exit 1
                ;;
        esac
    fi
}

# Check that all sources are present
cd $PKG_SOURCETREE

unpack() {
    url="$1"
    case "$url" in
        svn://*|svn+ssh://*)    checkout "$@";;
        git-svn://*|git-svn+ssh://*)    checkout "$@";;
        *)
            archivename="`basename $url`"
            pkgname="`basename $archivename`"
            pkgname="`basename $pkgname .tar.gz`"
            pkgname="`basename $pkgname .tar.bz2`"
            pkgname="`basename $pkgname .tar.xz`"
            if ! [ -d $pkgname ] ; then
                if ! [ -f $archivename ] ; then
                    if [ "$NONET" ] ; then
                        echo "Cannot find archive $archivename (net disabled)" >&2
                        exit 1
                    fi
                    if type -p wget > /dev/null ; then
                            wget $url
                    elif type -p curl > /dev/null ; then
                            curl $url > $archivename
                    else
                        echo "Could neither find wget nor curl" >&2
                        exit 1
                    fi
                fi
                tar xf $archivename
            fi
            ;;
    esac
    pkgname="`basename $url`"
    pkgname="`basename $pkgname .tar.gz`"
    pkgname="`basename $pkgname .tar.bz2`"
    pkgname="`basename $pkgname .tar.xz`"
    case $pkgname in
        gmp*) pkgname="`basename $pkgname a`" ;;
    esac
    if ! [ -d "$pkgname" ] ; then
        echo "$pkgname not correctly unpacked" >&2
        exit 1
    fi
    cd "$pkgname"
    do_autoreconf
    case $pkgname in
        mpfrcx*)
            find . -type f | xargs grep -l __gmp_const | xargs -n 1 perl -pe 's/__gmp_const/const/g;' -i
            ;;
    esac
    cd ..
}


unpack ftp://ftp.gnu.org/pub/gnu/gmp/gmp-${GMP_VERSION}.tar.bz2
unpack http://perso.ens-lyon.fr/damien.stehle/fplll/libfplll-${LIBFPLLL_VERSION}.tar.gz
unpack http://multiprecision.org/mpfrcx/download/mpfrcx-${MPFRCX_VERSION}.tar.gz
# unpack git-svn://scm.gforge.inria.fr/svn/mpfrcx
unpack ftp://ftp.gnu.org/pub/gnu/mpfr/mpfr-${MPFR_VERSION}.tar.gz
unpack ftp://ftp.gnu.org/pub/gnu/mpc/mpc-${MPC_VERSION}.tar.gz
# unpack svn://scm.gforge.inria.fr/svn/mpfr
# unpack svn://scm.gforge.inria.fr/svn/mpc
# unpack svn://scm.gforge.inria.fr/svn/ecm
unpack http://pari.math.u-bordeaux.fr/pub/pari/unix/pari-${PARI_VERSION}.tar.gz

cd $TOPDIR

compile gmp-${GMP_VERSION}               \
    --enable-cxx

# rev_mpfr=`update $PKG_SOURCETREE/mpfr`
compile mpfr-${MPFR_VERSION}             \
    --with-gmp=$gmp_prefix

# rev_mpc=`update $PKG_SOURCETREE/mpc`
compile mpc-${MPC_VERSION}               \
    --with-gmp=$gmp_prefix         \
    --with-mpfr=$mpfr_prefix

# rev_mpfrcx=`update $TOPDIR/soft/mpfrcx`
compile mpfrcx-${MPFRCX_VERSION}                  \
    --with-gmp=$gmp_prefix      \
    --with-mpfr=$mpfr_prefix    \
    --with-mpc=$mpc_prefix

export LDFLAGS=-L$PKG_INSTALLTREE/lib
compile libfplll-${LIBFPLLL_VERSION}         \
    --with-gmp=$gmp_prefix      \
    --with-mpfr=$mpfr_prefix    \

# rev_ecm=`update $TOPDIR/soft/ecm`
# compile ecm                     \
    # --with-gmp=$PKG_INSTALLTREE/

compile pari-${PARI_VERSION}    \
    --with-gmp=$gmp_prefix

# echo off...
set +x

echo -n "Arguments to be passed to configure:"
echo -n " --with-gmp=$gmp_prefix"
echo -n " --with-mpfr=$mpfr_prefix"
echo -n " --with-mpc=$mpc_prefix"
echo -n " --with-mpfrcx=$mpfrcx_prefix"
echo -n " --with-fplll=$fplll_prefix"
echo -n " --with-pari=$pari_prefix"
echo
