#!/usr/bin/env bash

set -e

ver="$1"
if ! [ "$1" ] ; then
    echo "Usage: $0 <version>" >&2
    exit 1
fi
if ! git diff-index --quiet HEAD ; then
    echo "Working directory has uncommitted files" >&2
    git diff-index HEAD >&2
    exit 1
fi
"`dirname $0`"/cleanup.sh
"`dirname $0`"/autogen.sh
dd=$(mktemp -d /tmp/XXXXXXXXXXXXXX)
git archive --format=tar --prefix=cmh-$ver/ HEAD | (cd $dd ; tar xf -)
rsync --files-from=- ./ $dd/cmh-$ver/ <<EOF
Makefile.in
aclocal.m4
config.h.in
config/ar-lib
config/config.guess
config/config.sub
config/depcomp
config/install-sh
config/ltmain.sh
config/m4/libtool.m4
config/m4/ltoptions.m4
config/m4/ltsugar.m4
config/m4/ltversion.m4
config/m4/lt~obsolete.m4
config/missing
configure
src/Makefile.in
config/compile
scripts/Makefile.in
EOF
echo "cmh_long_version=\"$ver ($(git rev-parse --short HEAD))\"" > $dd/cmh-$ver/version_stamp.sh
(cd $dd ; tar cf - cmh-$ver) | gzip -9 > cmh-$ver.tar.gz
rm -rf $dd
echo "Created cmh-$ver.tar.gz"
