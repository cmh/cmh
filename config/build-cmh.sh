#!/usr/bin/env bash

# This script is responsible of handing over the build process, in a
# proper out of source build directory. It takes care of calling the
# autotools first if needed, and then cd's into the proper sub-directory
# of the build tree, and runs make there. The intent is that this script
# is called from a Makefile within the source tree (see the accompanying
# GNUmakefile.example in the same directory).

# The location of the build tree is, by default,
# $source_tree/build/`hostname`, but customizing it is easy.

up_path="${0%%config/build-cmh.sh}"
if [ "$up_path" = "$0" ] ; then
    echo "Error: build-cmh.sh must be called with a path ending in config/build-cmh.sh" >&2
    exit 1
elif [ "$up_path" = "" ] ; then
    up_path=./
fi
called_from="`pwd`"
absolute_path_of_source="`cd "$up_path" ; pwd`"
relative_path_of_cwd="${called_from##$absolute_path_of_source}"

########################################################################
# Set some default variables which can be overridden from config/local.sh

# The default behaviour
: ${build_tree:="${up_path}build/`hostname`"}
# Joe gets confused by " in prev line; this line fixes it

# By default, we also avoid /usr/local ; of course, it may be overridden.
# Note that cmake does not really have phony targets, so one must avoid
# basenames which correspond to targets !
## : ${PREFIX:="$absolute_path_of_source/installed"}
: ${PREFIX:=/tmp/cmh}

# It is possible to overwrite this from local.sh, and add flags.
call_this_configure_script() { "$@" ; }

# XXX XXX XXX LOOK LOOK LOOK: here you've got an entry point for customizing.
# The source directory may contain a hint script with several useful
# preferences. Its job is to put environment variables. Quite notably,
# $build_tree is amongst these.
if [ -f "${up_path}config/local.sh" ] ; then
    . "${up_path}config/local.sh"
fi

# If no CFLAGS have been set yet, set something sensible: get optimization by
# default, as well as asserts.  If you want to disable this, use either
# config/local.sh or the environment to set an environment variable CFLAGS to be
# something non-empty, like CFLAGS=-g or CFLAGS=-O0 ; Simply having CFLAGS=
# <nothing> won't do, because bash makes no distinction between null and unset
# here.
: ${CFLAGS:=-O2}
: ${CXXFLAGS:=-O2}
: ${PTHREADS:=1}

########################################################################
# Arrange so that relevant stuff is passed to cmake -- the other end of
# the magic is in CMakeLists.txt. The two lists must agree.
# (no, it's not as simple. As long as the cmake checks care about
# *environment variables*, we are here at the right place for setting
# them. However, we might also be interested in having cmake export test
# results to scripts. This is done by cmake substitutions, but the
# corresponding names need not match the ones below).

export CC
export CXX
export CFLAGS
export CXXFLAGS
export LDFLAGS
export MPI

if [ "$1" = "tidy" ] ; then
    echo "Wiping out $build_tree"
    rm -rf "$build_tree"
    exit 0
fi

if [ "$1" = "show" ] ; then
    echo "build_tree=\"$build_tree\""
    echo "up_path=\"$up_path\""
    echo "called_from=\"$called_from\""
    echo "absolute_path_of_source=\"$absolute_path_of_source\""
    echo "relative_path_of_cwd=\"$relative_path_of_cwd\""
    echo "CC=\"$CC\""
    echo "CXX=\"$CXX\""
    echo "CFLAGS=\"$CFLAGS\""
    echo "CXXFLAGS=\"$CXXFLAGS\""
    echo "LDFLAGS=\"$CFLAGS\""
    echo "MPI=\"$MPI\""
    exit 0
fi

if [ "$1" = "conf" ] || ! [ -d "$build_tree" ] ; then
    mkdir -p "$build_tree"
    (cd "$build_tree" ;
    # Here we're tinkering with the source tree, it's evil
    if ! [ -x "$absolute_path_of_source/configure" ] ; then
        (cd "$absolute_path_of_source" ; ./config/autogen.sh)
    fi
    call_this_configure_script "$absolute_path_of_source/configure")
fi

if [ "$1" = "conf" ] ; then
    exit 0
fi

# Now cd into the target directory, and build everything required.
# Note that it's useful to kill MAKELEVEL, or otherwise we'll get scores
# and scores of ``Entering directory'' messages (sure, there's the
# --no-print-directory option -- but it's not the right cure here).
# env | grep -i make
unset MAKELEVEL
(cd "$build_tree$relative_path_of_cwd" ; make "$@")
