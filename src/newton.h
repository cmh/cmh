#ifndef NEWTON_H_
#define NEWTON_H_

/* newton.h -- headers for newton.c
 *
 * Copyright (C) 2006, 2010, 2011, 2012, 2013 INRIA
 *
 * This file is part of CMH.
 *
 * CMH is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * CMH is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/ .
 */

#ifdef __cplusplus
extern "C" {
#endif

/* method == 0: use finite differences
 * method == 1: use converging sequences of derivatives
 */
void newtonstep_3thetaq (mpc_t *res, mpc_t *r, mpc_t *tau, int method);

#ifdef __cplusplus
}
#endif

#endif
