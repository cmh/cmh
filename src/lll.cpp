/* lll.cpp -- encapsulate LLL taken from external libraries

   Copyright (C) 2019, 2020 INRIA

   This file is part of CMH.

   CMH is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   CMH is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE. See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License
   along with this program. If not, see http://www.gnu.org/licenses/ .
*/

#include "config.h"
#include <mpfr.h>
#if defined HAVE_FPLLL_4 || defined HAVE_FPLLL_5
#define HAVE_FPLLL
#include <fplll.h>
#ifdef HAVE_FPLLL_4
#include <fplll/matrix.h>
#else
#include <fplll/nr/matrix.h>
#endif
#else
#include <pari/pari.h>
#include "parsepari.h"
#endif
#include "lll.h"

#ifdef HAVE_FPLLL
static void lll_fplll (mpz_t *v, mpz_t **M, const int m, const int n)
{
   fplll::ZZ_mat <mpz_t> Mp (m, n);
   int i, j;

   for (i = 0; i < m; i++)
      for (j = 0; j < n; j++)
#ifdef HAVE_FPLLL_4
         Mp [i][j].set (M [i][j]);
#else
         Mp [i][j] = M [i][j];
#endif

#ifdef HAVE_FPLLL_4
   lllReduction (Mp);
#else
   lll_reduction (Mp);
#endif

   for (i = 0; i < n; i++)
#ifdef HAVE_FPLLL_4
      mpz_set (v [i], Mp [0][i].GetData ());
#else
      mpz_set (v [i], Mp [0][i].get_data ());
#endif
}

#else
static void lll_pari (mpz_t *v, mpz_t **M, const int m, const int n)
{
   pari_sp av;
   GEN Mp;
   int i, j;

   pari_init (4000000, 0);
   paristack_setsize (4000000, 500000000);
   av = avma;

   /* Transpose the matrix for PARI. */
   Mp = cgetg (n+1, t_MAT);
   for (i = 1; i <= n; i++) {
      gel (Mp, i) = cgetg (m+1, t_COL);
      for (j = 1; j <= m; j++)
         gel (gel (Mp, i), j) = mpz_get_GEN (M [i-1][j-1]);
   }
   GEN vect = gel (ZM_lll (Mp, 0.99, LLL_INPLACE), 1);
   for (i = 0; i < n; i++)
      mpz_set_GEN (v [i], gel (vect, i+1));

   avma = av;
   pari_close ();
}
#endif


void cmh_lll (mpz_t *v, mpz_t **M, const int m, const int n)
   /* Run LLL on the row matrix M with m rows and n columns and return
      the first element of the resulting basis in v, which must already
      be initialised. */
{
#ifdef HAVE_FPLLL
   lll_fplll (v, M, m, n);
#else
   lll_pari (v, M, m, n);
#endif
}

