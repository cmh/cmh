/* jinvariants.c -- computation of invariants from theta constants
 *
 * Copyright (C) 2006, 2011, 2012 INRIA
 *
 * This file is part of CMH.
 *
 * CMH is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * CMH is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/ .
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <mpc.h>
#include <time.h>

#include "params.h"
#include "macros.h"
#include "jinvariants.h"

static void product_of_six(mpc_t v, mpc_t * t, int i0, int i1, int i2, int i3, int i4, int i5)
{
    cset_ui(v, 1);
    cmul(v, v, t[i0]);
    cmul(v, v, t[i1]);
    cmul(v, v, t[i2]);
    cmul(v, v, t[i3]);
    cmul(v, v, t[i4]);
    cmul(v, v, t[i5]);
}

static void sum_of_four(mpc_t v, mpc_t * t, int i0, int i1, int i2, int i3)
{
    cset_ui(v, 0);
    cadd(v, v, t[i0]);
    cadd(v, v, t[i1]);
    cadd(v, v, t[i2]);
    cadd(v, v, t[i3]);
}

/* {{{ compute h12 and h16 by enumerating all Göpel quadruples */
static void h12h16_from_th4th8(mpc_t * h, mpc_t * th4, mpc_t * th8)
{
    mpc_ptr h12 = h[0];
    mpc_ptr h16 = h[1];

    mpc_t v[15];
    int prec = cprec(h12);
    mpc_t aux;

    cinit(aux, prec);

    for (int i=0; i<15; i++)
	cinit(v[i], prec);

    cset_ui(h12, 0);
    cset_ui(h16, 0);

    product_of_six(v[0], th4, 0, 1, 4, 5, 8, 9);
    sum_of_four(aux, th8, 2, 3, 6, 7);
    cadd(h12, h12, v[0]);
    cmul(aux, aux, v[0]);
    cadd(h16, h16, aux);

    product_of_six(v[1], th4, 0, 1, 2, 5, 7, 8); 
    sum_of_four(aux, th8, 3, 4, 6, 9);
    cadd(h12, h12, v[1]);
    cmul(aux, aux, v[1]);
    cadd(h16, h16, aux);

    product_of_six(v[2], th4, 4, 5, 6, 7, 8, 9);
    sum_of_four(aux, th8, 0, 1, 2, 3);
    cadd(h12, h12, v[2]);
    cmul(aux, aux, v[2]);
    cadd(h16, h16, aux);

    product_of_six(v[3], th4, 1, 2, 4, 5, 6, 7);
    sum_of_four(aux, th8, 0, 3, 8, 9);
    cadd(h12, h12, v[3]);
    cmul(aux, aux, v[3]);
    cadd(h16, h16, aux);

    product_of_six(v[4], th4, 0, 1, 2, 4, 6, 9);
    sum_of_four(aux, th8, 3, 5, 7, 8);
    cadd(h12, h12, v[4]);
    cmul(aux, aux, v[4]);
    cadd(h16, h16, aux);

    product_of_six(v[5], th4, 0, 2, 6, 7, 8, 9);
    sum_of_four(aux, th8, 1, 3, 4, 5);
    cadd(h12, h12, v[5]);
    cmul(aux, aux, v[5]);
    cadd(h16, h16, aux);

    product_of_six(v[6], th4, 0, 1, 3, 4, 7, 9);
    sum_of_four(aux, th8, 2, 5, 6, 8);
    cadd(h12, h12, v[6]);
    cmul(aux, aux, v[6]);
    cadd(h16, h16, aux);

    product_of_six(v[7], th4, 0, 2, 3, 4, 7, 8);
    sum_of_four(aux, th8, 1, 5, 6, 9);
    cadd(h12, h12, v[7]);
    cmul(aux, aux, v[7]);
    cadd(h16, h16, aux);

    product_of_six(v[8], th4, 2, 3, 4, 5, 8, 9);
    sum_of_four(aux, th8, 0, 1, 6, 7);
    cadd(h12, h12, v[8]);
    cmul(aux, aux, v[8]);
    cadd(h16, h16, aux);

    product_of_six(v[9], th4, 1, 2, 3, 5, 7, 9);
    sum_of_four(aux, th8, 0, 4, 6, 8);
    cadd(h12, h12, v[9]);
    cmul(aux, aux, v[9]);
    cadd(h16, h16, aux);

    product_of_six(v[10], th4, 0, 1, 3, 5, 6, 8);
    sum_of_four(aux, th8, 2, 4, 7, 9);
    cadd(h12, h12, v[10]);
    cmul(aux, aux, v[10]);
    cadd(h16, h16, aux);

    product_of_six(v[11], th4, 0, 3, 4, 5, 6, 7);
    sum_of_four(aux, th8, 1, 2, 8, 9);
    cadd(h12, h12, v[11]);
    cmul(aux, aux, v[11]);
    cadd(h16, h16, aux);

    product_of_six(v[12], th4, 1, 3, 6, 7, 8, 9);
    sum_of_four(aux, th8, 0, 2, 4, 5);
    cadd(h12, h12, v[12]);
    cmul(aux, aux, v[12]);
    cadd(h16, h16, aux);

    product_of_six(v[13], th4, 1, 2, 3, 4, 6, 8);
    sum_of_four(aux, th8, 0, 5, 7, 9);
    cadd(h12, h12, v[13]);
    cmul(aux, aux, v[13]);
    cadd(h16, h16, aux);

    product_of_six(v[14], th4, 0, 2, 3, 5, 6, 9);
    sum_of_four(aux, th8, 1, 4, 7, 8);
    cadd(h12, h12, v[14]);
    cmul(aux, aux, v[14]);
    cadd(h16, h16, aux);

    for (int i=0; i<15; i++) cclear(v[i]);
    cclear(aux);
}
/* }}} */

void h4h10h12h16_from_th2(mpc_t * h, mpc_t * th2)/*{{{*/
{
    mpc_ptr h4 = h[0];
    mpc_ptr h10 = h[1];
    // mpc_ptr h12 = h[2];
    // mpc_ptr h16 = h[3];

    mpc_t th4[10], th8[10];
    int prec = cprec(h4);

    for (int i=0; i<10; i++)
    {
	cinit(th4[i], prec);
	cinit(th8[i], prec);
    }
    for (int i=0; i<10; i++) {
	csqr(th4[i], th2[i]);
	csqr(th8[i], th4[i]);
    }

    // sum of ^8
    cset_ui(h4, 0);
    for (int i=0; i<10; i++)
	cadd(h4, h4, th8[i]);

    // product of ^2
    cset_ui(h10, 1);
    for (int i=0; i<10; i++)
	cmul(h10, h10, th2[i]);

    h12h16_from_th4th8(h + 2, th4, th8);

    for (int i=0; i<10; i++)
    {
	cclear(th4[i]);
	cclear(th8[i]);
    }
}/*}}}*/

void I2I4I6I10_from_h4h10h12h16(mpc_t * I, mpc_t * h)/*{{{*/
{
    mpc_ptr h4 = h[0];
    mpc_ptr h10 = h[1];
    mpc_ptr h12 = h[2];
    mpc_ptr h16 = h[3];
    mpc_ptr I2 = I[0];
    mpc_ptr I4 = I[1];
    mpc_ptr I6 = I[2];
    mpc_ptr I10 = I[3];

    int prec = cprec(I2);

    mpc_t aux;
    cinit(aux, prec);

    cinv(aux, h10);
    cmul(I2, h12, aux);         // I2 == h12/h10
    cset(I4, h4);               // I4 == h4
    cmul(I6, h16, aux);         // I16 = h16 / h10
    cset(I10, h10);             // I10 = h10

    cclear(aux);
}/*}}}*/

void gorenlauter_i1i2i3_from_I2I4I6I10(mpc_t * absolute_invariants, mpc_t * I)
{
    mpc_ptr i1 = absolute_invariants[0];
    mpc_ptr i2 = absolute_invariants[1];
    mpc_ptr i3 = absolute_invariants[2];
    mpc_ptr I2 = I[0];
    mpc_ptr I4 = I[1];
    mpc_ptr I6 = I[2];
    mpc_ptr I10 = I[3];

    int prec = cprec(I2);       // not i1 !!!

    mpc_t aux;
    mpc_t I10_inv;
    mpc_t I2_2;
    mpc_t I2_3;

    cinit(aux, prec);
    cinit(I10_inv, prec);
    cinit(I2_2, prec);
    cinit(I2_3, prec);

    cinv(I10_inv, I10);
    csqr(I2_2, I2);                     
    cmul(I2_3, I2_2, I2);

    cmul(aux, I2_3, I2_2);
    cmul(i1, aux, I10_inv);    // jinv[0] == I2^5/I10 == i1 in streng notes

    cmul(aux, I2_3, I4);
    cmul(i2, aux, I10_inv);    // jinv[1] == I2^3*I4/I10 == i2 in streng notes

    cmul(aux, I6, I2_2);
    cmul(i3, aux, I10_inv);    // jinv[2] == I2^2*I6/I10 == i3 in streng notes

    cclear(aux);
    cclear(I2_3);
    cclear(I2_2);
    cclear(I10_inv);
}

void kohel_j1j2j3_from_I2I4I6I10(mpc_t * absolute_invariants, mpc_t * I)
{
    mpc_ptr j1 = absolute_invariants[0];
    mpc_ptr j2 = absolute_invariants[1];
    mpc_ptr j3 = absolute_invariants[2];
    mpc_ptr I2 = I[0];
    mpc_ptr I4 = I[1];
    mpc_ptr I6 = I[2];
    mpc_ptr I10 = I[3];

    int prec = cprec(I2);       // not i1 !!!

    mpc_t aux;
    mpc_t I10_inv;
    mpc_t I2_2;
    mpc_t I2_3;

    cinit(aux, prec);
    cinit(I10_inv, prec);
    cinit(I2_2, prec);
    cinit(I2_3, prec);

    cinv(I10_inv, I10);
    csqr(I2_2, I2);                     
    cmul(I2_3, I2_2, I2);

    cmul(aux, I4, I10_inv);
    cmul(j2, aux, I2_3);

    cmul(j1, aux, I6);

    cmul(aux, I6, I10_inv);
    cmul(j3, aux, I2_2);

    cclear(aux);
    cclear(I2_3);
    cclear(I2_2);
    cclear(I10_inv);
}

void streng_i1i2i3_from_I2I4I6I10(mpc_t * absolute_invariants, mpc_t * I)
{
    mpc_ptr i1 = absolute_invariants[0];
    mpc_ptr i2 = absolute_invariants[1];
    mpc_ptr i3 = absolute_invariants[2];
    mpc_ptr I2 = I[0];
    mpc_ptr I4 = I[1];
    mpc_ptr I6 = I[2];
    mpc_ptr I10 = I[3];

            // old-Streng -2^10I4I6'/I10, 2^18I4^5/I10^2,2^28/3I2I4^2/I10
            // Streng I4I6'/I10, I2I4^2/I10, I4^5/I10^2
            //
    int prec = cprec(I2);       // not i1 !!!

    mpc_t aux, aux2, I4_div_I10;

    cinit(aux, prec);
    cinit(aux2, prec);
    cinit(I4_div_I10, prec);

    cinv(I4_div_I10, I10);
    cmul(I4_div_I10, I4_div_I10, I4);

    cmul_ui(aux, I6, 3);
    cmul(aux2, I2, I4);
    csub(aux, aux2, aux);
    cdiv_2ui(aux, aux, 1);
    // aux = I6' = (I2I4-3I6)/2

    // i1 = I4I6'/I10 = I6' * I4/I10
    cmul(i1, aux, I4_div_I10);

    // i2 = I2I4^2/I10 = I2I4 * I4/I10
    cmul(i2, aux2, I4_div_I10);

    // i3 = I4^5/I10^2 = I4^2 * I4 * (I4/I10)^2
    csqr(aux, I4);
    cmul(aux, aux, I4);
    csqr(aux2, I4_div_I10);
    cmul(i3, aux, aux2);

    cclear(aux);
    cclear(aux2);
    cclear(I4_div_I10);
}

void invariants_from_th2(mpc_t * jinv, mpc_t * th2, int invariant_set)
{
    mpc_t h[4];
    mpc_t I[4];

    int prec = cprec(jinv[0])+3*SEC_MARGIN;

    for(int i = 0 ; i < 4 ; i++) cinit(h[i], prec);
    for(int i = 0 ; i < 4 ; i++) cinit(I[i], prec);

    // since most invariants are computed from I, factor it out. We could
    // also make it appear in the switch statement, of course.
    h4h10h12h16_from_th2(h, th2);
    I2I4I6I10_from_h4h10h12h16(I, h);

    switch(invariant_set) {
        case 0: // Goren-Lauter / van Wamelen (I2^5/I10,I2^3I4/I10,I2^2I6/I10)
            gorenlauter_i1i2i3_from_I2I4I6I10(jinv, I);
            break;
        case 1: // Kohel I4I6/I10,I2^3I4/I10,I2^2I6/I10
            kohel_j1j2j3_from_I2I4I6I10(jinv, I);
            break;
        case 2:
            // old-Streng -2^10I4I6'/I10, 2^18I4^5/I10^2,2^28/3I2I4^2/I10
            // Streng I4I6'/I10, I2I4^2/I10, I4^5/I10^2
            // ( I6' = (I2I4-3I6)/2)
            streng_i1i2i3_from_I2I4I6I10(jinv, I);
            break;
        default:
            fprintf(stderr, "No such invariant set defined !!!\n");
            abort();
    }

    for(int i = 0 ; i < 4 ; i++) cclear(h[i]);
    for(int i = 0 ; i < 4 ; i++) cclear(I[i]);

    return;
}

