#ifndef CPUTIME_H_
#define CPUTIME_H_

/* cputime.h -- headers for cputime.c
 *
 * Copyright (C) 2010, 2011 INRIA
 *
 * This file is part of CMH.
 *
 * CMH is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * CMH is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/ .
 */

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

uint64_t microseconds();
double cputime();
double hires_wct();


#ifdef __cplusplus
}
#endif

#endif	/* CPUTIME_H_ */
