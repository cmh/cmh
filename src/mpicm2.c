/* mpicm2.c -- mpi-level functions for cm2.c
 *
 * Copyright (C) 2010, 2011, 2012, 2013 INRIA
 *
 * This file is part of CMH.
 *
 * CMH is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * CMH is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/ .
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <stdint.h>
#include <string.h>

#include "macros.h"
#include "cm2.h"
#include "mpicm2.h"
#include "lprintf.h"
#include "cputime.h"
#include "mpfrx-mpi.h"

struct mpi_comms pals[1];

void mpi_custom_types_check()
{
    int ret;
    /* If this ever fails, we have to do something to the defines above
     * (and maybe touch the cmake files) */
    MPI_Type_size(MPI_MY_SIZE_T,&ret);   ASSERT_ALWAYS(ret==sizeof(size_t));
    MPI_Type_size(MPI_MY_MP_SIZE_T,&ret);ASSERT_ALWAYS(ret==sizeof(mp_size_t));
    MPI_Type_size(MPI_MY_MP_LIMB_T,&ret);ASSERT_ALWAYS(ret==sizeof(mp_limb_t));
    MPI_Type_size(MPI_MY_UINT64_T,&ret);ASSERT_ALWAYS(ret==sizeof(uint64_t));
    MPI_Type_size(MPI_MY_INT64_T,&ret);ASSERT_ALWAYS(ret==sizeof(int64_t));
    {
        mpz_t z;
        MPI_Type_size(MPI_MY_GMP_INTERNAL_SIZE_FIELD_T,&ret);
        ASSERT_ALWAYS(ret==sizeof(z->_mp_size));
    }
}


/* This routine splits the input communicator into a star-like set of
 * communicators, where all nodes except the root node belong to exactly
 * one communicator of size 2, where the peer is always the root node
 * (which therefore belongs to all created communicators).
 * The complexity of this function should be O(n\log n), where n is the
 * size of the input communicator.
 * The return value is the number of created communicators on the current
 * node, which is n-1 for rank 0 (root node), and 1 for all other nodes.
 *
 * XXX I am pretty confident that this code is correct. I have stared at
 * it for a while, while getting mpi segv's at the same time in the
 * roundabouts of this. My conclusion is that ubuntu's openmpi package
 * 1.4.3-2.1ubuntu3 is badly broken. I'm upgrading to 1.5 so that the
 * problem disappears.
 */
static int split_comms(MPI_Comm orig, MPI_Comm * dst, int offset)
{
    int size,rank,color;
    MPI_Comm * dst0 = dst;
    MPI_Comm tmp;
    MPI_Comm_size(orig, &size);
    MPI_Comm_rank(orig, &rank);
    if (rank == 0) {
        lprintf(LOG_PROGRESS "split_comms on size %d, range [%d..%d[\n",
                size, offset, offset+size-1);
    }
    if (size <= 4) {
        for(int i = 1 ; i < size ; i++) {
            MPI_Comm * newcomm = & tmp;
            color = i == rank || rank == 0;
            if (color) newcomm = dst++;
            MPI_Comm_split(orig, color ? 1 : MPI_UNDEFINED, rank, newcomm);
        }
    } else {
        color = !rank || (rank-1) < (size-1) / 2;
        MPI_Comm_split(orig, color ? 1 : MPI_UNDEFINED, rank, &tmp);
        if (color) {
            dst += split_comms(tmp, dst, offset + (dst - dst0));
            MPI_Comm_free(&tmp);
        }

        color = !rank || (rank-1) >= (size-1) / 2;
        MPI_Comm_split(orig, color ? 1 : MPI_UNDEFINED, rank, &tmp);
        if (color) {
            dst += split_comms(tmp, dst, offset + (dst - dst0));
            MPI_Comm_free(&tmp);
        }
    }
    return dst - dst0;
}

void mpi_comms_init(struct mpi_comms * m)
{
    int size;
    int rank;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    m->c = malloc(size * sizeof(MPI_Comm));
    memset(m->c, 0, size * sizeof(MPI_Comm));
    split_comms(MPI_COMM_WORLD, m->c + rank + (rank == 0), 0);
    m->up = &(m->c[rank]);
}


void mpi_comms_clear(struct mpi_comms * m)
{
    int size;
    int rank;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    if (rank == 0) {
        for(int i = 1 ; i < size ; i++) {
            MPI_Comm_free(&(m->c[i]));
        }
    } else {
        MPI_Comm_free(&(m->c[rank]));
    }
    free(m->c);
}



/***** broadcast functions *****/

static void broadcast_mpz(mpz_ptr z, int root, MPI_Comm comm) /*{{{*/
{
    // that's much, much uglier than it should.
    int k;
    MPI_Comm_rank(comm, &k);
    // that's much, much uglier than it should.
    mp_size_t nlimbs = mpz_size(z);
    MPI_Bcast(&nlimbs, 1, MPI_MY_MP_SIZE_T, root, comm);
    if (k != root) _mpz_realloc(z, nlimbs);
    if (nlimbs) {
        MPI_Bcast(z->_mp_d, nlimbs, MPI_MY_MP_LIMB_T, root, comm);
    }
    MPI_Bcast(&z->_mp_size, 1, MPI_MY_GMP_INTERNAL_SIZE_FIELD_T, root, comm);
}/*}}}*/


void broadcast_int (int *x, int root, MPI_Comm comm)
{
    int v = *x;
    MPI_Bcast(&v, 1, MPI_INT, root, comm);
    *x  = v;
}


void broadcast_mp_prec(mpfr_prec_t * x, int root, MPI_Comm comm)
{
    long v = *x;
    MPI_Bcast(&v, 1, MPI_LONG, root, comm);
    *x  = v;
}


static void broadcast_mpfr(mpfr_ptr x, int root, MPI_Comm comm)
{
    assert(sizeof(mpfr_prec_t) <= sizeof(long));
    assert(sizeof(mpfr_sign_t) <= sizeof(long));
    assert(sizeof(mpfr_exp_t) <= sizeof(long));
    long z;
    z = MPFR_PREC(x); MPI_Bcast(&z, 1, MPI_LONG, root, comm);
    size_t mysize = MPFR_GET_ALLOC_SIZE(x);
    if ((int) mysize < ((z-1)/GMP_NUMB_BITS + 1))
        mpfr_set_prec(x, z);
    MPFR_PREC(x)=z;
    z = MPFR_EXP(x); MPI_Bcast(&z, 1, MPI_LONG, root, comm); MPFR_EXP(x)=z;
    z = MPFR_SIGN(x); MPI_Bcast(&z, 1, MPI_LONG, root, comm); MPFR_SIGN(x)=z;
    if (MPFR_LIMB_SIZE(x)) {
        MPI_Bcast(MPFR_MANT(x), MPFR_LIMB_SIZE(x), MPI_MY_MP_LIMB_T, root, comm);
    }
}

static void broadcast_mpc(mpc_ptr x, int root, MPI_Comm comm) /*{{{*/
{
    broadcast_mpfr(mpc_realref(x), root, comm);
    broadcast_mpfr(mpc_imagref(x), root, comm);
}/*}}}*/


void broadcast_cm_data_partial(struct cm_data * K, int root, MPI_Comm comm)
{
    long server_prec = fprec(K->r);
    MPI_Bcast(&server_prec, 1, MPI_LONG, root, comm);
    long client_prec = fprec(K->r);
    MPI_Allreduce(MPI_IN_PLACE, &client_prec, 1, MPI_LONG, MPI_MIN, comm);
    if (client_prec < server_prec) {
        /* then we broadcast the rx fields */
        broadcast_mpfr(K->r, root, comm);
    }
}


void broadcast_cm_period_data(struct cm_period_data * p, int root, MPI_Comm comm)
{
    broadcast_mp_prec(&p->base_prec, root, comm);
    MPI_Bcast(&p->field, 1, MPI_INT, root, comm);
    for(int i = 0 ; i < 3 ; i++) for(int j = 0 ; j < 4 ; j++) {
        broadcast_mpz(p->pm_t[i][j], root, comm);
    }
    broadcast_mpz(p->pm_denom, root, comm);
    for(int i = 0 ; i < 3 ; i++) {
        broadcast_mpc(p->b[i], root, comm);
        broadcast_mpc(p->j[i], root, comm);
    }
    broadcast_mp_prec(&p->probably_correct_bits, root, comm);
    MPI_Bcast(&p->orbit, 1, MPI_INT, root, comm);
    MPI_Bcast(&p->no, 1, MPI_INT, root, comm);
    MPI_Bcast(&p->iter, 1, MPI_INT, root, comm);
}


void broadcast_cm_data_full(struct cm_data * K, int root, MPI_Comm comm)
{
    MPI_Bcast(K->DAB, 3, MPI_INT, root, comm);
    MPI_Bcast(K->DAB_r, 3, MPI_INT, root, comm);
    broadcast_mpz(K->dr, root, comm);
    broadcast_mpfr(K->r, root, comm);
    broadcast_cm_data_partial(K, root, comm);
    MPI_Bcast(&K->ncosets, 1, MPI_INT, root, comm);
    int rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    ASSERT_ALWAYS((rank != root) == (K->orbits == NULL));
}


static void broadcast_quadratic_number(quadratic_number_ptr q, int root, MPI_Comm comm) /* {{{ */
{
    broadcast_mpz(q->a, root, comm);
    broadcast_mpz(q->b, root, comm);
    broadcast_mpz(q->c, root, comm);
}
/* }}} */


/***** reconstruction of quadratic coefficients *****/

int reconstruction_what_is_happening(int * pwho, char * client_id, int tagmask)
{
    MPI_Status st;
    /* Don't start new work if we are on hold. */
    MPI_Recv(client_id, CM2_CLIENT_ID_BYTES, MPI_BYTE,
            MPI_ANY_SOURCE,
            tagmask,
            MPI_COMM_WORLD, &st);
    *pwho = st.MPI_SOURCE;
    return st.MPI_TAG;
}


void reconstruction_send_coeff_job(reconstruction_status_ptr state, reconstruction_coeff_result_ptr c, int who, char * client_id)
{
    state->wlist[who] = c->i;

    lprintf(LOG_PROGRESS
            "(%d,%d,%d) [X^%d] %s --> #%d %s\n",
            state->ndone,state->nworking,state->ntodo,
            c->i, state->poly_name,
            who, client_id);

    state->io_time -= hires_wct();
    int todo = CM2_TODO_RECOGNIZE_NUMBER;
    MPI_Bcast(&todo, 1, MPI_INT, 0, pals->c[who]);
    broadcast_mpfr(c->cx, 0, pals->c[who]);
    broadcast_mpz (c->premult, 0, pals->c[who]);
    broadcast_int (&(c->orbit), 0, pals->c[who]);
    broadcast_int (&(c->inum), 0, pals->c[who]);
    broadcast_int (&(c->iter), 0, pals->c[who]);
    broadcast_int (&(c->i), 0, pals->c[who]);
    state->io_time += hires_wct();
}


void reconstruction_receive_coeff_result(reconstruction_coeff_result_ptr c, reconstruction_status_ptr state, int who)
{
    c->i = state->wlist[who];
    ASSERT_ALWAYS(state->status[c->i] == 1);      /* working */
    c->q = state->H[c->i];
    state->io_time -= hires_wct();
    MPI_Bcast(&c->ttc, 1, MPI_DOUBLE, 1, pals->c[who]);
    MPI_Bcast(progress_string, sizeof(progress_string), MPI_BYTE, 1, pals->c[who]);
    /* If recognizing failed, then everything is set to zero here */
    broadcast_quadratic_number(c->q, 1, pals->c[who]);
    state->io_time += hires_wct();
}


void reconstruction_receive_coeff_job(reconstruction_coeff_result_ptr c)
{
    broadcast_mpfr(c->cx, 0, *pals->up);
    broadcast_mpz (c->premult, 0, *pals->up);
    broadcast_int (&(c->orbit), 0, *pals->up);
    broadcast_int (&(c->inum), 0, *pals->up);
    broadcast_int (&(c->iter), 0, *pals->up);
    broadcast_int (&(c->i), 0, *pals->up);
}


void reconstruction_send_coeff_result(reconstruction_coeff_result_ptr c, char * client_id)
{
    MPI_Send(client_id, CM2_CLIENT_ID_BYTES, MPI_BYTE,
            0, CM2_MPI_JOB_DONE_TAG,
            MPI_COMM_WORLD);
    MPI_Bcast(&c->ttc, 1, MPI_DOUBLE, 1, *pals->up);
    MPI_Bcast(progress_string, sizeof(progress_string), MPI_BYTE, 1, *pals->up);
    /* If recognizing failed, then everything is set to zero here */
    broadcast_quadratic_number(c->q, 1, *pals->up);
}


void mpi_mpfrx_server_start () {
   int size, todo, i;
   MPI_Status status;
   char client_id[CM2_CLIENT_ID_BYTES];

   MPI_Comm_size (MPI_COMM_WORLD, &size);
   lprintf (LOG_PROGRESS "MPI_MPFRX started\n");

   /* have all clients join */
   for (i = 1; i < size; i++) {
      MPI_Recv (client_id, CM2_CLIENT_ID_BYTES, MPI_BYTE, MPI_ANY_SOURCE,
            CM2_MPI_READY_TAG, MPI_COMM_WORLD, &status);
      lprintf(LOG_DEBUG "MPI_MPFRX joined #%d %s\n", status.MPI_SOURCE, client_id);
   }

   /* switch all clients to MPI_MPFRX mode */
   todo = CM2_TODO_MPFRX;
   for (i = 1; i < size; i++) {
      MPI_Bcast (&todo, 1, MPI_INT, 0, pals->c [i]);
      lprintf(LOG_DEBUG "MPI_MPFRX switched #%d\n", i);
   }

   /* go into server mode and wait for clients to connect */
   mpi_mpfrx_server_init ();
}


void mpi_mpfrx_server_stop () {
   mpi_mpfrx_server_finalise ();
   lprintf (LOG_PROGRESS "MPI_MPFRX stopped\n");
}
