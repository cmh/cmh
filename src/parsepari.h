#ifndef PARSEPARI_H_
#define PARSEPARI_H_

/* parsepari.h -- headers for parsepari.c
 *
 * Copyright (C) 2012, 2018 INRIA
 *
 * This file is part of CMH.
 *
 * CMH is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * CMH is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/ .
 */


#include <gmp.h>
#include <pari/pari.h>

#ifdef __cplusplus
extern "C" {
#endif

void mpz_set_GEN (mpz_ptr z, GEN x);
GEN mpz_get_GEN (mpz_srcptr z);

void parifopen (const char * filename);
void parifclose (void);
GEN parifreadint ();
GEN parifreadmod (GEN p);
void parifskipint (long n);

#ifdef __cplusplus
}
#endif

#endif  /* PARSEPARI_H_ */
