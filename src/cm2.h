#ifndef CM2_H_
#define CM2_H_

/* cm2.h -- data types related to cm2.c
 *
 * Copyright (C) 2010, 2011, 2012, 2013 INRIA
 *
 * This file is part of CMH.
 *
 * CMH is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * CMH is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/ .
 */

#include <mpfrcx.h>
#include "quadratic_field.h"
#include "factor.h"
#include "params.h"

#ifdef __cplusplus
extern "C" {
#endif

struct cm_data {
    int DAB[3];
    int DAB_r[3];
    mpz_t dr;   /* square-free integer s.t. K0r = Q(sqrt(dr)) */

    // square root of largest root of X^2-2AX+A^2-4B. i times this
    // generates the reflex real field.
    mpfr_t r;

    int ncosets;

    unsigned long int denom_prime_bound;

    int invariant_set;

    struct cm_orbit_data * orbits;
};

struct cm_orbit_data {
    int nperiods;
    // the period matrices corresponding to an orbit of ppav's of the same type
    // under the action of the group C(K)
    struct cm_period_data * periods;
    int nclasses;       // counting conjugate pairs, the number of classes
                        // (this is sometimes h1, sometimes 1/2^k times h1)

    // minimum and maximum of xprec's across the different periods.
    mpfr_prec_t minprec;
    mpfr_prec_t maxprec;

    // class polynomials H1, H2, H3, H12, H13
    quadratic_number * H[3];    // eventually all have denominator 1
    factor_matrix Hdenom_fact[3];
    mpz_t Hdenom[3];
};

struct cm_period_data {

    mpfr_prec_t base_prec;  // precision from which the newton lifting
                            // process started, for this period (might
                            // differ from what we have for other periods)

    int field;             // 1 for a real conjugate, 2 for a complex pair.

    mpz_t pm_t[3][4];   // symbolic period_matrix (coefficients, denominator)
    mpz_t pm_denom;
    
    mpc_t b[3];         // Quotients of theta constants.
                        // These get lifted iteration after iteration.

    mpc_t j[3];         // j-invariants. Recomputed from b[] at each iteration

    /* Computation of invariants from b[] is done at precision
     * prec(b[0]). However, the result is truncated to smaller precision,
     * because we believe that b[] is only accurate to some relative
     * precision. This count is updated at each Newton step. */
    mpfr_prec_t probably_correct_bits;

    int orbit, no, iter;
      /* only needed for checkpointing to determine the file name */
};

struct reconstruction_status_s {/*{{{*/
    int size;
    int ncoeffs;
    int last;
    int ndone, ntodo, nworking;
    int hold;
    int * status;
    int * wlist;
    double io_time;
    double client_time;
    double vain_time;
    double total_time;
    mpz_t denom_lcm;
    /* we do not (yet) have ownership on these */
    mpfrx_srcptr Hr;

    quadratic_number * H;
    int hecke;  /* 1 when we're recognizing an interpolating poly */
    const char * poly_name;
    mpz_t premult;
};
typedef struct reconstruction_status_s reconstruction_status[1];
typedef struct reconstruction_status_s * reconstruction_status_ptr;
typedef const struct reconstruction_status_s * reconstruction_status_srcptr;
/*}}}*/
struct reconstruction_coeff_result_s {/*{{{*/
    int i;
    double ttc;
    /* The following two are either private data for the mpi client loop,
     * or possibly direct pointer to the relevant parent structures for
     * the non-mpi program. Thus in any case, this structure does
     * obviously not have ownership of the data */
    quadratic_number_ptr q;
    mpfr_ptr cx;

    mpz_t premult;

    /* only needed for checkpointing to determine the right file name */
    int orbit, inum, iter;
};
typedef struct reconstruction_coeff_result_s reconstruction_coeff_result[1];
typedef struct reconstruction_coeff_result_s * reconstruction_coeff_result_ptr;
typedef const struct reconstruction_coeff_result_s * reconstruction_coeff_result_srcptr;
/*}}}*/

#ifdef __cplusplus
}
#endif

#endif	/* CM2_H_ */
