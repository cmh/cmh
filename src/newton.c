/* newton.c -- Newton lifting for theta constants.
 *
 * Copyright (C) 2006, 2010, 2011, 2012, 2013, 2019 INRIA
 *
 * This file is part of CMH.
 *
 * CMH is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * CMH is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/ .
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <time.h>

#include <mpc.h>
#include <limits.h>

#include "params.h"
#include "macros.h"
#include "naive.h"
#include "borchardt.h"
#include "newton.h"
#include "misc.h"

/*
 * The function f_tau on which we apply Newton iterations is
 *
 *
 * f_tau(b1,b2,b3) = (..., B_MEAN(b1_t, b2_t, b3_t)+Det(...)*B_MEAN(b1, b2, b3), ...)
 *
 */

static void transp_comatrix_and_det_3x3(mpc_t da[][3], mpc_t det, mpc_t db[][3])
{
    mpc_t aux1;
    cinit(aux1, cprec(da[0][0]));
    // db[3i+j] is d(b[i])/d(r[j])
    // so we exactly have the jacobian matrix we were looking for.
    // We compute the inverse of this matrix in da.
    //
    // we write the matrix entries as
    // 0 1 2    d(b[0])/d(r[0]) d(b[0])/d(r[1]) d(b[0])/d(r[2])
    // 3 4 5    d(b[1])/d(r[0]) d(b[1])/d(r[1]) d(b[1])/d(r[2])
    // 6 7 8    d(b[2])/d(r[0]) d(b[2])/d(r[1]) d(b[2])/d(r[2])
    //

    // compute the transposed comatrix.
    // 4 8 - 5 7
    cmul(da[0][0], db[1][1], db[2][2]);
    cmul(aux1, db[1][2], db[2][1]);
    csub(da[0][0], da[0][0], aux1);

    // 2 7 - 1 8
    cmul(da[0][1], db[0][2], db[2][1]);
    cmul(aux1, db[0][1], db[2][2]);
    csub(da[0][1], da[0][1], aux1);

    // 1 5 - 2 4
    cmul(da[0][2], db[0][1], db[1][2]);
    cmul(aux1, db[0][2], db[1][1]);
    csub(da[0][2], da[0][2], aux1);

    // 5 6 - 3 8
    cmul(da[1][0], db[1][2], db[2][0]);
    cmul(aux1, db[1][0], db[2][2]);
    csub(da[1][0], da[1][0], aux1);

    // 0 8 - 2 6
    cmul(da[1][1], db[0][0], db[2][2]);
    cmul(aux1, db[0][2], db[2][0]);
    csub(da[1][1], da[1][1], aux1);

    // 2 3 - 0 5
    cmul(da[1][2], db[0][2], db[1][0]);
    cmul(aux1, db[0][0], db[1][2]);
    csub(da[1][2], da[1][2], aux1);

    // 3 7 - 4 6
    cmul(da[2][0], db[1][0], db[2][1]);
    cmul(aux1, db[1][1], db[2][0]);
    csub(da[2][0], da[2][0], aux1);

    // 1 6 - 0 7
    cmul(da[2][1], db[0][1], db[2][0]);
    cmul(aux1, db[0][0], db[2][1]);
    csub(da[2][1], da[2][1], aux1);

    // 0 4 - 1 3
    cmul(da[2][2], db[0][0], db[1][1]);
    cmul(aux1, db[0][1], db[1][0]);
    csub(da[2][2], da[2][2], aux1);

    // det computed by developping the first row
    cmul(det, db[0][0], da[0][0]);

    cmul(aux1, db[0][1], da[1][0]);
    cadd(det, det, aux1);

    cmul(aux1, db[0][2], da[2][0]);
    cadd(det, det, aux1);

    // invert the determinant.
    cinv(det, det);
    cclear(aux1);
}

static void multiply_mat_vec(mpc_t a[3], mpc_t da[3][3], mpc_t b[3])
{
    mpc_t aux1;
    cinit(aux1, cprec(a[0]));
    // multiply the  transposed comatrix by the observed deltas.
    cmul(a[0], da[0][0], b[0]);
    cmul(aux1, da[0][1], b[1]);
    cadd(a[0], a[0], aux1);
    cmul(aux1, da[0][2], b[2]);
    cadd(a[0], a[0], aux1);

    cmul(a[1], da[1][0], b[0]);
    cmul(aux1, da[1][1], b[1]);
    cadd(a[1], a[1], aux1);
    cmul(aux1, da[1][2], b[2]);
    cadd(a[1], a[1], aux1);

    cmul(a[2], da[2][0], b[0]);
    cmul(aux1, da[2][1], b[1]);
    cadd(a[2], a[2], aux1);
    cmul(aux1, da[2][2], b[2]);
    cadd(a[2], a[2], aux1);
    cclear(aux1);
}

void
tau_from_bj_diff (mpc_t F[3], mpc_t dF[3][3], mpc_t b[3], mpc_t tau[3])
/* Input:
 * tau represents the matrix [tau0, tau1; tau1, tau2] in the Siegel half space
 * b contains an approximation to the three quotients of the
 *   fundamental theta constants at tau/2
 * Output:
 * F an approximation of tau computed from r, but with larger precision.
 * dF (if non NULL): approximations of the derivatives 
 */
{
   mpc_t s, ds[3];
   mpc_t t[10], dt[10][3];
   mpc_t m[3], dm[3][3];

   mpfr_prec_t prec = cprec (F [0]);

   cinit(s, prec);
   for (int i = 0; i < 10; i++) cinit(t[i], prec);

   if (dF) {
       for (int j = 0; j < 3; j++) {
           cinit(ds[j], prec);
           for (int i = 0; i < 10; i++) cinit(dt[i][j], prec);
       }
   }

   /* Get (projectively) all squares of theta constants at tau.  */
   {
       mpc_t       db[3][3];
       if (dF) {
           /* FIXME: It's slightly dumb */
           for (int i = 0; i < 3; i++) {
              for (int j = 0; j < 3; j++) {
                  cinit(db[i][j], prec);
                  cset_ui(db[i][j], (i==j));
              }
           }
       }
       get_10theta2x_from_3thetaqtauhalf_diff(t, dF?dt:NULL, b, dF?db:NULL);
       if (dF) {
           for (int i = 0; i < 3; i++) {
              for (int j = 0; j < 3; j++) {
                  cclear(db[i][j]);
              }
           }
       }
   }

   /* Now get the projective multiplier above. This will be used later on */
   BorchardtMean4Diff(s, dF?ds:NULL, t, dF?dt:NULL);

   for (int i = 0; i < 3; i++) cinit(m[i], prec);

   if (dF) {
       for (int j = 0; j < 3; j++) {
           for (int i = 0; i < 3; i++) cinit(dm[i][j], prec);
       }
   }

   /* Now three Borchardt means to recover the coefficients of tau */
   {
       int means[][4] = {
           {4,0,6,2},
           {0,8,4,12},
           {8,9,0,1}
       };
       mpc_t p[4], dp[4][3];
       for(int i = 0 ; i < 3 ; i++) {
           for(int k = 0 ; k < 4 ; k++) {
               cswap(p[k], t[even_thetas_ix[means[i][k]]]);
               if (dF) {
                   for(int j = 0 ; j < 3 ; j++) {
                       cswap(dp[k][j], dt[even_thetas_ix[means[i][k]]][j]);
                   }
               }
           }
           InverseBorchardtMean4Diff(m[i], dF?dm[i]:NULL, p, dF?dp:NULL);
           for(int k = 0 ; k < 4 ; k++) {
               cswap(p[k], t[even_thetas_ix[means[i][k]]]);
               if (dF) {
                   for(int j = 0 ; j < 3 ; j++) {
                       cswap(dp[k][j], dt[even_thetas_ix[means[i][k]]][j]);
                   }
               }
           }
       }
   }

   for (int i = 0; i < 10; i++) cclear(t[i]);

   if (dF) {
       for (int j = 0; j < 3; j++) {
           for (int i = 0; i < 10; i++) cclear(dt[i][j]);
       }
   }

   /* the m[i]'s are actually 1/s times the m[i]'s we are looking for.
    * Proceed with the multiplication.
    */
   {
       mpc_t aux1;
       if (dF) cinit(aux1, prec);
       for (int i = 0; i < 3; i++) {
           if (dF) {
               for (int j = 0; j < 3; j++) {
                   cmul(aux1, m[i], ds[j]);
                   cmul(dm[i][j], dm[i][j], s);
                   cadd(dm[i][j], dm[i][j], aux1);
               }
           }
           cmul(m[i], m[i], s);
       }
       if (dF) cclear(aux1);
   }

   cclear(s);
   if (dF) for (int j = 0; j < 3; j++) cclear(ds[j]);

   /* Compute F and dF */

   /* m0 = 1/i * tau0
    * m2 = 1/i * tau2
    * m1 = -tau0*tau2 + tau1^2
    * m1 = m0*m2 + tau1^2
    *
    * F0 = tau0 = i m0
    * F1 = tau2 = i m2
    * F2 = tau1 = sqrt(m1 - m0 m2)
    */
   cmul_by_i(F[0], m[0]);
   cmul_by_i(F[2], m[2]);
   cmul(F[1], m[0], m[2]);
   csub(F[1], m[1], F[1]);
   csqrt(F[1], F[1]);

   if (dF) {
       /* 1/2sqrt(m1 - m0 m2) appears in all derivatives. We use m[1], which
        * is otherwise unused from now on, to store it. */
       cmul_2ui(m[1], F[1], 1);
       cinv(m[1], m[1]);
       for (int j = 0; j < 3; j++) {
           /* Use dF[0][j] and dF[2][j] as temporaries */
           /* dF1 = 1/2F1*(dm1 - m0 dm2 - m2 dm0) */
           cmul(dF[0][j], dm[2][j], m[0]);  // m0 dm2
           cmul(dF[2][j], dm[0][j], m[2]);  // m2 dm0
           cadd(dF[1][j], dF[0][j], dF[2][j]);
           csub(dF[1][j], dm[1][j], dF[1][j]); // dm1 - m0 dm2 - m2 dm0
           cmul(dF[1][j], dF[1][j], m[1]);
           cmul_by_i(dF[0][j], dm[0][j]);
           cmul_by_i(dF[2][j], dm[2][j]);
       }
   }

   /* Which of b[1] and -b[1] is closest to tau[1] ? */
   if (does_negation_bring_closer(F[1], tau[1])) {
         cneg(F[1], F[1]);
         if (dF) {
             for (int i = 0; i < 3; i++) {
                 cneg(dF[1][i], dF[1][i]);
             }
         }
   }

   /* Now we are done with tau_from_bj */
   for (int i = 0; i < 3; i++) cclear(m[i]);

   if (dF) {
       for (int j = 0; j < 3; j++) {
           for (int i = 0; i < 3; i++) cclear(dm[i][j]);
       }
   }
}


void
tau_from_bj (mpc_t *F, mpc_t *r, mpc_t *tau)
{
    tau_from_bj_diff(F, NULL, r, tau);
}

/* Identical to tau_from_bj_diff, except that we work with finite
 * differences instead */
void
tau_from_bj_diff_fd (mpc_t F[3], mpc_t dF[3][3], mpc_t b[3], mpc_t tau[3])
{
   /* Call tau_from_bj four times, no less. */

   mpfr_prec_t prec = cprec (F [0]);

   mpc_t bx[3], Fx[3];
   mpc_t epsilon;

   for (int i = 0; i < 3; i++) {
       cinit(bx[i], prec);
       cinit(Fx[i], prec);
   }

   cinit(epsilon, prec);
   cone(epsilon);
   mpfr_prec_t exp_epsilon = (prec - SEC_MARGIN) / 2;
   cdiv_2ui(epsilon, epsilon, exp_epsilon);

    /* Evaluate tau_from_bj at r, and at r+some variations, in order to
     * get estimations for the partial derivatives.
     */

   tau_from_bj(F, b, tau);

   cadd(bx[0], b[0], epsilon);
   cset(bx[1], b[1]);
   cset(bx[2], b[2]);
   tau_from_bj(Fx, bx, tau);
   csub(Fx[0], Fx[0], F[0]); cmul_2ui(dF[0][0], Fx[0], exp_epsilon);
   csub(Fx[1], Fx[1], F[1]); cmul_2ui(dF[1][0], Fx[1], exp_epsilon);
   csub(Fx[2], Fx[2], F[2]); cmul_2ui(dF[2][0], Fx[2], exp_epsilon);

   cset(bx[0], b[0]);
   cadd(bx[1], b[1], epsilon);
   cset(bx[2], b[2]);
   tau_from_bj(Fx, bx, tau);
   csub(Fx[0], Fx[0], F[0]); cmul_2ui(dF[0][1], Fx[0], exp_epsilon);
   csub(Fx[1], Fx[1], F[1]); cmul_2ui(dF[1][1], Fx[1], exp_epsilon);
   csub(Fx[2], Fx[2], F[2]); cmul_2ui(dF[2][1], Fx[2], exp_epsilon);

   cset(bx[0], b[0]);
   cset(bx[1], b[1]);
   cadd(bx[2], b[2], epsilon);
   tau_from_bj(Fx, bx, tau);
   csub(Fx[0], Fx[0], F[0]); cmul_2ui(dF[0][2], Fx[0], exp_epsilon);
   csub(Fx[1], Fx[1], F[1]); cmul_2ui(dF[1][2], Fx[1], exp_epsilon);
   csub(Fx[2], Fx[2], F[2]); cmul_2ui(dF[2][2], Fx[2], exp_epsilon);

   for (int i = 0; i < 3; i++) {
       cclear(bx[i]);
       cclear(Fx[i]);
   }
   cclear(epsilon);
}

/* Input:
 *
 * tau      represents the matrix [tau0, tau1; tau1, tau2] in the Siegel
 *          half space
 * r        contains an approximation to the three fundamental theta
 *          quotients at tau/2
 * method   choose between a finite differences method (0) or a
 *          computation of derivatives by Borchardt mean (1).
 *          (The finite differences method is faster.)
 *
 * Output:
 *
 * res      the three fundamental theta quotients after one step of the
 *          Newton iteration on r
 *    
 * Typically, the precison of res equals that of tau and is twice
 * that of r.
 *
 *
 * Note that res is *not* claimed to be correctly rounded.
 *
 * If we assume that tau is exact, and we denote by acc(x) the number of
 * accurate bits in a complex number compared to what it should be (both
 * r and res "should be" equal to the theta quotients at tau/2), then we
 * expect something like
 *      acc(res) = 2*acc(r) - delta.
 *
 * where delta is presently not computed.
 */

void
newtonstep_3thetaq (mpc_t *res, mpc_t *r, mpc_t *tau, int method)
{
   mpfr_prec_t prec = cprec (res [0]) + SEC_MARGIN;
   mpc_t a[3];
   mpc_t F[3];
   mpc_t J[3][3];       /* Jacobian matrix */
   mpc_t Jinv[3][3];    /* Inverse of the Jacobian matrix */
   mpc_t detJinv;       /* Inverse determinant of the Jacobian matrix */

   for (int i = 0; i < 3; i++) cinit(F[i], prec);
   for(int j = 0 ; j < 3 ; j++) {
       for (int i = 0; i < 3; i++) cinit(J[i][j], prec);
   }

   if (method == 0) {
       tau_from_bj_diff_fd(F, J, r, tau);
   } else {
       tau_from_bj_diff(F, J, r, tau);
   }

   for (int i = 0; i < 3; i++) {
      csub(F[i], F[i], tau[i]);
   }

   for (int j = 0; j < 3; j++) {
       for (int i = 0; i < 3; i++) cinit(Jinv[i][j], prec);
   }
   cinit(detJinv, prec);
   transp_comatrix_and_det_3x3(Jinv, detJinv, J);

   for (int j = 0; j < 3; j++) {
       for (int i = 0; i < 3; i++) cclear(J[i][j]);
   }
   for (int i = 0; i < 3; i++) cinit(a[i], prec);

   multiply_mat_vec(a, Jinv, F);

   for (int i = 0; i < 3; i++) cclear(F[i]);
   for (int j = 0; j < 3; j++) {
       for (int i = 0; i < 3; i++) cclear(Jinv[i][j]);
   }

   // and eventually multiply by the inverse determinant
   for (int i = 0; i < 3; i++) {
      cmul(a[i], a[i], detJinv);
      csub(res[i], r[i], a[i]);
   }

   for (int i = 0; i < 3; i++) cclear(a[i]);
   cclear(detJinv);
}

/* This function computes the squares of the ten fundamental theta
 * constants, for tau in the fundamental domain. The code here is
 * directly extracted from code in cm2.c
 *
 * Input:
 *
 * tau         represents the matrix [tau0, tau1; tau1, tau2] in the Siegel
 *             half space
 * base_prec   tell where the naive computation begins. Any value below
 *             the configured default will be silently set to the default value
 *             (currently around 2000 bits).
 * method      choose between a finite differences method (0) or a
 *             computation of derivatives by Borchardt mean (1).
 *             (The finite differences method is faster.)
 *
 * Output:
 *
 * th2         th2[0] to th2[9] contain the squares of the theta constants
 *             theta^2_{0,1,2,3,4,6,8,9,12,15}(0;tau).
 *
 * theta constants are computed at low precision first, and later lifted
 * using newtonstep_3thetaq as defined above.
 */
#define DEFAULT_BASE_PREC       2000
void
eval_10theta2_newton (mpc_t *th2, mpc_t *tau, int base_prec, int method)
{

   mpc_t b[3], b1[3];
   mpfr_prec_t prec, probably_correct;

   base_prec = CMH_MAX (base_prec, DEFAULT_BASE_PREC);

   prec = base_prec;

   for (int k = 0; k < 3; k++)  cinit (b[k], prec);

   /* Start with something ! */
   {
       mpc_t tmp;
       mpc_t tauhalf[4], th [4];
       for (int k = 0; k < 3; k++)  cinit (tauhalf[k], prec);
       for (int k = 0; k < 4; k++)  cinit (th [k], prec);
       cinit(tmp, prec);

       for (int k = 0; k < 3; k++) cdiv_2ui (tauhalf [k], tau [k], 1);
       eval_4theta_naive (th, tauhalf);
       cinv (tmp, th[0]);
       for (int k = 0; k < 3; k++) cmul (b [k], th[k+1], tmp);

       cclear(tmp);
       for (int k = 0; k < 4; k++)  cclear (th [k]);
       for (int k = 0; k < 3; k++)  cclear (tauhalf[k]);
       probably_correct = base_prec;
   }

   for (int k = 0; k < 3; k++)  cinit (b1[k], prec);

   for( ; probably_correct < cprec(th2[0]) ; ) {
      mpfr_prec_t oprec = cprec(b[0]);
      mpfr_prec_t nprec = 2*cprec(b[0]);
      for(int i = 0 ; i < 3 ; i++) cset_prec(b1[i], CMH_MIN(cprec(th2[0]), nprec));

      newtonstep_3thetaq (b1, b, tau, method);

      /* How many correct bits did we have in b ? */
      mpfr_prec_t correct_b = agreeing_bits_n(b1, b, 3);

      assert (correct_b > base_prec / 2);

      /* Truncate so as to keep a constant amount of garbage */
      nprec = 2 * correct_b;
      /* And anticipate for the rest of the computation */
      probably_correct = nprec - (oprec - correct_b);

      nprec = CMH_MIN(nprec, cprec(b1[0]));
      probably_correct = CMH_MIN(probably_correct, cprec(b1[0]));

      for(int i = 0 ; i < 3 ; i++) {
         cprec_round(b1[i], nprec);
         cswap(b[i], b1[i]);
      }
   }

   for (int k = 0; k < 3; k++)  cclear (b1[k]);
   /* Now recover the desired squares of theta constants from b[] */
   {
       /* We're running the duplication formulae on
        * (1,theta_1/theta_0,theta_2/theta_0,theta_3/theta_0).
        */
       get_10theta2x_from_3thetaqtauhalf(th2, b);
       /* We need a Borchardt mean to get the proper scaling factor (note
        * that for invariant computations, this step is useless, as we
        * are computing homogeneous formulae anyway...
        */
       mpc_t tmp;
       cinit(tmp, cprec(th2[0]));
       InverseBorchardtMean4Diff(tmp, NULL, th2, NULL);
       /* This should be theta_0^2(0,tau/2) */
       for(int k = 0 ; k < 10 ; k++) {
           cmul(th2[k], th2[k], tmp);
       }
       cclear(tmp);
   }
   for (int k = 0; k < 3; k++)  cclear (b[k]);
}
