#ifndef MPICM2_H
#define MPICM2_H

/* mpicm2.h -- headers for mpicm2.c
 *
 * Copyright (C) 2010, 2011, 2012, 2013 INRIA
 *
 * This file is part of CMH.
 *
 * CMH is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * CMH is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/ .
 */

#include <mpi.h>
#include "cm2.h"

#define CM2_CLIENT_ID_BYTES     80
#define CM2_MPI_READY_TAG       100
#define CM2_MPI_JOB_DONE_TAG    101
#define CM2_TODO_COMPUTE_THETA_CONSTANTS 20
#define CM2_TODO_RECOGNIZE_NUMBER        21
#define CM2_TODO_MPFRX                   22
#define CM2_TODO_BYE_BYE       0

#define MPI_MY_SIZE_T   MPI_UNSIGNED_LONG
#define MPI_MY_UINT64_T   MPI_UNSIGNED_LONG
#define MPI_MY_INT64_T   MPI_LONG
#define MPI_MY_MP_SIZE_T        MPI_LONG
#define MPI_MY_MP_LIMB_T        MPI_UNSIGNED_LONG
#define MPI_MY_GMP_INTERNAL_SIZE_FIELD_T MPI_INT

struct mpi_comms {
    MPI_Comm * c;
    MPI_Comm * up;
};

extern char progress_string[80];

void mpi_custom_types_check();
void mpi_comms_init(struct mpi_comms * m);
void mpi_comms_clear(struct mpi_comms * m);

void broadcast_mp_prec(mpfr_prec_t * x, int root, MPI_Comm comm);
//void broadcast_mpfr(mpfr_ptr x, int root, MPI_Comm comm);
void broadcast_cm_data_partial(struct cm_data * K, int root, MPI_Comm comm);
void broadcast_cm_data_full(struct cm_data * K, int root, MPI_Comm comm);
void broadcast_cm_period_data(struct cm_period_data * p, int root, MPI_Comm comm);

int reconstruction_what_is_happening(int * pwho, char * client_id, int tagmask);
void reconstruction_send_coeff_job(reconstruction_status_ptr state, reconstruction_coeff_result_ptr c, int who, char * client_id);
void reconstruction_receive_coeff_result(reconstruction_coeff_result_ptr c, reconstruction_status_ptr state, int who);
void reconstruction_receive_coeff_job(reconstruction_coeff_result_ptr c);
void reconstruction_send_coeff_result(reconstruction_coeff_result_ptr c, char * client_id);

extern void mpi_mpfrx_server_start ();
extern void mpi_mpfrx_server_stop ();
#endif
