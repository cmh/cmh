/* cm2.c -- computation of class polynomials
 *
 * Copyright (C) 2010, 2011, 2012, 2013, 2014, 2019, 2021, 2022 INRIA
 *
 * This file is part of CMH.
 *
 * CMH is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * CMH is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/ .
 */

/*{{{ includes, etc. */
#define _POSIX_C_SOURCE 200112L
#define _GNU_SOURCE
#include "config.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <errno.h>
#include <assert.h>
#include <mpfrcx.h>
#include <math.h>
#include <limits.h>
#include <time.h>
#include <sys/utsname.h>
#include <sys/types.h>
#include <unistd.h>
#ifdef  WANT_STACK_BACKTRACES
/* This exposes the right prototypes for both the extended code, and the
 * plain glibc code. Note that the autoconf code ensures that at least
 * execinfo.h is available in order for WANT_STACK_BACKTRACES to be
 * defined. */
#include <execinfo.h>
#endif
#include <signal.h>

#ifdef  WITH_MPI
#include "mpicm2.h"
#include "mpfrx-mpi.h"
#endif

#include "cm2.h"
#include "params.h"
#include "macros.h"

#include "cmh.h"
/* functions from misc.c are actually in cmh, but not publicly exposed */
#include "misc.h"

#include "jinvariants.h"        /* invariants_from_th2 */
// #include "borchardt.h"
// #include "naive.h"
#include "newton.h"             /* newtonstep_3thetaq */
#include "io.h"
#include "cputime.h"
#include "lprintf.h"
#include "version.h"
/*}}}*/

/*{{{ globals, macros */
int loglevel = 3;
static int nobother_prec = 0;
static int inputfile_format = -1;       /* version number */
static int newton_method = 0;

static int BASE_PREC = 2000;

char progress_string[80];
#ifdef  WITH_MPI
extern struct mpi_comms pals[1];
#endif

#define DEGH(O,inum)    ((O)->nclasses-((inum)>0))
/*}}}*/

/* {{{ A note on precision *
 *
 * At a given iteration, the (imaginary part of the) root defining the
 * CM field has been computed to some precision. This precision is
 * the precision of the mpfr_t value K->r. This value may itself embed
 * some inaccuracy. But we for the moment we do not attempt to track this
 * (read: we do _not_ have correct rounding of K->r, this is an
 * acknowledged defect which we could easily fix).
 *
 * So K->r is an mpfr_t number, with some garbage bits in the end.
 *
 * For each period matrix, the complex coefficients of tau are computed,
 * and K->r is an input in this process, together with the coefficients
 * of the symbolic period matrices which are found in the .in file. A
 * period matrix is computed to some precision p. We require that
 * prec(K->r) be at least p, otherwise we recompute it to precision p. As
 * is the case for K->r, we do not have correct rounding for period matrices.
 * Garbage bits may accumulate in the tail of the expansion of the
 * coefficients. Tracking this would be possible, by inspecting the
 * coefficients of the period matrices, to start with. This would imply
 * that we require K->r be computed to some precision possibly higher.
 *
 * So p->tau is a matrix to some precision, and coefficients may have
 * some garbage bits in the end, which we could track down but don't,
 * yet.
 *
 * From p->tau, the quotients of squares of theta constants are computed
 * by Newton lifting. The basic computation accumulates the garbage bits
 * in p->tau, and also has some method inaccuracy. Thus p->b, computed
 * from p->tau, even if at the same precision p, has even more garbage
 * bits. Like what happens for K->r and p->tau, we do not track down the
 * inaccuracy here. Contrarily to what happens above, though, we do not
 * even remotely _hope_ to track it down. Thus we have to live with the
 * idea that p->b is less accurate than what prec(p->b) says. From this
 * data, Newton lifts are computed. There goes the trick. We would like
 * to have the amount of garbage bits in the end remain somewhat
 * constant, and not keep doubling as the computation goes. For this
 * reason, the following algorithm is used. Let b' be the triple obtained
 * by a newton lift of b, which uses tau as an input. We expect the
 * following. Let err(x) = |x-x*|, where x* is the correct value. We have
 *      err(b') <= err(b)^2 * M1 + err(tau) * M2
 * where M1 and M2 are some computable values, depending on tau. We do
 * not know the sizes in bits m1, m2 of the constants M1, M2. Let us
 * assume that err(tau) is small enough, so that the first term above
 * dominates.
 *
 * Say that we have p-x really correct bits for b (we do not know x).
 * Computations are carried out at precision 2p. We assume that
 * tau is accurate enough. We expect to have 2p-2x-m1 correct bits for b'
 * after lifting.  By comparing b' and b, we have an idea of the value of
 * x, which tells us how far off we were in assuming b was correct. This
 * indicates the extraneous amount of garbage which accumulates in b'. We
 * may strip 2x bits in b'. This way, the amount of garbage bits in b
 * stabilizes at m1.
 *
 * As an example, imagine that we start with 10 garbage bits in the first
 * iterate for b, and that m1 is 15 bits. Let b' be the triple computed
 * at precision 2*prec(b). Let x(b) denote the number of garbage bits in
 * the expansion.
 *
 * prec(b0)=100  x(b0)=100 -  90=10 x(b0')= 200- 165=35 -> truncate at 180
 * prec(b1)=180  x(b1)=180 - 165=15 x(b1')= 360- 315=45 -> truncate at 330
 * prec(b2)=330  x(b2)=330 - 315=15 x(b2')= 660- 615=45 -> truncate at 630
 * prec(b3)=630  x(b3)=630 - 615=15 x(b2')=1260-1215=45 -> truncate at 1230
 * prec(b4)=1230 x(b4)=1230-1215=15 x(b4')=2460-2415=45 -> truncate at 2430
 *
 * It is possible to guess the number of correct bits in b. In the scheme
 * above, x(b) is going to stabilize at some value. Thus the _next_ value
 * x(bi) is predictible. We use this as an indirect means to deduce the
 * precision to be used for the j-invariants.
 *
 * }}} */

/* {{{ some error checks */
#define ASPRINTF_CHECK(rc__) do {					\
    if ((rc__) < 0) {							\
        fprintf(stderr, "asprintf error in %s !!!\n", __func__);	\
        abort();							\
    }									\
} while (0)

#define GENERIC_ERROR_CHECK_1ARG_ERRNO(cond__, fcn__, arg1__) do {	\
    if (cond__) {							\
        fprintf(stderr, "%s(%s): %s\n", fcn__, arg1__, strerror(errno));\
        abort();							\
    }									\
} while (0)

#define FOPEN_CHECK(f__, filename__) do {                               \
    GENERIC_ERROR_CHECK_1ARG_ERRNO(f__ == NULL, "fopen", filename__);   \
} while (0)
/* }}} */

static void cm_data_init(struct cm_data * K)/*{{{*/
{
    memset(K, 0, sizeof(struct cm_data));
    mpfr_init(K->r);
    mpz_init(K->dr);
    K->invariant_set = 2;
       /* standard choice without command line argument */
}
/* }}} */

static void cm_data_init_aux(struct cm_data * K)/*{{{*/
{
    int D = K->DAB[0];
    int A = K->DAB[1];
    int B = K->DAB[2];

    int Ar = 2*A;
    int Br = A*A-4*B;
    if (((Ar&3)|(Br&15))==0) {
        Ar>>=2;
        Br>>=4;
    }
    int Dr = Ar*Ar-4*Br;
    // Dr = (4 ou 1/4)*A^2
    //     -(4 ou 1/4)*(A^2-4*B)
    //     = (16 ou 1)*B

    mpz_set_ui(K->dr, Dr);
    mpz_remove_squares(K->dr);
    Dr = mpz_get_ui(K->dr);
    // arrange so that it's still a fundamental discriminant !
    if (Dr % 4 != 0 && Dr % 4 != 1) Dr *= 4;
    K->DAB_r[0]=Dr;
    K->DAB_r[1]=Ar;
    K->DAB_r[2]=Br;

    /* According to Theorem 10.1 of Marco Streng: "Computing Igusa class
       polynomials", Mathematics of Computation 83 (285), pp. 275-309, 2014,
       if K=Q(sqrt(a+b sqrt(d))) with integers a, b and d, then the primes
       in the denominator are bounded by 4*d*a^2.
       In our notation, K is generated by sqrt(4*u) with u^2+Au+B=0, thus
       4*u=-2*A-2*sqrt(A^2-4B)=-a-b*d, where d is the square-free part of
       A^2-4B, which is either D or D/4. In a second step, we may divide by
       the greatest common square factor of a and b; by the supposed
       minimality of A and B this is 1 for odd and 4 for even A. */
    unsigned long int a, d;
    a = (A % 2 == 0 ? A/2 : 2*A);
    d = (D % 4 == 0 ? D/4 : D);
    K->denom_prime_bound = 4 * d * a * a;
    lprintf(LOG_NORMAL "prime divisors of denominators cannot exceed %lu\n",
            K->denom_prime_bound);

}/*}}}*/

static void cm_orbit_clear(struct cm_orbit_data * O)/*{{{*/
{
   for (int i = 0; i < 3; i++) {
      for(int j = 0 ; j <= O->nclasses ; j++)
         quadratic_number_clear(O->H[i][j]);
      if (O->Hdenom_fact [i] != NULL)
         factor_matrix_free (O->Hdenom_fact [i]);
      mpz_clear (O->Hdenom [i]);
      free(O->H[i]);
   }
}/*}}}*/

static void cm_data_clear(struct cm_data * K)/*{{{*/
{
    mpz_clear(K->dr);
    mpfr_clear(K->r);
}/*}}}*/

static void cm_data_consistency_check(struct cm_data * K)/*{{{*/
{
    mpz_t m;
    mpz_init(m);

    int D = K->DAB[0];
    int A = K->DAB[1];
    int B = K->DAB[2];

    mpz_t disc;
    mpz_init(disc);
    mpz_set_ui(disc, A);
    mpz_mul(disc, disc, disc);
    mpz_sub_ui(disc, disc, B);
    mpz_sub_ui(disc, disc, B);
    mpz_sub_ui(disc, disc, B);
    mpz_sub_ui(disc, disc, B);

    assert(mpz_divisible_ui_p(disc, D));
    mpz_divexact_ui(m, disc, D);
    assert(mpz_perfect_square_p(m));
    mpz_sqrt(m,m);

    mpz_clear(disc);
    mpz_clear(m);
}/*}}}*/

static void cm_data_set_real_root (struct cm_data *K, mpfr_prec_t prec)/*{{{*/
{
    // compute the square root of the largest root of X^2-2AX+A^2-4B. i
    // times this generates the reflex real field.
   int A = K->DAB [1], B = K->DAB [2];

   if (mpfr_get_prec(K->r) >= prec) {
       return;
   }

   lprintf(LOG_DEBUG "Computing field root with precision %Pu bits\n", prec);
   mpfr_set_prec (K->r, prec);

   if (K->DAB_r[1] > 0) {
       fset_ui (K->r, 4*B);
       fsqrt (K->r, K->r);
       fadd_ui (K->r, K->r, A);
       fsqrt(K->r, K->r);
   } else {
       /* We're Galois. Our data is written with respect to the root of
        * the polynomial x^4+Ax^2+B */
       /* Large root ot x^2-Ax+B */
       fset_ui (K->r, A*A-4*B);
       fsqrt (K->r, K->r);
       fadd_ui(K->r, K->r, A);
       fdiv_2ui(K->r, K->r, 1);
       fsqrt(K->r, K->r);
   }
}
/*}}}*/

static void cm_period_init_aux(struct cm_period_data * p)/*{{{*/
{
    mpz_init(p->pm_denom);
    for(int i = 0 ; i < 3 ; i++) {
        for(int j = 0 ; j < 4 ; j++) {
            mpz_init(p->pm_t[i][j]);
        }
    }

    for(int i = 0 ; i < 3 ; i++) {
        cinit(p->b[i], MPFR_PREC_MIN);
        cinit(p->j[i], MPFR_PREC_MIN);
    }
}/*}}}*/

static void cm_orbit_init_aux(struct cm_orbit_data * O)/*{{{*/
{
    memset(O, 0, sizeof(struct cm_orbit_data));
}
/* }}} */

static void cm_alloc_orbits(struct cm_data * K)/*{{{*/
{
    K->orbits = (struct cm_orbit_data *) malloc(K->ncosets * sizeof(struct cm_orbit_data));
    for(int i = 0 ; i < K->ncosets ; i++) {
        cm_orbit_init_aux(&(K->orbits[i]));
    }
}
/*}}}*/

static void cm_orbit_alloc_periods(struct cm_orbit_data * O, int no)/*{{{*/
{
    O->periods = (struct cm_period_data *) malloc(no * sizeof(struct cm_period_data));
    for(int i = 0 ; i < no ; i++) {
        cm_period_init_aux(&(O->periods[i]));
    }
}
/*}}}*/

static void cm_period_clear(struct cm_period_data * p)/*{{{*/
{
    mpz_clear(p->pm_denom);
    for(int i = 0 ; i < 3 ; i++) {
        for(int j = 0 ; j < 4 ; j++) {
            mpz_clear(p->pm_t[i][j]);
        }
    }

    for(int i = 0 ; i < 3 ; i++) {
        cclear(p->b[i]);
        cclear(p->j[i]);
    }
}
/*}}}*/

static void cm_orbit_realloc_periods(struct cm_orbit_data * O, int no_old, int no_new)/*{{{*/
{
    ASSERT_ALWAYS(no_new <= no_old);
    for(int i = no_new ; i < no_old ; i++) {
        cm_period_clear(&(O->periods[i]));
    }
    O->periods = (struct cm_period_data *) realloc(O->periods, no_new * sizeof(struct cm_period_data));
}
/*}}}*/

static void cm_orbit_clear_periods(struct cm_orbit_data * O)/*{{{*/
{
    for(int i = 0 ; i < O->nperiods ; i++) {
        cm_period_clear(&(O->periods[i]));
    }
    free(O->periods);
}
/*}}}*/

static void cm_clear_orbits(struct cm_data * K)/*{{{*/
{
    /* It is possible to have K->orbits still unallocated here, in the
     * context of MPI clients (which never receive the corresponding
     * information */
    if (K->orbits) {
        for(int i = 0 ; i < K->ncosets ; i++) {
            cm_orbit_clear_periods(&(K->orbits[i]));
            cm_orbit_clear(&(K->orbits[i]));
        }
    }
    free(K->orbits);
    K->orbits = NULL;
}/*}}}*/

static void cm_read_one_period(struct cm_period_data * p, FILE * f)/*{{{*/
{
    int rc = fscanf(f, "%d", &p->field);
    if (rc != 1) {
       fprintf(stderr, "Could not read in cm_read_one_period !!!\n");
       abort();
    }
    gmp_fscanf(f, "%Zd", p->pm_denom);
    for(int i = 0 ; i < 3 ; i++)
        for(int j = 0 ; j < 4 ; j++)
            gmp_fscanf(f, "%Zd", p->pm_t[i][j]);
}/*}}}*/

static void cm_read_orbits(struct cm_data * K, FILE * f)/*{{{*/
{
    int rc = fscanf(f, "%d", &K->ncosets);
    if (rc != 1) {
       fprintf(stderr, "Could not read in cm_read_orbits !!!\n");
       abort();
    }
    cm_alloc_orbits(K);

    for(int i = 0 ; i < K->ncosets ; i++) {
        struct cm_orbit_data * O = &(K->orbits[i]);
        int rc, degree;
        rc = fscanf(f, "%d", &degree);
        if (rc != 1) {
           fprintf(stderr, "Could not read in cm_read_orbits !!!\n");
           abort();
        }

        cm_orbit_alloc_periods(O, degree);

        O->nclasses = 0;
        for(O->nperiods = 0 ; O->nclasses < degree ; O->nperiods++) {
            struct cm_period_data * p = &(O->periods[O->nperiods]);
            cm_read_one_period(p, f);
            p->orbit = i;
            p->no = O->nperiods;
            O->nclasses += p->field;
        }
        cm_orbit_realloc_periods (O, degree, O->nperiods);

        lprintf(LOG_NORMAL "Coset %d: %d periods: %d classes\n", i, O->nperiods, O->nclasses);
        for (int j = 0; j <=2; j++) {
            O->H[j] = malloc((1+DEGH(O, 0)) * sizeof(quadratic_number));
            for(int k = 0 ; k <= O->nclasses ; k++)
                quadratic_number_init(O->H[j][k]);
            mpz_init (O->Hdenom [j]);
        }
    }
}/*}}}*/

/* clear small real parts in tau. {{{ */
static void sanitize_tau(mpc_t * tp)
{
    mpfr_prec_t prec = cprec(tp[0]);
    mpfr_t auxf, auxcmp;
    finit(auxf, prec);
    finit(auxcmp, prec);
    fset_ui(auxcmp, 1);
    fdiv_2ui(auxcmp, auxcmp, prec-200);
    for (int i=0; i<3; i++)
    {
        fabs(auxf, MPC_RE(tp[i]));
        if ( fcmp(auxf, auxcmp) < 0 )
        {
            fset_ui(MPC_RE(tp[i]), 0);
        }
    }
    fclear(auxf);
    fclear(auxcmp);
} /*}}}*/

static mpfr_prec_t cm_orbit_invariants_precision(struct cm_orbit_data * O)/*{{{*/
{
    /* Return the minimal precision of the computed invariants */
    mpfr_prec_t prec = MPFR_PREC_MAX;
    for(int i = 0 ; i < O->nperiods ; i++) {
        struct cm_period_data * p = &(O->periods[i]);
        mpfr_prec_t x = p->probably_correct_bits;
        // ASSERT_ALWAYS(x);
        if (x < prec) prec = x;
    }
    return prec;
}/* }}} */


static void reconstruction_status_init(reconstruction_status_ptr state, int ncoeffs, int size)/*{{{*/
{
    memset(state, 0, sizeof(reconstruction_status));
    state->size = size;
    state->ncoeffs = ncoeffs;
    state->status = malloc(ncoeffs * sizeof(int));
    state->wlist = malloc(size * sizeof(int));
    state->ntodo = ncoeffs;
    state->last = ncoeffs;
    mpz_init (state->premult);
    mpz_init_set_ui(state->denom_lcm, 1);
    memset(state->status, 0, ncoeffs * sizeof(int));
}
/*}}}*/
static void reconstruction_status_clear(reconstruction_status_ptr state)/*{{{*/
{
    free(state->wlist);
    free(state->status);
    mpz_clear (state->premult);
    mpz_clear(state->denom_lcm);
    memset(state, 0, sizeof(*state));
}
/*}}}*/

static void reconstruction_coeff_result_init(reconstruction_coeff_result_ptr c)/*{{{*/
{
    memset(c, 0, sizeof(reconstruction_coeff_result));
    mpz_init (c->premult);
}
/*}}}*/

static void reconstruction_coeff_result_clear(reconstruction_coeff_result_ptr c)/*{{{*/
{
    mpz_clear (c->premult);
    memset(c, 0, sizeof(reconstruction_coeff_result));
}
/*}}}*/

static int reconstruction_per_coeff(reconstruction_status_ptr state, reconstruction_coeff_result_ptr c)/*{{{*/
/* This function is called by the master job upon reception of a class
 * polynomial coefficient.
 */
{
    int coeff_ok = mpz_cmp_ui(c->q->c, 0) != 0;
    const char * failure = NULL;
    if (!coeff_ok) {
        state->vain_time += c->ttc;
        /* This recognition failed. With the simplified factoring scheme,
         * we don't even plan on retrying with a given cofactor.
         * Therefore, we stop spawning new computations, and the
         * reconstruction is gonna fail eventually.
         */
        failure = "not recognized";
    } else {
        mpz_lcm(state->denom_lcm, state->denom_lcm, c->q->c);
        if ((mpfr_prec_t) mpz_sizeinbase(state->denom_lcm, 2) >= fprec(c->cx)) {
            failure = "brings lcm to above precision";
        }
    }

    if (failure) {
        lprintf(LOG_DEBUG "[X^%d] %s %s, setting hold\n",
                c->i, state->poly_name, failure);
        state->hold = 1;
        /* Mark this coefficient as ``todo'', even though we now concede
         * that we're not going to look at it again. */
        state->status[c->i] = 0;
        state->nworking--;
        state->ntodo++;
    } else {
        state->ndone++;
        state->nworking--;
        state->status[c->i] = -1; /* done */
    }

    state->client_time += c->ttc;
    return 0;
}
/*}}}*/

static int reconstruction_next_job(reconstruction_status_ptr state)/*{{{*/
{
    for(state->last-- ; state->last >= 0 && state->status[state->last] ; state->last--) {
        lprintf(LOG_INFO "[X^%d] %s has status %d\n", state->last, state->poly_name, state->status[state->last]);
    }
    ASSERT_ALWAYS(state->last >= 0);
    ASSERT_ALWAYS(state->status[state->last] == 0);
    return state->last;
}
/*}}}*/

static void reconstruction_prepare_for_coeff(reconstruction_status_ptr state, reconstruction_coeff_result_ptr c, int i, int orbit, int inum, int iter)/*{{{*/
{
    c->i = i;
    c->q = state->H[c->i];
    c->ttc = 0;
    c->cx = mpfrx_get_coeff(state->Hr, c->i);
    mpz_set (c->premult, state->premult);
    c->orbit = orbit;
    c->inum = inum;
    c->iter = iter;

    state->status[c->i] = 1; /* working */
    state->nworking++;
    state->ntodo--;
}/*}}}*/

static int reconstruction_do_one_coeff(struct cm_data * K, reconstruction_coeff_result_ptr c)/*{{{*/
{
   /* This is the job which gets run on each client node, only to do the
    * rational reconstruction */
   int ok = 0, loaded;

   c->ttc -= cputime ();

   loaded = load_quadratic_coefficient (c->q, K, c->orbit, c->inum, c->iter, c->i);
   assert (loaded != 0);
   if (loaded != -1)
      ok = 1;
   if (!ok) {
      mpfr_mul_z(c->cx, c->cx, c->premult, GMP_RNDN);
      ok = quadratic_number_recognize(c->q, c->cx, K->dr);
   }
   if (!ok) {
      mpz_set_ui(c->q->a, 0);
      mpz_set_ui(c->q->b, 0);
      mpz_set_ui(c->q->c, 0);
   }
   else if (loaded == -1)
      save_quadratic_coefficient (K, c->orbit, c->inum, c->iter, c->i, c->q);

   if (loaded == -1)
      c->ttc += cputime ();
   else
      c->ttc = -1; /* just as an indicator in the log file */

   return ok;
}/*}}}*/

static int recognise_quadratic_coefficients (struct cm_data *K,/*{{{*/
   mpfrx_srcptr H, int orbit, int inum, int iter)
   /* H is the floating point approximation of the inum-th class polynomial
      in O. The function tries to recognise its coefficients as algebraic
      numbers in the real quadratic subfield of the reflex field.
      It is assumed that the process has been carried out with success
      already on all class polynomials with index strictly less than inum.
      The return value is a boolean indicating the success of the operation.
      In case of success, the symbolic polynomial is stored in O, as well as
      the factorisation of the denominator.
   */
{
    int res = 1;
    struct cm_orbit_data *O = &(K->orbits [orbit]);
    int ncoeffs = O->nclasses;
    mpz_t scale;

    int size = 1;
#ifdef WITH_MPI
    MPI_Comm_size(MPI_COMM_WORLD, &size);
#endif

    mpz_init (scale);
    if (inum == 0)
        mpz_set_ui (scale, 1);
    else if (inum == 1) {
        /* Do +2 on each prime factor. This usually corresponds to the
         * powers encountered in the denominators of H2hat and H3hat */
        factor_matrix f = factor_matrix_dup(O->Hdenom_fact[0]);
        for(int i = 0 ; f[i][0] ; i++) f[i][1] += 2;
        factor_matrix_fold(scale, f);
        factor_matrix_free(f);
    } else
         mpz_set (scale, O->Hdenom [1]);

    reconstruction_status state;
    reconstruction_status_init(state, ncoeffs, size);

    state->Hr = H;
    state->H = O->H[inum];
    state->hecke = (inum > 0);
    state->poly_name = get_polyname (inum);

    mpz_set (state->premult, scale);

    reconstruction_coeff_result c;
    reconstruction_coeff_result_init(c);


    /* status:
     *  0 todo
     * -1 done
     *  1 working
     */
    state->total_time -= hires_wct();
    for( ; (state->ntodo && !state->hold) || state->nworking ; ) {
#ifdef WITH_MPI
        char client_id[CM2_CLIENT_ID_BYTES];
        int tagmask = (state->ntodo && !state->hold) ? MPI_ANY_TAG : CM2_MPI_JOB_DONE_TAG;
        int who;
        int what = reconstruction_what_is_happening(&who, client_id, tagmask);

        if (what == CM2_MPI_JOB_DONE_TAG) {
            lprintf(LOG_DEBUG_MORE "receiving rp result from %s\n", client_id);
            reconstruction_receive_coeff_result(c, state, who);
            lprintf(LOG_DEBUG_MORE "doing reconstruction_per_coeff for coeff computed by %s\n", client_id);
            reconstruction_per_coeff(state,c);
            lprintf(LOG_DEBUG_MORE "out of reconstruction_per_coeff for coeff computed by %s\n", client_id);
            double x = cputime();
            lprintf(LOG_DEBUG_MORE "done status_string_from_coeff_job (%.2f) for coeff computed by %s\n", cputime()-x, client_id);
            lprintf(LOG_PROGRESS
                    "(%d,%d,%d%s) [X^%d] %s <-- #%d %s (%.1f) denom=%d bits, %s %s\n",
                    state->ndone,
                    state->nworking,
                    state->ntodo,
                    state->hold?"*":"",
                    c->i, state->poly_name,
                    who, client_id,
                    c->ttc,
                    mpz_sizeinbase(state->denom_lcm, 2),
                    state->status[c->i] ? "keep going" : "too large",
                    progress_string);
            continue;
        } else if (what == CM2_MPI_READY_TAG) {
            int i = reconstruction_next_job(state);
            reconstruction_prepare_for_coeff(state, c, i, orbit, inum, iter);
            reconstruction_send_coeff_job(state, c, who, client_id);
        }
#else
        int i = reconstruction_next_job(state);
        reconstruction_prepare_for_coeff(state, c, i, orbit, inum, iter);
        reconstruction_do_one_coeff(K, c);
        reconstruction_per_coeff(state, c);
        lprintf(LOG_PROGRESS
                "(%d,%d,%d%s) [X^%d] %s <-- (%.1f) denom=%d bits, %s %s\n",
                state->ndone,
                state->nworking,
                state->ntodo,
                state->hold?"*":"",
                c->i, state->poly_name,
                c->ttc,
                mpz_sizeinbase(state->denom_lcm, 2),
                state->status[c->i] ? "keep going" : "too large",
                progress_string);
#endif
    }
    state->total_time += hires_wct();

    res = state->ntodo == 0;

    lprintf(LOG_PROGRESS "Time for recognizing %d coeffs of %s (%s): %.2f (%.0f%% cpu, %.0f%% failed attempts)\n",
            state->ndone, state->poly_name,
            res ? "success" : "failure",
            state->total_time,
            state->total_time ? 100.0 * state->client_time / state->total_time : 0,
            state->client_time ? 100.0 * state->vain_time / state->client_time : 0);

    if (res) {
        mpz_set (O->Hdenom [inum], state->denom_lcm);

        /* For H1, this means all coeffs except the leading coeffs.
         * For H2h and H3h, this means all coeffs */
        for(int i = 0 ; i < ncoeffs ; i++) {
            quadratic_number_ptr q = state->H[i];
            mpz_divexact(q->c, O->Hdenom [inum], q->c);
            mpz_mul(q->a, q->a, q->c);
            mpz_mul(q->b, q->b, q->c);
            mpz_set_ui(q->c, 1);
        }
#if 1
        /* All quadratic numbers appearing in the coefficients have
         * _already_ been multiplied by scale. So we do not have to
         * multiply *again* by scale. This being said, scale
         * still has to appear in the denominator.
         */
        mpz_mul (O->Hdenom [inum], scale, O->Hdenom [inum]);
#endif

        /* Note that the leading coefficient of H1 is completely
         * fabricated from this code, so we have to put the complete
         * denominator here */
        if (!state->hecke) {
            quadratic_number_ptr lc = O->H[inum][DEGH(O,inum)];
            mpz_set(lc->a, O->Hdenom [inum]);
            mpz_set_ui(lc->b,0);
            mpz_set_ui(lc->c,1);
        }

        /* Small optimization: Try to make the output more canonical, by
         * avoiding spurious factors in the denominator */
        if (state->hecke) {
            mpz_t g;
            mpz_init_set_ui(g, 0);
            for(int i = 0 ; i < ncoeffs && mpz_cmp_ui(g, 1) != 0 ; i++) {
                quadratic_number_ptr q = state->H[i];
                mpz_gcd(g,g,q->a);
                mpz_gcd(g,g,q->b);
            }
            if (mpz_cmp_ui(g, 1) > 0) {
                /* Note that it is quite common to encounter non-trivial
                 * content in the numerator; remove gcd with denominator. */
                mpz_gcd (g, g, O->Hdenom [inum]);
                if (mpz_cmp_ui (g, 1) > 0) {
                    char * pfact;
                    factor_matrix fm = factor_trialdiv(g, K->denom_prime_bound);
                    factor_matrix_asprintf (&pfact, fm);
                    factor_matrix_free(fm);
                    lprintf(LOG_ERROR "Removed common factor %s from coefficients of %s (found in num. and den.)\n",
                            pfact,
                            state->poly_name);
                    free(pfact);
                    mpz_divexact (O->Hdenom [inum], O->Hdenom [inum], g);
                }
                for(int i = 0 ; i < ncoeffs ; i++) {
                    quadratic_number_ptr q = state->H[i];
                    mpz_divexact(q->a, q->a, g);
                    mpz_divexact(q->b, q->b, g);
                }
            }
            mpz_clear (g);
        }

        /* Factor the leading coefficient */
         double t;
         char *pfact;
         t = -cputime();
         O->Hdenom_fact [inum] = factor_trialdiv(O->Hdenom [inum],
                                 K->denom_prime_bound);
         t += cputime();
         if (O->Hdenom_fact [inum] == NULL) {
             lprintf(LOG_PROGRESS "Non-smooth denominator %Zd\n", O->Hdenom [inum]);
             res = 0;
         } else {
             ASSERT_ALWAYS(O->Hdenom_fact [inum] != NULL);
             factor_matrix_asprintf (&pfact, O->Hdenom_fact [inum]);
             lprintf(LOG_NORMAL "lc(%s)=%s\n", get_polyname (inum), pfact);
             free (pfact);
             lprintf(LOG_PROGRESS "Time for factoring denominator of %s: %.2f\n",
                      get_polyname (inum), t);
         }
    }

    mpz_clear (scale);

    reconstruction_status_clear(state);
    reconstruction_coeff_result_clear(c);

    return res;
}/*}}}*/

static int is_valid_complex_tuple(mpc_t * v, int n)/*{{{*/
{
    for(int i = 0 ; i < n; i++) {
        if (!mpfr_number_p(MPC_RE(v[0])) || !mpfr_number_p(MPC_IM(v[0])))
            return 0;
    }
    return 1;
}/*}}}*/


static int cm_period_invariants_from_theta(struct cm_period_data * p,/*{{{*/
                                           const int invariant_set)
{
   mpc_t th2[10];
   for(int i = 0 ; i < 10 ; i++)
      cinit(th2[i], cprec(p->b[0]));

   /* Given the three quotients of theta-constants at Omega/2, obtain ten
    * quotients of theta squares at Omega */
   {
       mpc_t th [4];
       for (int i = 0; i < 4; i++)
          cinit (th [i], cprec(p->b[0]));
       for (int i = 1; i < 4; i++)
          cswap (th [i], p->b[i-1]);
       cone(th[0]);
       get_10theta2_from_4thetatauhalf (th2, th);
       for (int i = 1; i < 4; i++)
          cswap (th [i], p->b[i-1]);
       for (int i = 0; i < 4; i++)
          cclear (th [i]);
   }

   /* We acknowledge the fact that some of b[] is garbage. Do not keep
    * it for the invariants */
   for(int i = 0 ; i < 3 ; i++)
      cset_prec(p->j[i], p->probably_correct_bits);
   invariants_from_th2(p->j, th2, invariant_set);
   for(int i = 0 ; i < 10 ; i++)
      cclear(th2[i]);

   if (is_valid_complex_tuple(p->j, 3))
      return 1;
   for(int i = 0 ; i < 3 ; i++) {
      /* In this case, make it really clear that it's invalid */
      mpc_set_nan(p->j[i]);
   }
   p->probably_correct_bits = 0;
   return 0;
}/*}}}*/


static void cm_period_compute_tau(mpc_t t[3], struct cm_data * K, struct cm_period_data * p)
   /* Computes a complex approximation to the symbolic period matrix p->pm_t
      and returns it via t, all components of which need to already be initialised
      at the desired precision. */
{
    mpc_t z;
    mpfr_t r;
    mpfr_prec_t prec = mpfr_get_prec (mpc_realref (t [0]));
    finit(r, prec);
    cinit(z, prec);

    /* This avoids recomputation if K->r is already known to sufficient
     * precision */
    cm_data_set_real_root (K, prec);

    fset_ui(MPC_RE(z), 0);
    fset(MPC_IM(z), K->r);

    for(int i = 0 ; i < 3 ; i++) {
        cset_z(t[i], p->pm_t[i][3]);
        for(int k = 2 ; k >= 0 ; k--) {
            cmul(t[i], t[i], z);
            fadd_z(MPC_RE(t[i]), MPC_RE(t[i]), p->pm_t[i][k]);
        }
        fdiv_z(MPC_RE(t[i]), MPC_RE(t[i]), p->pm_denom);
        fdiv_z(MPC_IM(t[i]), MPC_IM(t[i]), p->pm_denom);
    }

    sanitize_tau(t);
    mpfr_clear(r);
    cclear(z);
}


static void update_progress_string(const char * fmt, ...)/*{{{*/
{
    va_list ap;
    va_start(ap, fmt);
#ifdef WITH_MPI
    size_t len = strlen(progress_string);
    if (len >= sizeof(progress_string) - 1)
        return;
    char * ps = progress_string + len;
    vsnprintf(ps, sizeof(progress_string) - len, fmt, ap);
#else
    char * tmp = NULL;
    int rc = asprintf(&tmp, LOG_PROGRESS LOG_NOTIMESTAMP "%s", fmt);
    ASPRINTF_CHECK(rc);
    lvprintf(tmp, ap);
    free(tmp);
#endif
    va_end(ap);
}/*}}}*/


static void cm_period_refine (struct cm_data *K, struct cm_period_data *p)/*{{{*/
{
   /* refine the triple of quotients of squares of theta constants,
   * reaching some larger precision.
   *
   * TODO: We want some target precision to be fed in. Something
   * realistic. Certainly not something more that 2*prec(b[]). At the
   * moment, we lift, and see what we get. This isn't too smart, as we
   * could make better use of our cpu cycles.
   */

   (p->iter)++;
   /* Try to read the period from a file */
   int loaded = load_period (p, K, p->orbit, p->no, p->iter);
   if (loaded == -1)
      lprintf (LOG_DEBUG "File not existant period %i of orbit %i in iteration %i\n", p->no, p->orbit, p->iter);
   else if (loaded == 1)
      lprintf (LOG_DEBUG "Correctly loaded period %i of orbit %i in iteration %i\n", p->no, p->orbit, p->iter);
   else
      lprintf (LOG_DEBUG "Error in loading period %i of orbit %i in iteration %i\n", p->no, p->orbit, p->iter);

   if (loaded != 1) {
      /* do refine step */
      mpc_t tau [3], b1[3];

      /* We know that some bits of p->b are probably wrong, as per
      * p->probably_correct_bits. However, we carry out the computations
      * with precision 2*prec(b), and only later do we truncate the
      * result by stripping twice as many bits as we've seen incorrect
      * bits in p->b (judging by consistency with the lift result).
      */

      mpfr_prec_t oprec = cprec(p->b[0]);
      mpfr_prec_t nprec = 2*cprec(p->b[0]);
      for(int i = 0 ; i < 3 ; i++) {
         cinit(tau[i], nprec);
         cinit(b1[i], nprec);
      }
      cm_period_compute_tau(tau, K, p);

      newtonstep_3thetaq (b1, p->b, tau, newton_method);

      /* How many correct bits did we have in p->b ? */
      mpfr_prec_t correct_b = agreeing_bits_n(b1, p->b, 3);

      assert (correct_b > p->base_prec / 2);

      /* Truncate so as to keep a constant amount of garbage */
      nprec = nprec - 2 * (oprec - correct_b);
      /* And anticipate for the rest of the computation */
      p->probably_correct_bits = nprec - (oprec - correct_b);

      for(int i = 0 ; i < 3 ; i++) {
         cprec_round(b1[i], nprec);
         cswap(p->b[i], b1[i]);
         cclear(tau[i]);
         cclear(b1[i]);
      }

      cm_period_invariants_from_theta (p, K->invariant_set);
      
      save_period (K, p->orbit, p->no, p->iter, p);
   }
}/*}}}*/


static void cm_orbit_compute_low_precision_stuff(struct cm_data * K, struct cm_orbit_data * O)/*{{{*/
{
   mpc_t tmp;
   mpc_t tau [3], th [4], th2 [10];
   cinit (tmp, BASE_PREC);
   for (int k = 0; k < 3; k++)
      cinit (tau [k], BASE_PREC);
   for (int k = 0; k < 4; k++)
      cinit (th [k], BASE_PREC);
   for (int k = 0; k < 10; k++)
      cinit (th2 [k], BASE_PREC);

   for(int i = 0 ; i < O->nperiods ; i++) {
      struct cm_period_data * p = &(O->periods[i]);
      update_progress_string(".");
      p->base_prec = BASE_PREC;
      cm_period_compute_tau(tau, K, p);
      for(int i = 0 ; i < 3 ; i++)
         cprec_round(p->b[i], BASE_PREC);
      /* NOTE: The period matrix tau/2 need not be reduced any more; in
       * any case, the imaginary part is still not too small. */
      for (int k = 0; k < 3; k++)
         cdiv_2ui (tau [k], tau [k], 1);
      eval_4theta_naive (th, tau);
          
      cinv (tmp, th[0]);
      for (int k = 0; k < 3; k++) {
         cmul (p->b [k], th[k+1], tmp);
      }

      p->probably_correct_bits = BASE_PREC; /* yeah, we're optimistic ! */
      cm_period_invariants_from_theta(p, K->invariant_set);
   }

   cclear (tmp);
   for (int k = 0; k < 3; k++)
      cclear (tau [k]);
   for (int k = 0; k < 4; k++)
      cclear (th [k]);
   for (int k = 0; k < 10; k++)
      cclear (th2 [k]);
}/*}}}*/


static void cm_compute_minmax_prec(struct cm_orbit_data * O)/*{{{*/
{
    O->maxprec = 0;
    O->minprec = INT_MAX;
    for(int i = 0 ; i < O->nperiods ; i++) {
        struct cm_period_data * p = &(O->periods[i]);
        if (cprec(p->b[0]) < O->minprec) O->minprec = cprec(p->b[0]);
        if (cprec(p->b[0]) > O->maxprec) O->maxprec = cprec(p->b[0]);
    }
}/*}}}*/

static void cm_orbit_refine_step(struct cm_data * K, struct cm_orbit_data * O, int iter) /* {{{ */
{
    cm_compute_minmax_prec(O);
    lprintf(LOG_PROGRESS
            "-----------------------------------"
            "-----------------------------------\n");
    lprintf(LOG_DEBUG "current: min prec %Pu, max prec %Pu\n", O->minprec, O->maxprec);

    /* XXX Should be 2 * O->minprec. More should be innocuous, in that
     * it'll force doubling for everybody */
    mpfr_prec_t cutoff = MPFR_PREC_MAX;
    // lprintf(LOG_DEBUG "Increasing to at least %Pu\n", cutoff);

    int * todo_list = malloc(O->nperiods * sizeof(int));
    int nb_refined = 0;
    for(int i = 0 ; i < O->nperiods ; i++) {
        struct cm_period_data * p = &(O->periods[i]);
        nb_refined += (todo_list[i] = cprec(p->b[0]) < cutoff);
    }

    double tt;

#ifdef  WITH_MPI
    tt=-hires_wct();
    lprintf(LOG_PROGRESS "Refining %d periods out of %d\n", nb_refined, O->nperiods);
    int ndone = 0, ntodo = nb_refined, nworking = 0;
    int size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    int * wlist = malloc(size * sizeof(int));
    int last = -1;
    double io_time = 0;
    double client_time = 0;
    for( ; ntodo || nworking ; ) {
        struct cm_period_data * p;
        int i;

        char client_id[CM2_CLIENT_ID_BYTES];
        MPI_Status status;
        MPI_Recv(client_id, sizeof(client_id), MPI_BYTE,
                MPI_ANY_SOURCE,
                ntodo ? MPI_ANY_TAG : CM2_MPI_JOB_DONE_TAG,
                MPI_COMM_WORLD, &status);
        int what = status.MPI_TAG;
        int who = status.MPI_SOURCE;
        if (what == CM2_MPI_JOB_DONE_TAG) {
            i = wlist[who];
            p = &(O->periods[i]);
            lprintf(LOG_DEBUG_MORE "receiving newton step from %s (period #%d)\n", client_id, i);
            double ttc;
            io_time -= hires_wct();
            MPI_Bcast(&ttc, 1, MPI_DOUBLE, 1, pals->c[who]);
            lprintf(LOG_DEBUG_MORE "receiving newton step [progress string] from %s (period #%d)\n", client_id, i);
            MPI_Bcast(progress_string, sizeof(progress_string), MPI_BYTE, 1, pals->c[who]);
            mpfr_prec_t old_prec = cprec(p->b[0]);
            lprintf(LOG_DEBUG_MORE "receiving newton step [data partial] from %s (period #%d)\n", client_id, i);
            broadcast_cm_data_partial(K, 1, pals->c[who]);
            lprintf(LOG_DEBUG_MORE "receiving newton step [period data] from %s (period #%d)\n", client_id, i);
            broadcast_cm_period_data(p, 1, pals->c[who]);
            io_time += hires_wct();
            ndone++;
            nworking--;
            lprintf(LOG_PROGRESS
                    "(%d,%d,%d) p%d <-- #%d %s [%Pu -> %Pu] (%.1f) %s\n",
                    ndone,nworking,ntodo,
                    i, who, client_id,
                    old_prec, cprec(p->b[0]),
                    ttc,
                    progress_string);
            client_time += ttc;
#if 0
            /* By uncommenting the following line, the period is forgotten
               as soon as it is received. The clients will still write the "P"
               checkpoint files, but the "N" file written by the master will
               be empty. The aim is to compute only one Newton iteration
               without using too much memory on the master. This can help push
               records on homogeneous clusters with little main memory.
               Afterwards, comment the line again, delete the empty "N" file
               and run again on a machine with more memory, to assemble the
               "P" files into one "N" file and to do the polynomial
               reconstruction.
            */
            cm_period_clear (p);
#endif
            continue;
        } else if (what == CM2_MPI_READY_TAG) {
            for(last++ ; last < O->nperiods && !todo_list[last] ; last++) ;
            ASSERT_ALWAYS(last < O->nperiods);
            i = last;
            p = &(O->periods[i]);
            p->iter = iter;
            nworking++;
            ntodo--;
            wlist[who] = i;
            lprintf(LOG_PROGRESS
                    "(%d,%d,%d) p%d --> #%d %s [%Pu]\n",
                    ndone,nworking,ntodo,
                    i, who, client_id, cprec(p->b[0]));
            int todo = CM2_TODO_COMPUTE_THETA_CONSTANTS;
            io_time -= hires_wct();
            MPI_Bcast(&todo, 1, MPI_INT, 0, pals->c[who]);
            broadcast_mp_prec(&cutoff, 0, pals->c[who]);
            broadcast_cm_data_partial(K, 0, pals->c[who]);
            broadcast_cm_period_data(p, 0, pals->c[who]);
            io_time += hires_wct();
        }
    }

    free(wlist);

    tt+=hires_wct();
    lprintf(LOG_PROGRESS
            "Total for %d periods: %.1f wct, %.1f cpu (%.1f%%),"
            " %.1f I/O (%.1f%%)\n",
            nb_refined,
            tt,
            client_time, 100.0 * client_time / tt,
            io_time, 100.0 * io_time / tt);
#else
    tt=-cputime();
    lprintf(LOG_PROGRESS "Refining %d periods out of %d:", nb_refined, O->nperiods);
    lprintf(LOG_DEBUG LOG_NOTIMESTAMP "\n");
    for(int i = 0 ; i < O->nperiods ; i++) {
        struct cm_period_data * p = &(O->periods[i]);
        if (!todo_list[i])
            continue;
        lprintf(LOG_CHATROOM "%d/%d ", i, O->nperiods);
        lprintf(LOG_PROGRESS LOG_NOTIMESTAMP ".");
        // lprintf(LOG_PROGRESS "%d %Pd %d %d ", i, p->xprec, p->delta, p->extra);
        progress_string[0]='\0';
        p->iter = iter;
        cm_period_refine(K, p);
        lprintf(LOG_PROGRESS LOG_NOTIMESTAMP "%s", progress_string);
        lprintf(LOG_CHATROOM LOG_NOTIMESTAMP "\n");
    }
    tt+=cputime();
    lprintf(LOG_PROGRESS LOG_NOTIMESTAMP ": %.2fs\n", tt);
#endif

    free(todo_list);
    todo_list = NULL;

    cm_compute_minmax_prec(O);
    iter++;
    lprintf(LOG_PROGRESS "Iteration %i: min prec %Pu, max prec %Pu\n",
            iter, O->minprec, O->maxprec);
}/*}}}*/

static void cm_data_read_input(struct cm_data * K, FILE * infile)/*{{{*/
{
    int rc;
    int x;
    rc = fscanf(infile, "%d", &x);
    if (rc != 1) {
        fprintf(stderr, "Could not read in cm_data_read_input !!!\n");
        abort();
    }
    if (x < 0) {
        inputfile_format = x;
        rc = fscanf(infile, "%d", &K->DAB[0]);
        if (rc != 1) {
            fprintf(stderr, "Could not read in cm_data_read_input !!!\n");
            abort();
        }
    } else {
        K->DAB[0] = x;
    }
    rc = fscanf(infile, "%d", &K->DAB[1]); if (rc != 1) abort();
    rc = fscanf(infile, "%d", &K->DAB[2]); if (rc != 1) abort();

    lprintf(LOG_NORMAL "Considering CM field given by invariants [D,A,B]=[%d,%d,%d]\n",
            K->DAB[0], K->DAB[1], K->DAB[2]);
    // need to consider the complex roots of X^4+AX^2+B -- IOW first the
    // real roots of X^2+AX+B (both must be negative), and later their
    // imaginary square roots.

    cm_data_init_aux(K);

    if (inputfile_format > -3) {
        fprintf(stderr, "This version of the code is not compatible with pre-v3 input files\n");
        exit(1);
    }
    if (inputfile_format <= -2) {
        rc = fscanf(infile, "%d", &K->DAB_r[0]); if (rc != 1) abort();
        rc = fscanf(infile, "%d", &K->DAB_r[1]); if (rc != 1) abort();
        rc = fscanf(infile, "%d", &K->DAB_r[2]); if (rc != 1) abort();
        if (!K->DAB_r[1] && !K->DAB_r[2]) {
            /* Galois case, print it as such */
            ASSERT_ALWAYS(K->DAB_r[0] == K->DAB[0]);
            lprintf(LOG_NORMAL "reflex CM field has invariants [D,A,B]=[%d,%d,%d] (Galois, hence identical)\n",
                K->DAB_r[0], K->DAB[1], K->DAB[2]);
        } else {
            lprintf(LOG_NORMAL "reflex CM field has invariants [D,A,B]=[%d,%d,%d]\n",
                    K->DAB_r[0], K->DAB_r[1], K->DAB_r[2]);
        }
    } else {
        lprintf(LOG_NORMAL "reflex CM field has invariants [D,A,B]=[%d,%d,%d] (warning: not a minimal representative)\n",
                K->DAB_r[0], K->DAB_r[1], K->DAB_r[2]);
    }

    cm_data_consistency_check(K);
    cm_read_orbits(K, infile);

    int maxbits_coeffs = 0;
    for(int i = 0 ; i < K->ncosets ; i++) {
        struct cm_orbit_data * O = &(K->orbits[i]);
        for(int i = 0 ; i < O->nperiods ; i++) {
            struct cm_period_data * p = &(O->periods[i]);
            for(int i = 0 ; i < 3 ; i++) {
                for(int k = 0 ; k < 4 ; k++) {
                    mp_size_t b = mpz_sizeinbase(p->pm_t[i][k],2);
                    if (b > maxbits_coeffs)
                        maxbits_coeffs = b;
                }
            }
        }
    }

    /* It's not exactly appropriate. We should rather look more closely
     * at the derivatives.
     */
    if (BASE_PREC < maxbits_coeffs) {
        lprintf(LOG_IMPORTANT "Warning: base prec (%d) increased to"
                " largest coefficient data (%d)\n",
                (int) BASE_PREC, maxbits_coeffs);
        BASE_PREC = maxbits_coeffs;
    }
    /* Pehaps look at the derivative of the expressions, to see which
     * precision is needed to avoid cancellations.
     */
}/*}}}*/


static void reconstruct_class_polynomials (mpfrx_t *H, struct cm_data *K,/*{{{*/
   int orbit, mpfr_prec_t *prec, const int iter)
   /* Carries out the reconstruction step to obtain the floating point
         approximation of the three polynomials in the given orbit; the
         working precision is prec.
      The polynomials are returned in H[0], ..., H[2] (which need to be
         initialised).
      prec is modified if the precision is not taken from this input
         parameter, but read from a checkpoint file.
      iter is just passed through to determine the file name for
         checkpointing.
   */
{
   struct cm_orbit_data * O = &(K->orbits [orbit]);
   mpfrx_t **vals;
   int level;
   int no_pols, no_factors;
   int inum, i;
   double time;

   lprintf(LOG_NORMAL "Beginning polynomial reconstruction at precision %Pu\n",
           *prec);

   /* Check for checkpoint file. */
   level = get_level_producttrees (K, orbit, iter);
   if (level != -1) {
      load_producttrees (&no_pols, &no_factors, &vals, K, orbit, iter, level);
      *prec = mpfrx_get_prec (vals [0][0]);
   }
   else {
      /* set all leaves */
      no_pols = 3;
      no_factors = O->nperiods;
      vals = (mpfrx_t **) malloc (no_pols * sizeof (mpfrx_t *));
      for (inum = 0; inum < 3; inum++)
         vals [inum] = (mpfrx_t *) malloc (no_factors * sizeof (mpfrx_t));
      for (i = 0; i < no_factors; i++) {
         int deg = O->periods [i].field;
         mpc_srcptr j = O->periods [i].j [0];
         mpfrx_ptr P = vals [0][i];
         mpfrx_init (P, deg + 1, *prec);
         mpfrx_set_deg (P, deg);
         mpfr_set_ui (mpfrx_get_coeff (P, deg), 1, MPFR_RNDN);
         if (deg == 1)
            mpfr_neg (mpfrx_get_coeff (P, 0), mpc_realref (j), MPFR_RNDN);
         else /* deg == 2 */ {
            mpfr_mul_si (mpfrx_get_coeff (P, 1), mpc_realref (j), -2, MPFR_RNDN);
            mpc_norm (mpfrx_get_coeff (P, 0), j, MPFR_RNDN);
         }
      }
      for (inum = 1; inum < 3; inum++) {
         for (i = 0; i < no_factors; i++) {
            int deg = O->periods [i].field - 1;
            mpc_srcptr j = O->periods [i].j [inum];
            mpfrx_ptr P = vals [inum][i];
            mpfrx_init (P, deg + 1, *prec);
            mpfrx_set_deg (P, deg);
            if (deg == 0)
               mpfrx_set_coeff (P, 0, mpc_realref (j));
            else /* deg == 1 */ {
               mpfr_srcptr rj = mpc_realref (j);
               mpfr_srcptr ij = mpc_imagref (j);
               mpfr_srcptr rj0 = mpc_realref (O->periods [i].j [0]);
               mpfr_srcptr ij0 = mpc_imagref (O->periods [i].j [0]);
               /* compute (X-j0)*jbar + (X-j0bar)*j
                  = 2 rj X - 2 (rj*rj0 + ij*ij0)    */
               mpfrx_set_deg (P, 1);
               mpfr_ptr p1 = mpfrx_get_coeff (P, 1);
               mpfr_ptr p0 = mpfrx_get_coeff (P, 0);
               mpfr_mul (p1, rj, rj0, MPFR_RNDN); /* used as temporary variable */
               mpfr_mul (p0, ij, ij0, MPFR_RNDN);
               mpfr_add (p0, p0, p1, MPFR_RNDN);
               mpfr_mul_si (p0, p0, -2, MPFR_RNDN);
               mpfr_mul_2ui (p1, rj, 1, MPFR_RNDN);
            }
         }
         level = 0;
      }
   }

   for (inum = 0; inum < 3; inum++)
      mpfrx_set_prec (H [inum], *prec);

#ifdef WITH_MPI
   time = -hires_wct();
   mpi_mpfrx_server_start ();
   mpi_mpfrx_server_product_and_hecke (H, vals, no_pols, no_factors, level,
      K, orbit, iter);
   mpi_mpfrx_server_stop ();
   time += hires_wct();
#else
   time = -cputime();
   mpfrx_product_and_hecke (H, vals, no_pols, no_factors);
   time += cputime();
#endif
   lprintf(LOG_PROGRESS "Polynomial reconstruction at precision %lu: %.2f\n",
           *prec, time);

   for (inum = 0; inum < no_pols; inum++) {
      for (i = 0; i < no_factors; i++)
         mpfrx_clear (vals [inum][i]);
      free (vals [inum]);
   }
   free (vals);
}/*}}}*/


static void compute_class_polynomials (struct cm_data *K, struct cm_orbit_data * O, FILE * outstream)/*{{{*/
{
   /* Computes the class polynomials for the orbit with number orbit. */
   /* if outputname_temp is given, the result is written immediately to
    * that file. if not, and if checkpoints are allowed, then temp files
    * are written in the checkpoints directory */
   int orbit = O - K->orbits;  /* ptrdiff_t, really */
   int inum, iter, ok;
   mpfr_prec_t prec;
   int orbit_read = 0;
   int done_quadratic [3], failed_recognition [3], done_real [] = {0, 0, 0};

   /* Determine highest iteration with checkpoint files */
   make_checkpoint_directory (K);
   iter = get_iteration (K, orbit);
   lprintf (LOG_NORMAL "Starting with iteration %i\n", iter);

   long offset = 0;
   if (outstream) {
       offset = ftell(outstream);
       if (offset < 0) {
           fprintf(stderr, "output stream is not seekable\n");
           perror("ftell");
           abort();
       }
   }


   do {
       /* make sure that we clobber our output file each time an
        * inconsistency appeared in the previous attempt */
       if (outstream) {
           fflush(outstream);
           int rc;
           rc = fseek(outstream, offset, SEEK_SET);
           if (rc < 0) { perror("fseek"); exit(1); }
           rc = ftruncate(fileno(outstream), offset);
           if (rc < 0) { perror("ftruncate"); exit(1); }
           rc = fseek(outstream, offset, SEEK_SET);
           if (rc < 0) { perror("fseek"); exit(1); }
       }
      for (inum = 0; inum < 3; inum++) {
         done_quadratic [inum] = load_polynomial_quadratic (K, orbit, inum);
         assert (done_quadratic [inum] != 0);
         failed_recognition [inum] =
            load_recognition_failed (K, orbit, inum, iter);
         assert (failed_recognition [inum] != 0);
      }
      if (   done_quadratic [0] == 1 && done_quadratic [1] == 1
          && done_quadratic [2] == 1)
         ok = 1; /* finished! */
      else if (failed_recognition [0] == 1 || failed_recognition [1] == 1
               || failed_recognition [2] == 1)
         ok = 0; /* quadratic reconstruction failed already, we need to
                    do a Newton step */
      else {
         ok = 1; /* be optimistic! */
         mpfrx_t H [3];
         for (inum = 0; inum < 3; inum++)
            mpfrx_init (H [inum], 2, 2);
#if 0
         /* Uncomment the following lines to not read certain of the floating
            point polynomials. This allows to do the coefficient recognition
            only for H1, say, without losing time in reading H2hat and H3hat.
            Meant for record computations with manual intervention.
         */
         done_quadratic [1] = 1;
         done_quadratic [2] = 1;
#endif
         /* try to read the floating point polynomials from their files */
         for (inum = 0; inum < 3; inum++) {
            if (done_quadratic [inum] != 1) {
               done_real [inum] = load_polynomial_real (H [inum], K, orbit,
                                                        inum, iter);
               assert (done_real [inum] != 0);
            }
         }
         /* normally, either all or none of the polynomials are read; in
            any case, we recompute them all if one of them is missing */
         if (    (done_quadratic [0] != 1 && done_real [0] != 1)
              || (done_quadratic [1] != 1 && done_real [1] != 1)
              || (done_quadratic [2] != 1 && done_real [2] != 1)) {
            if (get_level_producttrees (K, orbit, iter) == -1) {
               /* Try to read orbit from checkpoint file; if no such file
                  exists, the low precision estimates are used. */
               if (!orbit_read) {
                  assert (load_orbit (O, K, orbit, iter) != 0);
                  orbit_read = 1;
               }
               prec = cm_orbit_invariants_precision (O);
               if (prec <= 2 || O->minprec < nobother_prec)
                  ok = 0;
            }
            else
               prec = 0;
               /* We do not know the precision yet; it is updated in
                  reconstruct_class_polynomials with the correct precision
                  read from the file.  */
               /* Not reading the orbit file also breaks the following line
                  in the polynomial output file, which is replaced by 0 0:
                  4046 4048: minimum and maximum precisions used for computing theta
                     constants associated to period matrices.
                  This could be repaired, but should we not rather drop the
                     information? */
            if (ok) {
               reconstruct_class_polynomials (H, K, orbit, &prec, iter);
               for (inum = 0; inum < 3; inum++) {
                  if (done_quadratic [inum] != 1 && done_real [inum] != 1)
                     save_polynomial_real (K, orbit, inum, iter, H [inum]);
               }
            }
         }
         /* recognise the quadratic coefficients, unless reconstruction
            has been disabled for the current precision */
         for (inum = 0; inum < 3 && ok; inum++) {
            if (done_quadratic [inum] != 1) {
               ok = ok && recognise_quadratic_coefficients (K, H [inum],
                             orbit, inum, iter);
               if (ok) {
                   if (io_allow_save) {
                       save_polynomial_quadratic (K, orbit, inum);
                   } else if (outstream) {
                       /* save it directly to the output file */
                       save_polynomial_quadratic_backend(outstream, K, orbit, inum);
                   }
               } else {
                  save_recognition_failed (K, orbit, inum, iter);
               }
            }
         }
         for (inum = 0; inum < 3; inum++)
            mpfrx_clear (H [inum]);
      }

      /* if quadratic recognition failed, do one Newton step */
      if (!ok) {
         if (!orbit_read) {
            assert (load_orbit (O, K, orbit, iter) != 0);
            orbit_read = 1;
         }
         cm_orbit_refine_step (K, O, iter);
         iter++;
         save_orbit (K, orbit, iter, O);
      }
      /* The output stream is invalid. It will be truncated at the next
       * iteration */
      if (outstream) fflush(outstream);
   } while (!ok);
}/*}}}*/

static void cm_main_loop(struct cm_data * K, const char *outputname)/*{{{*/
{
   char * outputname_temp = NULL;
   FILE * outstream = NULL;

   if (!io_allow_save && outputname) {
        int rc = asprintf(&outputname_temp, "%s.tmp", outputname);
        ASPRINTF_CHECK(rc);
        outstream = fopen(outputname_temp, "w");
        ASSERT_ALWAYS(outstream);
        save_polynomials_quadratic_standalone_header(outstream, K);
   }

       
   for(int i = 0 ; i < K->ncosets ; i++) {
      struct cm_orbit_data * O = &(K->orbits[i]);
      lprintf(LOG_NORMAL
               "##########"
               " Coset %d: %d periods: %d classes"
               " ############\n", i, O->nperiods, O->nclasses);

      /* Note that (at the moment) this step is not parallelized, so it
       * may have some cost */
      double tt=-cputime();
      lprintf(LOG_PROGRESS "Computing low-precision estimates");
      cm_orbit_compute_low_precision_stuff(K, &(K->orbits[i]));
      tt+=cputime();
      lprintf(LOG_PROGRESS LOG_NOTIMESTAMP ": %.2f\n", tt);

      compute_class_polynomials (K, O, outstream);

      long bitsize[3] = {0,};
      for(int inum = 0 ; inum < 3 ; inum++) {
            for(int i = 0 ; i <= DEGH(O, inum) ; i++) {
               bitsize[inum]+=mpz_sizeinbase(O->H[inum][i]->a, 2);
               bitsize[inum]+=mpz_sizeinbase(O->H[inum][i]->b, 2);
            }
      }
      lprintf(LOG_PROGRESS "Sizes for this class polynomial (bits): %ld+%ld %ld+%ld %ld+%ld\n",
               mpz_sizeinbase(O->Hdenom[0], 2), bitsize[0],
               mpz_sizeinbase(O->Hdenom[1], 2), bitsize[1],
               mpz_sizeinbase(O->Hdenom[2], 2), bitsize[2]);
   }

   if (outputname) {
       if (io_allow_save) {
           /* concatenate the checkpoints */
           save_polynomials_quadratic (K, outputname);
       } else {
           fclose(outstream);
           /* bless our temp file with the final name */
           rename(outputname_temp, outputname);
           free(outputname_temp);
       }
   }
}/*}}}*/

#ifdef  WITH_MPI
static void cm_client_loop(struct cm_data * K)/*{{{*/
{
    char client_id[CM2_CLIENT_ID_BYTES];
    struct utsname u[1];
    uname(u);
    strncpy(client_id, u->nodename, CM2_CLIENT_ID_BYTES);
    struct cm_period_data p[1];
    memset(p, 0, sizeof(p));
    cm_period_init_aux(p);

    for( ; ; ) {
        client_id[CM2_CLIENT_ID_BYTES-1]='\0';
        progress_string[0]='\0';
        MPI_Send(client_id, CM2_CLIENT_ID_BYTES, MPI_BYTE,
                0, CM2_MPI_READY_TAG,
                MPI_COMM_WORLD);
        int todo;
        MPI_Bcast(&todo, 1, MPI_INT, 0, *pals->up);
        if (todo == CM2_TODO_COMPUTE_THETA_CONSTANTS) {
            mpfr_prec_t cutoff = 0;
            broadcast_mp_prec(&cutoff, 0, *pals->up);
            broadcast_cm_data_partial(K, 0, *pals->up);
            broadcast_cm_period_data(p, 0, *pals->up);
            double tt = cputime();
            cm_period_refine(K, p);
            tt = cputime() - tt;
            lprintf(LOG_DEBUG_MORE "%s sends back data\n", client_id);

            MPI_Send(client_id, CM2_CLIENT_ID_BYTES, MPI_BYTE,
                    0, CM2_MPI_JOB_DONE_TAG,
                    MPI_COMM_WORLD);
            MPI_Bcast(&tt, 1, MPI_DOUBLE, 1, *pals->up);
            MPI_Bcast(progress_string, sizeof(progress_string), MPI_BYTE, 1, *pals->up);
            broadcast_cm_data_partial(K, 1, *pals->up);
            broadcast_cm_period_data(p, 1, *pals->up);
        } else if (todo == CM2_TODO_RECOGNIZE_NUMBER) {

            mpfr_t cr;
            mpfr_init(cr);

            quadratic_number qr;
            quadratic_number_init(qr);

            lprintf(LOG_DEBUG_MORE "%s ready to receive coeff to recognize\n", client_id);
            reconstruction_coeff_result c;
            reconstruction_coeff_result_init(c);
            c->cx = cr;
            c->q = qr;

            reconstruction_receive_coeff_job(c);
            lprintf(LOG_DEBUG_MORE "%s has received coeff to recognize\n", client_id);
            reconstruction_do_one_coeff(K, c);
            lprintf(LOG_DEBUG_MORE "%s out of reconstruction_do_one_coeff\n", client_id);
            reconstruction_send_coeff_result(c, client_id);
            lprintf(LOG_DEBUG_MORE "%s out of reconstruction_send_coeff_result\n", client_id);

            reconstruction_coeff_result_clear(c);
            mpfr_clear(cr);
            quadratic_number_clear(qr);
        } else if (todo == CM2_TODO_MPFRX) {
            int rank;
            MPI_Comm_rank (MPI_COMM_WORLD, &rank);
            mpi_mpfrx_client_init ();
            lprintf (LOG_DEBUG "MPI_MPFRX %s started as %i\n", client_id, rank);
            mpi_mpfrx_client ();
            lprintf (LOG_DEBUG "MPI_MPFRX %s stopped as %i\n", client_id, rank);
        } else if (todo == CM2_TODO_BYE_BYE) {
            break;
        }
    }
    MPI_Barrier(MPI_COMM_WORLD);
}
/*}}}*/
static void cm_join_clients()/*{{{*/
{
    /* Join all client loops */
    int size;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    for(int i = 1 ; i < size ; i++) {
        char client_id[CM2_CLIENT_ID_BYTES];
        MPI_Status status;
        MPI_Recv(client_id, sizeof(client_id), MPI_BYTE,
                MPI_ANY_SOURCE, CM2_MPI_READY_TAG,
                MPI_COMM_WORLD, &status);
        int who = status.MPI_SOURCE;
        lprintf(LOG_PROGRESS "joined #%d %s\n", who, client_id);
        int todo = CM2_TODO_BYE_BYE;
        MPI_Bcast(&todo, 1, MPI_INT, 0, pals->c[who]);
    }
    MPI_Barrier(MPI_COMM_WORLD);
}/*}}}*/
#endif

static void signal_handling (int signum)/*{{{*/
{
   fprintf (stderr, "ERROR Received signal \"%s\"\n", strsignal (signum));

#ifdef  WANT_STACK_BACKTRACES
   int sz = 100, i;
   void *buffer [sz];
   char** text;

   sz = backtrace (buffer, sz);
   text = backtrace_symbols (buffer, sz);

   for (i = 0; i < sz; i++)
      fprintf (stderr, "BT %s\n", text [i]);

   signal (signum, SIG_DFL);
   raise (signum);
#endif
}/*}}}*/

int main(int argc, char * argv[])/*{{{*/
{
    /* XXX protect the use of this ? */
   signal (SIGABRT, signal_handling);

#ifdef  WITH_MPI
#define OMPI_IS_VERSION(x,y,z) defined(OMPI_MAJOR_VERSION) && OMPI_MAJOR_VERSION==x && OMPI_MINOR_VERSION==y && OMPI_RELEASE_VERSION==z
#if OMPI_IS_VERSION(1,4,3)
#error "Sorry, this version of MPI apparently has bugs in MPI_Comm_split, which make it unusable for this program. Please upgrade"
#endif
    MPI_Init(&argc, &argv);
#endif

    struct cm_data K[1];
    cm_data_init(K);

    const char * inputname = NULL;
    const char * outputname = NULL;
    FILE * infile = stdin;

    setbuf(stdout, NULL);
    setbuf(stderr, NULL);

    /* {{{ parse command line */
    argv++, argc--;
    for( ; argc ; ) {
        if (strncmp(argv[0], "-v", 2) == 0) {
            const char * s = argv[0] + 2;
            int l = strlen(s);
            if ((int) strspn(s, "v") == l) {
                loglevel++;
                loglevel += l;
                argv++, argc--;
                continue;
            } else if ((int) strspn(s, "0123456789") == l) {
                loglevel = atoi(s);
                argv++, argc--;
                continue;
            }
        }
        if (strncmp(argv[0], "-q", 2) == 0) {
            const char * s = argv[0] + 2;
            int l = strlen(s);
            if ((int) strspn(s, "q") == l) {
                loglevel--;
                loglevel -= l;
                argv++, argc--;
                continue;
            } else if ((int) strspn(s, "0123456789") == l) {
                loglevel = atoi(s);
                argv++, argc--;
                continue;
            }
        }
        if (strcmp(argv[0], "-i") == 0) {
            inputname = argv[1];
            argv++, argc--;
            argv++, argc--;
            continue;
        }
        if (strcmp(argv[0], "-o") == 0) {
            outputname = argv[1];
            argv++, argc--;
            argv++, argc--;
            continue;
        }
        if (strcmp(argv[0], "-b") == 0) {
            BASE_PREC = atoi(argv[1]);
            argv++, argc--;
            argv++, argc--;
            continue;
        }
        if (strcmp(argv[0], "-j") == 0) {
            K->invariant_set = atoi(argv[1]);
            argv++, argc--;
            argv++, argc--;
            continue;
        }
        if (strcmp(argv[0], "-n") == 0) {
            nobother_prec = atoi(argv[1]);
            argv++, argc--;
            argv++, argc--;
            continue;
        }
        if (strcmp(argv[0], "--newton-kind") == 0) {
            newton_method = atoi(argv[1]);
            argv++, argc--;
            argv++, argc--;
            continue;
        }
        if (strcmp(argv[0], "--checkpoints") == 0) {
            io_allow_save = 1;
            io_allow_load = 1;
            argv++, argc--;
            continue;
        }
        if (strcmp(argv[0], "--no-checkpoints") == 0) {
            io_allow_save = 0;
            io_allow_load = 0;
            argv++, argc--;
            continue;
        }
        fprintf(stderr, "%s: argument not understood\n", argv[0]);
        exit(1);
    }
    /* }}} */

    lprintf_init_timer();

    int master = 1;
#ifdef  WITH_MPI
    int size;
    int rank;
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    mpi_custom_types_check();
    mpi_comms_init(pals);
    master = rank == 0;
    if (size == 1) {
        fprintf(stderr, "You are running the MPI version of this program, which absolutely requires a master job and many slave jobs. Thus you must run this code via an appropriate mpiexec command line. Please refer to the documentation of your MPI implementation\n");
        exit(1);
    }

    char hostname [255];
    gethostname (hostname, 255);
    lprintf (LOG_NORMAL "Started node %s, rank %i/%i\n", hostname, rank, size);
#endif

    struct utsname u[1];
    uname(u);

    time_t now[1];
    double tw = hires_wct();
    double tc = cputime();
    time(now);

    if (master) {
        lprintf(LOG_NORMAL "Main job run on node %s, pid %lu, %s",
                u->nodename, (unsigned long int) getpid (), ctime(now));
        lprintf(LOG_NORMAL "version %s\n", version_string);
        int modelname=0;
        int mhz=0;
        FILE * f = fopen("/proc/cpuinfo","r");
        if (f) {
            for( ; !modelname || !mhz; ) {
                char line[160];
                if (fgets(line, sizeof(line), f) == NULL)
                    break;
                const char * key;
                key = "model name";
                if (!modelname && strncmp(line, key, strlen(key)) == 0) {
                    lprintf(LOG_NORMAL "%s", line);
                    modelname = 1;
                }
                key = "cpu MHz";
                if (!mhz && strncmp(line, key, strlen(key)) == 0) {
                    lprintf(LOG_NORMAL "%s", line);
                    mhz = 1;
                }
            }
            fclose(f);
        }

        if (inputname) {
            infile = fopen(inputname, "r");
            if (infile == NULL) { perror(inputname); exit(1); }
        } else {
            infile = stdin;
        }
        cm_data_read_input(K, infile);
        if (inputname)
            fclose(infile);
    }

#ifdef WITH_MPI
    /* Note that this does *not* broadcast the orbits */
    broadcast_cm_data_full(K, 0, MPI_COMM_WORLD);
#endif

#ifdef WITH_MPI
    if (master) {
        cm_main_loop(K, outputname);
        cm_join_clients();
    } else {
        cm_client_loop(K);
    }
#else
    /* TODO: do we support a cache file in this setting ? */
    cm_main_loop(K, outputname);
#endif


#ifdef  WITH_MPI
    mpi_comms_clear(pals);
#endif

    /* TODO: we've been sloppy on allocation of orbits, so cm_orbit_clear
     * will crash presently.
     * [20111206] is this still the case ???
     */
    cm_clear_orbits(K);
    cm_data_clear(K);
    mpfr_free_cache();

    if (master) {
        time(now);
        lprintf(LOG_NORMAL "Job done on node %s, %s", u->nodename, ctime(now));
        tw = hires_wct() - tw;
        tc = cputime() - tc;
        lprintf(LOG_NORMAL "Taken %.1f wct, %.1f cpu (%.1f%%) [%s]\n",
                tw, tc, tw ? 100.0*tc/tw : 0, u->nodename);
    }

#ifdef  WITH_MPI
    MPI_Finalize();
#endif
    return 0;
}/*}}}*/
