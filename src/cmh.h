#ifndef CMH_H_
#define CMH_H_

/* cmh.h -- public interfaces for cmh
 *
 * Copyright (C) 2013, 2017, 2018, 2022 INRIA
 *
 * This file is part of CMH.
 *
 * CMH is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * CMH is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/ .
 */

#include <mpc.h>

#ifdef __cplusplus
extern "C" {
#endif

/* This file lists the publicly exported interfaces of CMH. All
 * interfaces here are preliminary and subject to change without notice.
 *
 * Precision of results is always governed by the precision of the outpu
 * variables. In most cases however, we do not provide correct rounding.
 *
 */

/**************************************************************/
/* Computation of theta constants (with quadratic complexity) */

/* Functions below which take a period matrix argument denoted tau expect
 * it in the form of a three-coordinate vector of mpc_t numbers, where
 * the matrix is
 *
 *      [ tau[0]   tau[1] ]
 * tau =[ tau[1]   tau[2] ]
 *
 * The period matrix is always expected in the Siegel upper half space
 * (symmetric, and imaginary part positive definite). Pathological period
 * matrices which are very far from the fundamental domain may possibly
 * lead to misbehaviour, as this escapes the realm of tested input.
 *
 */

/* Theta constants: we use the following notation, for a=(a0,a1),
 * b=(b0,b1) both within {0,1}^2.
 *
 * theta_j(tau) = \theta_{a/2,b/2}(tau,z)
 *           = \sum_{n\in\Z^2}\exp(2*i*pi*(n+a/2)^T*tau*(n+a/2)+(n+a/2)^T*b)
 *
 * where j = 8a1 + 4a0 + 2b1 + b0.
 */

/* Four fundamental theta constants in tau */
/* This means:
 * theta_0 = theta_{((0,0),(0,0))}
 * theta_1 = theta_{((0,0),(1,0))}
 * theta_2 = theta_{((0,0),(0,1))}
 * theta_3 = theta_{((0,0),(1,1))}
 */
extern void eval_4theta_naive (mpc_t *th2, mpc_t *tau);

/* Squares of the ten even theta constants in tau */
/* This means:
 * theta_0^2 = theta_{((0,0),(0,0))}^2
 * theta_1^2 = theta_{((0,0),(1,0))}^2
 * theta_2^2 = theta_{((0,0),(0,1))}^2
 * theta_3^2 = theta_{((0,0),(1,1))}^2
 * theta_4^2 = theta_{((1,0),(0,0))}^2
 * theta_6^2 = theta_{((1,0),(0,1))}^2
 * theta_8^2 = theta_{((0,1),(0,0))}^2
 * theta_9^2 = theta_{((0,1),(1,0))}^2
 * theta_12^2 = theta_{((1,1),(0,0))}^2
 * theta_15^2 = theta_{((1,1),(1,1))}^2
 */
extern void eval_10theta2_naive (mpc_t *th2, mpc_t *tau);

/* Squares of three fundamental theta quotients in tau */
/* this means
 *      theta_1^2/theta_0^2, theta_2^2/theta_0^2, theta_3^2/theta_0^2
 */
extern void eval_3theta2q_naive (mpc_t *b, mpc_t *tau);

/* Same as eval_10theta2_naive, but use Newton lifting. base_prec
 * indicates the precision where lifting starts, and method indicates
 * whether the code should use finite differences (0, faster) or
 * computation of derivatives by Borchardt mean (1, slower).
 */
extern void
eval_10theta2_newton (mpc_t *th2, mpc_t *tau, int base_prec, int method);

/***********************************************************************/
/* Conversion fonctions between theta constants (duplication formulae) */

/* Compute the suqares of the ten even theta constants at tau from the
 * values of the ten fundamental theta constants at tau/2 */
extern void get_10theta2_from_4thetatauhalf (mpc_t *th2, mpc_t *th);

/* This does the same operation, but silently operates on the quadruple
 * (1,th[0],th[1],th[2]) instead. If th[0..2] corresponds to the three
 * fundamental theta quotients at tau/2 (see above), the output is thus
 * the vector:
 * 1/theta_0(tau/2)^2*(theta_j^2(tau))_{j in 0,1,2,3,4,6,8,9,12,15}.
 */
extern void get_10theta2x_from_3thetaqtauhalf(mpc_t th2[10], mpc_t th[3]);


/****************************************************/
/* Conversions related to computation of invariants */

/* Compute h4, h10, h12, h16 from the ten even theta-constants at tau */
/* TODO: Also do h6! It doesn't really make sense to not provide it. If
 * I'm not mistaken, both quadruples (h4, h10, h12, h16) and (h4, h6,
 * h10, h12) generate the ring of holomorphic Siegel modular forms over
 * C. Yet some expressions are easier to write using h6 that h16
 */
extern void h4h10h12h16_from_th2(mpc_t * h, mpc_t * th2);

/* Compute the Igusa-Clebsch invariants (I2,I4,I6,I10) (a.k.a. (A,B,C,D))
 * from (h4, h10, h12, h16).
 */
extern void I2I4I6I10_from_h4h10h12h16(mpc_t * I, mpc_t * h);

/* From a quadruple (I2,I4,I6,I10), we may compute several triples of
 * absolute invariants.
 */
extern void gorenlauter_i1i2i3_from_I2I4I6I10(mpc_t * absolute_invariants, mpc_t * I);
extern void kohel_j1j2j3_from_I2I4I6I10(mpc_t * absolute_invariants, mpc_t * I);
extern void streng_i1i2i3_from_I2I4I6I10(mpc_t * absolute_invariants, mpc_t * I);

#ifdef __cplusplus
}
#endif

#endif	/* CMH_H_ */
