/* quadratic.c -- functions for algebraic numbers in quadratic number fields
  
   Copyright (C) 2010, 2011, 2012, 2018, 2019 INRIA

   This file is part of CMH.

   CMH is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   CMH is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE. See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License
   along with this program. If not, see http://www.gnu.org/licenses/ .
*/

#include <stdlib.h>
/* This is a bit brittle: both pari.h and macros.h define a macro cmul.
   So include our header file later. */
#include <pari/pari.h>
#include "parsepari.h"
#include "macros.h"
#include "quadratic_field.h"
#include "lll.h"

void quadratic_number_init (quadratic_number_ptr q)
{
   mpz_init (q->a);
   mpz_init (q->b);
   mpz_init (q->c);
}

void quadratic_number_clear (quadratic_number_ptr q)
{
   mpz_clear (q->a);
   mpz_clear (q->b);
   mpz_clear (q->c);
}

static void quadratic_number_normalize (quadratic_number_ptr q)
{
    mpz_t g;

    mpz_init (g);
    mpz_gcd (g, q->a, q->c);
    mpz_gcd (g, q->b, g);
    if (mpz_cmp_ui (g, 1) > 0) {
       mpz_divexact (q->a, q->a, g);
       mpz_divexact (q->b, q->b, g);
       mpz_divexact (q->c, q->c, g);
    }
    mpz_clear(g);

    if (mpz_cmp_ui (q->c, 0) < 0) {
       mpz_neg (q->c, q->c);
       mpz_neg (q->a, q->a);
       mpz_neg (q->b, q->b);
    }
}


static void cmh_halfgcd (mpz_t* u, mpz_t *w, mpz_srcptr a, mpz_srcptr b)
   /* Given two integers a and b, the function computes a 2x2-matrix U
      and a 2-vector w such that U*(a, b)^T=w^T represents the half-gcd of
      a and b, that is, two successive remainders "in the middle" of the
      remainder sequence for computing the gcd of a and b. It returns w
      and the second column u of U, which in our special case is the only
      needed part. */
{

/* We need pari-2.12.1 or later for the ghalfgcd function to be present. */
#if PARI_VERSION_CODE >= 134145
extern GEN halfgcd(GEN x, GEN y);
   GEN ap, bp, tmp, up, wp;
   int i;

   pari_sp av;
   pari_init (500000, 0);
   paristack_setsize (500000, 500000000);
   av = avma;

   ap = mpz_get_GEN (a);
   bp = mpz_get_GEN (b);
   tmp = ghalfgcd (ap, bp);
   up = gel (gel (tmp, 1), 2);
   wp = gel (tmp, 2);
   for (i = 0; i < 2; i++) {
      mpz_set_GEN (u [i], gel (up, i+1));
      mpz_set_GEN (w [i], gel (wp, i+1));
   }

   avma = av;
   pari_close ();
#else
   mpz_set_ui (u [0], 0);
   mpz_set_ui (u [1], 1);
   mpz_set (w [0], a);
   mpz_set (w [1], b);
#endif

}


int quadratic_number_recognize (quadratic_number_ptr q, mpfr_srcptr x,
   mpz_srcptr disc)
   /* The function takes a positive quadratic discriminant disc and a floating
     point approximation x to a quadratic number q = (a + b sqrt (disc)) / c;
     it tries to return a candidate for q. The return value reflects the
     success of the operation. */
{
   mpfr_prec_t prec;
   mpfr_t s; /* sqrt(disc) */
   mpfr_t tmp;
   int i, j, res;
   mpfr_exp_t X, Xp, Xm, E;
   mpz_t **M;
   mpz_t sE, mxE;
   mpz_t *v, *u, *w;

   prec = mpfr_get_prec (x);
   X = mpfr_get_exp (x);
   Xp = CMH_MAX (X, 0);
   Xm = CMH_MAX (-X, 0);
   E = prec - X;

   if (E < 0)
      /* prec is too small to recognise a number of exponent X */
      return 0;
   else if (mpfr_get_exp (x) < - 0.9*prec) {
      /* This is a safeguard for recognising 0 instead of
         approximating garbage. */
      mpz_set_si (q->a, 0);
      mpz_set_si (q->b, 0);
      mpz_set_si (q->c, 1);
      return 1;
   }

   finit (tmp, prec);
   finit (s, prec);
   fset_z (s, disc);
   fsqrt (s, s);

   /* The integers a, b and c are found as small integral linear coefficients
      in the relation a*1 + b*s - c*x = 0. We essentially use the same
      algorithm as lindep in PARI, which calls LLL on a matrix obtained by
      joining to the vector [1, s, x] the 3x3 unit matrix. We refine this
      approach by scaling to take the relative magnitude of the base numbers
      into account.

      First we scale the base numbers by 2^E, which turns the floating point
      number x into an integer with exactly prec digits.
      We expect s to have the same magnitude as 1, and a, b and c*x
      to be of roughly the same size. The exponent of x is written as
      X = Xp - Xm, with one of Xp or Xm equal to 0. So
      a*2^Xm, b*2^Xm and c*2^Xp are of about the same size.
      This motivates to scale the unit matrix to arrive at the following
      matrix passed to LLL:

      M = [ 2^Xm,    0,     0,    2^E;
               0, 2^Xm,     0,  s*2^E;
               0,    0,  2^Xp, -x*2^E ].

      To accelerate the algorithm, we drop the first column, which then
      requires a little more work to derive a from the short vector
      (this appears to be more efficient than dropping the third column,
      which was also tested).

      For further acceleration, we may compute a halfgcd between two entries
      in the last column and apply the transformation matrix from the left;
      this results in smaller entries in quasi-linear time. Experiments have
      shown that the best choice is to work with the first and the second
      row: Since in our application with large numbers x we generally have
      Xm=0, we precondition the matrix
         0  0    2^E
         1  0  s*2^E
      and keep the zero column; the total size of the matrix entries will
      thus be roughly preserved. */

   M = (mpz_t **) malloc (3 * sizeof (mpz_t *));
   v = (mpz_t *) malloc (3 * sizeof (mpz_t));
   for (i = 0; i < 3; i++) {
      M [i] = (mpz_t *) malloc (3 * sizeof (mpz_t));
      for (j = 0; j < 3; j++)
         mpz_init_set_ui (M [i][j], 0);
      mpz_init (v [i]);
   }
   mpz_init (sE);
   mpz_init (mxE);
   /* Last column. */
   mpz_set_ui (M [0][2], 1);
   mpz_mul_2exp (M [0][2], M [0][2], E);
   fmul_2ui (tmp, s, E);
   fget_z (sE, tmp);
   mpz_set (M [1][2], sE);
   fmul_2ui (tmp, x, E);
   fneg (tmp, tmp);
   fget_z (mxE, tmp);
   mpz_set (M [2][2], mxE);
   /* Scaled unit matrix without first column. */
   mpz_set_ui (M [1][0], 1);
   mpz_mul_2exp (M [1][0], M [1][0], Xm);
   mpz_set_ui (M [2][1], 1);
   mpz_mul_2exp (M [2][1], M [2][1], Xp);

   /* Pre-condition M using the half-gcd. */
   u = (mpz_t *) malloc (2 * sizeof (mpz_t));
   w = (mpz_t *) malloc (2 * sizeof (mpz_t));
   for (i = 0; i < 2; i++) {
      mpz_init (u [i]);
      mpz_init (w [i]);
   }
   cmh_halfgcd (u, w, M [0][2], M [1][2]);
   mpz_set (M [0][0], u [0]);
   mpz_set (M [1][0], u [1]);
   mpz_set (M [0][2], w [0]);
   mpz_set (M [1][2], w [1]);
   for (int i = 0; i < 2; i++) {
      mpz_clear (u [i]);
      mpz_clear (w [i]);
   }
   free (u);
   free (w);

   cmh_lll (v, M, 3, 3);

   res = mpz_cmp_ui (v [1], 0);
   if (res) {
      mpz_tdiv_q_2exp (q->b, v [0], Xm);
      mpz_tdiv_q_2exp (q->c, v [1], Xp);

      /* Reuse v [0] and v [1] as temporary variables. */
      mpz_mul (v [0], q->b, sE);
      mpz_mul (v [1], q->c, mxE);
      mpz_add (v [0], v [0], v [1]);
      mpz_sub (v [1], v [2], v [0]);
      mpz_tdiv_q_2exp (q->a, v [1], E);

      quadratic_number_normalize (q);
   }

   for (int i = 0; i < 3; i++) {
      for (int j = 0; j < 3; j++)
         mpz_clear (M [i][j]);
      free (M [i]);
      mpz_clear (v [i]);
   }
   free (M);
   free (v);
   mpz_clear (sE);
   mpz_clear (mxE);
   mpfr_clear (tmp);
   mpfr_clear (s);

   return res;
}

