#ifndef FACTOR_H_
#define FACTOR_H_

/* factor.h -- header for factor.c
 *
 * Copyright (C) 2012, 2013 INRIA
 *
 * This file is part of CMH.
 *
 * CMH is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * CMH is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/ .
 */

#include <gmp.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef unsigned long int (*factor_matrix)[2];

void factor_matrix_free (factor_matrix f);

factor_matrix factor_trialdiv (mpz_srcptr n, unsigned long int bound);
void factor_matrix_asprintf (char ** pres, factor_matrix f);
int factor_matrix_fprintf (FILE *file, factor_matrix f);
factor_matrix factor_matrix_dup(factor_matrix f);
void factor_matrix_fold(mpz_ptr z, factor_matrix f);

void mpz_remove_squares(mpz_ptr);

#ifdef __cplusplus
}
#endif

#endif  /* FACTOR_H_ */
