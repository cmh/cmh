/* borchardt.c -- computation of Borchardt means
 *
 * Copyright (C) 2006, 2010, 2011, 2012, 2013, 2022 INRIA
 *
 * This file is part of CMH.
 *
 * CMH is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * CMH is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/ .
 */

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

#include <gmp.h>
#include <mpfr.h>
#include <mpc.h>

#include "params.h"
#include "macros.h"
#include "borchardt.h"
#include "misc.h"



#if 0
/*
 *  The following function does the same, but uses an "accumulator" and only 3
 *  square roots per iteration (moreover, the square roots are extracted from
 *  complex numbers close to 1, and that could speed things up a bit if the
 *  Taylor series for the square root is used at some point...)
 */

/*nb*/
static void
BorchardtMeanOneStep_fast (mpc_t *a)
{
    int i, prec;
    mpc_t ra[3];
    mpc_t aux1;

    prec = cprec(a[0]);
    for (i=0; i<3; i++)
    {
	cinit(ra[i], prec);
	csqrt(ra[i], a[i+1]);
    }
    cinit(aux1, prec);
    cadd_ui(aux1, a[1], 1);
    cadd(aux1, aux1, a[2]);
    cadd(aux1, aux1, a[3]);
    cdiv_2ui(aux1, aux1, 2);
    cmul(a[0], a[0], aux1);
    cinv(aux1, aux1);
    cmul(a[1], ra[1], ra[2]);
    cadd(a[1], a[1], ra[0]);
    cdiv_2ui(a[1], a[1], 1);
    cmul(a[1], a[1], aux1);
    cmul(a[2], ra[0], ra[2]);
    cadd(a[2], a[2], ra[1]);
    cdiv_2ui(a[2], a[2], 1);
    cmul(a[2], a[2], aux1);
    cmul(a[3], ra[0], ra[1]);
    cadd(a[3], a[3], ra[2]);
    cdiv_2ui(a[3], a[3], 1);
    cmul(a[3], a[3], aux1);

    cclear(aux1);
    for (i=0; i<3; i++)
    {
	cclear(ra[i]);
    }

    return;
}
#endif


/*
 * This is similar to BorchardtMeanOneStepDiff. It computes the ``good''
 * Borchardt iterate of a[0..3] (whose partial derivatives wrt chosen
 * variables are in da[0..3][0..2]).
 *
 * The choice of roots which is made guarantees
 * that (ra[0]:ra[1]:ra[2]:ra[3]) is projectively close to
 * (low[0]:low[1]:low[2]:low[3]) (see src/misc.h)
 *
 * if (low==NULL), then all roots are taken with positive real part.
 *
 */
static void
BorchardtMeanOneStepDiffWithSigns (mpc_t a[4], mpc_t da[4][3], int th_sign[4][2])
{
    int i, prec;
    mpc_t ra[4], ira[4], db[4][3], aux;

    // a0 a1 a2 a3 is some borchard iterate 4-uple, or with notation more
    // consistent with other places, some (a_n,b_n,c_n,d_n). We
    // compute (a_{n+1},b_{n+1},c_{n+1},d_{n+1}). We also obtain as input
    // the derivatives of a0 a1 a2 a3 w.r.t three variables x y z,
    // subject to:
    /*
     * da[i][j] = da[i]/dxj
     */
    // see page 215 for the formulae which gives the derivatives of the
    // next iteration w.r.t this choice.
    //
    // a[] and da[] are modified in place

    prec = cprec(a[0]);
    cinit(aux, prec);
    for (i=0; i<4; i++)
    {
	cinit(ra[i], prec);
	csqrt(ra[i], a[i]);     // pick positive square roots of a0..a3
	if (da) cinit(ira[i], prec);
	if (da) cinv(ira[i], ra[i]);    // and their inverses
    }
    if (th_sign) {
        int negate = find_projectively_close_quadruple(ra, th_sign);
        for (i=0; i<4; i++) {
            if (negate & (1<<i)) {
                cneg(ra[i], ra[i]);
                if (da) cneg(ira[i], ira[i]);
            }
        }

    }
    if (da) {
        for (int i=0; i<4; i++)
            for(int j = 0 ; j < 3 ; j++)
                cinit(db[i][j], prec);
    }

    /* Seq. 1 */
    cadd(a[0], a[0], a[1]);
    cadd(a[0], a[0], a[2]);
    cadd(a[0], a[0], a[3]);
    cdiv_2ui(a[0], a[0], 2);   // a0 <- (a0+a1+a2+a3)/4

    if (da) for (i=0; i<3; i++)
    {
	cadd(db[0][i], da[0][i], da[1][i]);
	cadd(db[0][i], db[0][i], da[2][i]);
	cadd(db[0][i], db[0][i], da[3][i]);
	cdiv_2ui(db[0][i], db[0][i], 2);    // db[0]0/dxi=1/4 * sum_j da_j/dxi
    }

    /* Seq. 2 */
    cmul(a[1], ra[0], ra[1]);
    cmul(aux, ra[2], ra[3]);
    cadd(a[1], a[1], aux);
    cdiv_2ui(a[1], a[1], 1); // a1 <- (\sqrt{a0}\sqrt{a1}+\sqrt{a2}\sqrt{a3})/2
    if (da) for (i=0; i<3; i++)
    {
	cmul(db[1][i], da[0][i], ra[1]);
	cmul(db[1][i], db[1][i], ira[0]);
	cmul(aux, da[1][i], ra[0]);
	cmul(aux, aux, ira[1]);
	cadd(db[1][i], db[1][i], aux);
	cmul(aux, da[2][i], ra[3]);
	cmul(aux, aux, ira[2]);
	cadd(db[1][i], db[1][i], aux);
	cmul(aux, da[3][i], ra[2]);
	cmul(aux, aux, ira[3]);
	cadd(db[1][i], db[1][i], aux);
	cdiv_2ui(db[1][i], db[1][i], 2);
    }

    /* Seq. 3 */
    cmul(a[2], ra[0], ra[2]);
    cmul(aux, ra[1], ra[3]);
    cadd(a[2], a[2], aux);
    cdiv_2ui(a[2], a[2], 1);
    if (da) for (i=0; i<3; i++)
    {
	cmul(db[2][i], da[0][i], ra[2]);
	cmul(db[2][i], db[2][i], ira[0]);
	cmul(aux, da[2][i], ra[0]);
	cmul(aux, aux, ira[2]);
	cadd(db[2][i], db[2][i], aux);
	cmul(aux, da[1][i], ra[3]);
	cmul(aux, aux, ira[1]);
	cadd(db[2][i], db[2][i], aux);
	cmul(aux, da[3][i], ra[1]);
	cmul(aux, aux, ira[3]);
	cadd(db[2][i], db[2][i], aux);
	cdiv_2ui(db[2][i], db[2][i], 2);
    }

    /* Seq. 4 */
    cmul(a[3], ra[0], ra[3]);
    cmul(aux, ra[2], ra[1]);
    cadd(a[3], a[3], aux);
    cdiv_2ui(a[3], a[3], 1);
    if (da) for (i=0; i<3; i++)
    {
	cmul(db[3][i], da[0][i], ra[3]);
	cmul(db[3][i], db[3][i], ira[0]);
	cmul(aux, da[3][i], ra[0]);
	cmul(aux, aux, ira[3]);
	cadd(db[3][i], db[3][i], aux);
	cmul(aux, da[2][i], ra[1]);
	cmul(aux, aux, ira[2]);
	cadd(db[3][i], db[3][i], aux);
	cmul(aux, da[1][i], ra[2]);
	cmul(aux, aux, ira[1]);
	cadd(db[3][i], db[3][i], aux);
	cdiv_2ui(db[3][i], db[3][i], 2);
    }

    if (da) {
        for (int i=0; i<4; i++) {
            for(int j = 0 ; j < 3 ; j++) {
                cset(da[i][j], db[i][j]);
                cclear(db[i][j]);
            }
        }
    }
    for(int i=0; i<4; i++)
    {
	cclear(ra[i]);
	if (da) cclear(ira[i]);
    }
    cclear(aux);

    return;
}

static void
BorchardtMeanOneStepDiff (mpc_t a[4], mpc_t da[4][3])
{
    BorchardtMeanOneStepDiffWithSigns(a, da, NULL);
}


/* Computes 1/B2(a0,a1,a2), with da[3i+j] = da[i]/dxj */
/* th_sign give the signs for the first choice of roots. If NULL, this
 * means ``by default''.
 */
/*nf*/
int
InverseBorchardtMeanDiffWithSigns (mpc_t r, mpc_t dr[3], mpc_t a[3], mpc_t da[3][3], int th_sign[4][2])
{
   mpc_t b[4], db[4][3];
   int prec, prec1, i;

   assert ( (mpc_cmp_si (a [0], 0) == 0)
           + (mpc_cmp_si (a [1], 0) == 0)
           + (mpc_cmp_si (a [2], 0) == 0)
           <= 1);
      /* actually, not even one of the theta functions should be zero */

   prec1 = cprec(r);
   prec = prec1+SEC_MARGIN;
   for (i=0; i<4; i++)
      cinit(b[i], prec);
   if (da) {
       for(int i = 0 ; i < 4 ; i++) {
           for(int j = 0 ; j < 3 ; j++) {
               cinit(db[i][j], prec);
           }
       }
   }
   for (i=0; i<3; i++) {
      cset(b[i+1], a[i]);
      if (da) czero(db[0][i]);
   }
   cone(b[0]); // b0,b1,b2,b3 == 1,a0,a1,a2
   if (da) {
       for(int i = 1 ; i < 4 ; i++) {
           for(int j = 0 ; j < 3 ; j++) {
               cset(db[i][j], da[i-1][j]);
           }
       }
   }
   // derivatives of b0,b1,b2,b3 (as initial parameters) are the same as
   // those of a0,a1,a2, with three zeroes padded in front.

   if (th_sign)
      BorchardtMeanOneStepDiffWithSigns(b, da?db:NULL, th_sign);

   while ( (creldist(b[0], b[1]) < prec1+1) ||
         (creldist(b[0], b[2]) < prec1+1) ||
         (creldist(b[0], b[3]) < prec1+1) )
      BorchardtMeanOneStepDiff(b, da?db:NULL);

   // Now compute the *inverse* of the limit, together with the partial
   // derivatives.
   // r=1/b0
   cinv(r, b[0]);
   if (da) {
       // dr/dx = -1/b0^2*db0/dx
       csqr(b[0], r);
       for (i=0; i<3; i++) {
           cmul(dr[i], db[0][i], b[0]);
           cneg(dr[i], dr[i]);
       }
   }
   for (i=0; i<4; i++)
      cclear(b[i]);
   if (da) {
       for(int i = 0 ; i < 4 ; i++) {
           for(int j = 0 ; j < 3 ; j++) {
               cclear(db[i][j]);
           }
       }
   }


   return 1;
}

int
InverseBorchardtMeanDiff (mpc_t r, mpc_t dr[3], mpc_t a[3], mpc_t da[3][3])
{
    return InverseBorchardtMeanDiffWithSigns(r, dr, a, da, NULL);
}

int
InverseBorchardtMeanWithSigns (mpc_t r, mpc_t a[3], int th_sign[4][2])
{
    return InverseBorchardtMeanDiffWithSigns(r, NULL, a, NULL, th_sign);
}

int
InverseBorchardtMean(mpc_t r, mpc_t a[3])
{
    return InverseBorchardtMeanDiffWithSigns(r, NULL, a, NULL, NULL);
}

/* Takes a0...a3 and their derivatives w.r.t three variables. Return
 * their borchardt mean, and its derivatives wrt the same three variables
 */
void
BorchardtMean4Diff(mpc_t r, mpc_t dr[3], mpc_t a[4], mpc_t da[4][3])
{
   mpc_t b[4], db[4][3], aux1, aux2;

   assert ( (mpc_cmp_si (a [0], 0) == 0)
           + (mpc_cmp_si (a [1], 0) == 0)
           + (mpc_cmp_si (a [2], 0) == 0)
           + (mpc_cmp_si (a [3], 0) == 0)
           <= 1);
      /* actually, not even one of the theta functions should be zero */

   int prec1 = cprec(r);
   int prec = prec1+SEC_MARGIN;

   /* We need temporary variables which are modified throughout the
    * iterations, and whose first value is (1,a1/a0,a2/a0,a3/a0).
    */
   for (int i=0; i<4; i++)
      cinit(b[i], prec);
   cinv(b[0], a[0]);
   for(int i = 1 ; i < 4 ; i++) {
       cmul(b[i], b[0], a[i]);
   }
   /* setting b[0] to one is postponed, we still need 1/a0 for computing
    * the derivatives */

   if (da) {
       cinit(aux1, prec);
       cinit(aux2, prec);
       for (int i=0; i<4; i++)
           for(int j = 0 ; j < 3 ; j++)
               cinit(db[i][j], prec);
       /* clearly the derivative of b0=1 is 0. */
       for(int j = 0 ; j < 3 ; j++)
           czero(db[0][j]);
       /* The derivative of bi=ai/a0 is dai/a0 - ai*da0/a0^2 */
       /* Note that at this point b[0] still contains 1/a0 */
       for(int i = 1 ; i < 4 ; i++) {
           cmul(aux1, b[i], b[0]);      /* ai/a0^2 */
           for(int j = 0 ; j < 3 ; j++) {
               cmul(db[i][j], da[i][j], b[0]);  /* dai/a0 */
               cmul(aux2, aux1, da[0][j]);      /* ai*da0/a0^2 */
               csub(db[i][j], db[i][j], aux2);
           }
       }
   }
   cone(b[0]);

   while ( (creldist(b[0], b[1]) < prec1+1) ||
         (creldist(b[0], b[2]) < prec1+1) ||
         (creldist(b[0], b[3]) < prec1+1) )
      BorchardtMeanOneStepDiff(b, da?db:NULL);

   // Now compute the *inverse* of the limit, together with the partial
   // derivatives.
   // r=1/(a0b0)
   
   // first compute a0b0
   cmul(r, a[0], b[0]);
   /* Put in db[0] the derivatives of a0b0 */
   if (da) {
       for(int j = 0 ; j < 3 ; j++) {
           cmul(aux2, da[0][j], b[0]);
           cmul(dr[j], db[0][j], a[0]);
           cadd(dr[j], dr[j], aux2);
       }
   }

   for (int i=0; i<4; i++)
      cclear(b[i]);
   if (da) {
       for (int i=0; i<4; i++)
           for(int j = 0 ; j < 3 ; j++)
           cclear(db[i][j]);
       cclear(aux1);
       cclear(aux2);
   }
}

/* Takes a0...a3 and their derivatives w.r.t three variables. Return the
 * inverse of their borchardt mean, and its derivatives wrt the same
 * three variables
 */
int
InverseBorchardtMean4Diff(mpc_t r, mpc_t dr[3], mpc_t a[4], mpc_t da[4][3])
{
   BorchardtMean4Diff(r, dr, a, da);

   cinv(r, r); // r = 1/(a0b0) ; a0b0 is in b[0], its diffs in db[0]

   if (dr) {
       int prec1 = cprec(r);
       int prec = prec1+SEC_MARGIN;
       mpc_t aux;
       cinit(aux, prec);
       csqr(aux, r);

       // d(1/r)/dx = -1/r^2*dr/dx
       for(int j = 0 ; j < 3 ; j++) {
           cmul(dr[j], dr[j], aux);
           cneg(dr[j], dr[j]);
       }
       cclear(aux);
   }
   return 1;
}

