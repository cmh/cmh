#ifndef BORCHARDT_MEAN_H_
#define BORCHARDT_MEAN_H_

/* borchardt.h -- headers for borchardt.c
 *
 * Copyright (C) 2006, 2010, 2011, 2012, 2013 INRIA
 *
 * This file is part of CMH.
 *
 * CMH is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * CMH is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/ .
 */

/* Inherit publicly exported prototypes (BorchardtMean3 and
 * BorchardtMean4) */
#include "cmh.h"

#ifdef __cplusplus
extern "C" {
#endif

/* TODO: should we have interfaces both for inverse and non-inverse ? */
/* TODO: add const-ness info (if not in the prototype, at least here as a
 * comment) */

/* The additional arguments th_sign here give the choice of roots for the
 * first iteration (may be NULL, which indicates the default choice of
 * taking the positive real part).
 */
int InverseBorchardtMean (mpc_t r, mpc_t a[3]);
int InverseBorchardtMeanWithSigns (mpc_t r, mpc_t a[3], int th_sign[4][2]);
int InverseBorchardtMeanDiff (mpc_t r, mpc_t dr[3], mpc_t a[3], mpc_t da[3][3]);
int InverseBorchardtMeanDiffWithSigns (mpc_t r, mpc_t dr[3], mpc_t a[3], mpc_t da[3][3], int th_sign[4][2]);
int InverseBorchardtMean4Diff(mpc_t r, mpc_t dr[3], mpc_t a[4], mpc_t da[4][3]);
void BorchardtMean4Diff(mpc_t r, mpc_t dr[3], mpc_t a[4], mpc_t da[4][3]);

#ifdef __cplusplus
}
#endif


#endif
