#ifndef MPFRX_MPI_H
#define MPFRX_MPI_H

/* mpfrx-mpi.h -- headers for mpfrx-mpi.h
 *
 * Copyright (C) 2012, 2013 INRIA
 *
 * This file is part of CMH.
 *
 * CMH is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * CMH is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/ .
 */

#include <mpfrcx.h>
#include "assert.h"
#include "cm2.h"
   /* for the definition of struct cm_data */
#include "io.h"
   /* for checkpointing of the product trees */

#define MPI_MPFRX_READY          201
#define MPI_MPFRX_JOB_MPFRX_MUL  202
#define MPI_MPFRX_JOB_MPFRX_KARA 203
#define MPI_MPFRX_JOB_MPFRX_TC   204
#define MPI_MPFRX_RESULT         205
#define MPI_MPFRX_DATA           206
#define MPI_MPFRX_WAIT           207
#define MPI_MPFRX_FINISH         208

typedef struct {
   int no;
   int levels;
   int **node;
   int *width;
}
__tree_struct;

typedef __tree_struct tree_t [1];
typedef __tree_struct *tree_ptr;

#if defined (__cplusplus)
extern "C" {
#endif

extern void mpfrx_tree_print (mpfrx_tree_ptr t);

extern void mpi_mpfrx_server_init ();
extern void mpi_mpfrx_server_finalise ();
extern void mpi_mpfrx_client_init ();
extern void mpi_mpfrx_client ();

extern void mpi_mpfrx_server_product_and_hecke (mpfrx_t *rop, mpfrx_t **vals,
   int no_pols, int no_factors, int level,
   const struct cm_data * K, const int orbit, const int iter);

#if defined (__cplusplus)
}
#endif

#endif
