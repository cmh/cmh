/* lprintf.c -- printing
 *
 * Copyright (C) 2010, 2011, 2012 INRIA
 *
 * This file is part of CMH.
 *
 * CMH is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * CMH is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/ .
 */

#include <stdarg.h>
#include <stdio.h>
#include <gmp.h>
#include <mpfr.h>
#include <ctype.h>
#include <string.h>
#include <sys/time.h>
#include "lprintf.h"
#include "cputime.h"
#include "process_status.h"

extern int loglevel;

double start_time = -1;

void lprintf_init_timer()
{
    start_time = hires_wct();
}

#ifdef __linux
static const char *size_disp(size_t s, char buf[16])
{
    char *prefixes = "bkMGT";
    double ds = s;
    const char *px = prefixes;
    for (; px[1] && ds > 500.0;) {
        ds /= 1024.0;
        px++;
    }
    snprintf(buf, 10, "%.1f%c", ds, *px);
    return buf;
}
#endif


int lvprintf(const char * fmt0, va_list ap)
{
    int level = 2;
    int res;
    const char * fmt = fmt0;
    if (*fmt && fmt[0] == '<' && isdigit(fmt[1]) && fmt[2] == '>') {
        level = fmt[1]-'0';
        fmt += 3;
    }
    if (level > loglevel)
        return 0;
    if (strncmp(fmt, LOG_NOTIMESTAMP, strlen(LOG_NOTIMESTAMP)) == 0) {
        fmt += strlen(LOG_NOTIMESTAMP);
    } else if (start_time > 0) {
         double t = hires_wct();
#ifdef __linux
         static unsigned long peak_vsize = 0;
         char buf1[16];
         char buf2[16];
         struct process_status pr[1];
         int rc = get_process_status(pr, 0);
         if (rc < 0) {
               fprintf(stderr, "Could not get process status\n");
         } else {
            size_disp(pr->vsize, buf1);
            if (pr->vsize > peak_vsize) peak_vsize = pr->vsize;
            size_disp(peak_vsize, buf2);
            fprintf(stdout, "[%.3f %s %s] ", t - start_time, buf1, buf2);
         }
#else
            fprintf(stdout, "[%.3f] ", t - start_time);
#endif
    }
    if (strstr(fmt, "%P") || strstr(fmt, "%R")) {
        res = mpfr_vfprintf(stdout, fmt, ap);
    } else if (strstr(fmt, "%Z") || strstr(fmt, "%F") || strstr(fmt, "%Q") || strstr(fmt, "%M") || strstr(fmt, "%N")) {
    va_end(ap);
        res = gmp_vfprintf(stdout, fmt, ap);
    } else {
        res = vfprintf(stdout, fmt, ap);
    }
    return res;
}

int lprintf(const char * fmt0, ...)
{
    va_list ap;
    va_start(ap, fmt0);
    int rc = lvprintf(fmt0, ap);
    va_end(ap);
    return rc;
}

