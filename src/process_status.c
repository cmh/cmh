/* process_status.c -- get max time and memory
 *
 * Copyright (C) 2012 INRIA
 *
 * This file is part of CMH.
 *
 * CMH is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * CMH is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/ .
 */

#define _BSD_SOURCE
#include <stdio.h>
#include <ctype.h>
#include "process_status.h"

int get_process_status(struct process_status * pr, int pid)
{
    char fname[20] = "/proc/self/stat";
    if (pid)
        snprintf(fname, sizeof(fname), "/proc/%d/stat", pid);

    FILE * f = fopen(fname, "r");
    if (f == NULL) return -1;

#define GET1(fmt__, name__) \
    rc = fscanf(f, fmt__, &pr->name__); error |= rc != 1;

    int rc;
    int error = 0;

    GET1("%d", pid);
    /* Boring. Get the process name */
    char c='\0';
    for( ; !feof(f) && isspace(c=fgetc(f)) ; );
    error |= c != '(';
    for(int i = 0 ; i < sizeof(pr->name)-1 ; i++) {
        c = fgetc(f);
        if (c == ')') break;
        pr->name[i]=c;
        pr->name[i+1]='\0';
    }
    error |= !isspace(c=fgetc(f));

    GET1("%c", state);
    GET1("%d", ppid);
    GET1("%d", pgrp);
    GET1("%d", session);
    GET1("%d", tty_nr);
    GET1("%d", tpgid);
    GET1("%lu", flags);
    GET1("%lu", minflt);
    GET1("%lu", cminflt);
    GET1("%lu", majflt);
    GET1("%lu", cmajflt);
    GET1("%lu", utime);
    GET1("%lu", stime);
    GET1("%ld", ctime);
    GET1("%ld", cstime);
    GET1("%ld", priority);
    GET1("%ld", nice);
    GET1("%ld", num_threads);
    GET1("%ld", itrealvalue);
    GET1("%lu", starttime);     /* I know it's lonlong in theory */
    GET1("%lu", vsize);
    GET1("%ld", rss);
    GET1("%lu", rsslim);
    GET1("%lu", startcode);
    GET1("%lu", endcode);
    GET1("%lu", startstack);
    GET1("%lu", kstkesp);
    GET1("%lu", kstkeip);
    GET1("%lu", signal);
    GET1("%lu", blocked);
    GET1("%lu", sigignore);
    GET1("%lu", sigcatch);
    GET1("%lu", wchan);
    GET1("%lu", nswap);
    GET1("%lu", cnswap);
    GET1("%d", exit_signal);
    GET1("%d", processor);
    fclose(f);
    return error ? -1 : 0;
}

