#!/usr/bin/env bash

WORKDIR=`pwd`
SCRIPTSDIR=$WORKDIR/../scripts
TMPDIR=`mktemp -d`

function compute_pol ()
{
   cd $TMPDIR
   $SCRIPTSDIR/prepare-input.sh $1 $2
   $WORKDIR/cm2 -f *_$1_$2.in
}

function compute_pol_mpi ()
{
   cd $TMPDIR
   $SCRIPTSDIR/prepare-input.sh $1 $2
   mpirun -n 2 $WORKDIR/cm2-mpi -f *_$1_$2.in
}

compute_pol 27 52
compute_pol_mpi 35 65
