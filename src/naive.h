#ifndef NAIVE_H_
#define NAIVE_H_

/* naive.h -- headers for naive.c
 *
 * Copyright (C) 2006, 2011, 2012, 2013 INRIA
 *
 * This file is part of CMH.
 *
 * CMH is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * CMH is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/ .
 */

static const int even_thetas[10] = { 0, 1, 2, 3, 4, 6, 8, 9, 12, 15 };
static const int even_thetas_ix[16] = { 0, 1, 2, 3, 4, -1, 5, -1, 6, 7, -1, -1, 8, -1, -1, 9 };

/* Inherit publicly exported prototypes:
 *      eval_4theta_naive
 *      eval_3theta2q_naive
 *      eval_10theta2_naive
 *      get_10theta2_from_4thetatauhalf
 *      get_10theta2x_from_3thetaqtauhalf
 */
#include "cmh.h"

/* These do the same as the publicly exported function, but with the
 * derivatives (the back-end is in fact identical, but multiplications
 * are omitted in the without-diff case) */
void get_10theta2_from_4thetatauhalf_diff (mpc_t th2[10], mpc_t dth2[10][3], mpc_t th[4], mpc_t dth[4][3]);

/* These are hacks. We are doing the same thing as above, but
 * special-cased for a quadruple which is (1,th[0],th[1],th[2]).
 * 
 * Derivatives are taken with respect to variables (th[0],th[1],th[2]),
 * which means that on input we certainly expect things like
 * dth[i][j]=(i==j). However the latter point is not taken into account
 * for optimizations (FIXME: fix this, perhaps).
 */
void get_10theta2x_from_3thetaqtauhalf_diff(mpc_t th2[10], mpc_t dth2[10][3], mpc_t th[3], mpc_t dth[3][3]);


/* Some considerations about cost of the six functions in the
 * get_10theta2_from_4thetatauhalf family:
 *
 * non-diff           | diff
 * get_10: 4S + 6M    | 4S + 12M + 6M + 36M = 4S + 54M
 * get_4:  0M         | 0M
 * get_6:  6M         | 6M + 36M = 42M
 *
 * So there's at least a 6x ratio 
 */
#endif
