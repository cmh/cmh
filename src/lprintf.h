#ifndef LPRINTF_H_
#define LPRINTF_H_

/* lprintf.h -- headers for lprintf.c
 *
 * Copyright (C) 2010, 2011, 2012 INRIA
 *
 * This file is part of CMH.
 *
 * CMH is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * CMH is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/ .
 */

#include <stdarg.h>

#define LOG_ALWAYS  "<0>"
#define LOG_ERROR  "<0>"
#define LOG_IMPORTANT  "<1>"
#define LOG_NORMAL  "<2>"
#define LOG_PROGRESS   "<3>"
#define LOG_INFO   "<5>"
#define LOG_DEBUG  "<6>"
#define LOG_DEBUG_MORE  "<7>"
#define LOG_CHATROOM  "<8>"
#define LOG_NOTIMESTAMP "<#>"

#ifdef __cplusplus
extern "C" {
#endif

extern int loglevel;

int lprintf(const char * fmt, ...);
int lvprintf(const char * fmt, va_list ap);
void lprintf_init_timer();

#ifdef __cplusplus
}
#endif

#endif	/* LPRINTF_H_ */
