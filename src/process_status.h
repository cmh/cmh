#ifndef PROCESS_STATUS_H_
#define PROCESS_STATUS_H_

/* process_status.h -- headers for process_status.c
 *
 * Copyright (C) 2012 INRIA
 *
 * This file is part of CMH.
 *
 * CMH is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * CMH is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/ .
 */

#ifdef __cplusplus
extern "C" {
#endif


/* This is directly from man proc (5) */
struct process_status {
    int	pid;
    char	name[256];
    char	state;
    int	ppid;
    int	pgrp;
    int	session;
    int	tty_nr;
    int	tpgid;
    unsigned long	flags;
    unsigned long	minflt;
    unsigned long	cminflt;
    unsigned long	majflt;
    unsigned long	cmajflt;
    unsigned long	utime;
    unsigned long	stime;
    long	ctime;
    long	cstime;
    long	priority;
    long	nice;
    long	num_threads;
    long	itrealvalue;
    unsigned long	starttime;
    unsigned long	vsize;
    long	rss;
    unsigned long	rsslim;
    unsigned long	startcode;
    unsigned long	endcode;
    unsigned long	startstack;
    unsigned long	kstkesp;
    unsigned long	kstkeip;
    unsigned long	signal;
    unsigned long	blocked;
    unsigned long	sigignore;
    unsigned long	sigcatch;
    unsigned long	wchan;
    unsigned long	nswap;
    unsigned long	cnswap;
    int	exit_signal;
    int	processor;
    struct {
        unsigned long	size;
        unsigned long	resident;
        unsigned long	share;
        unsigned long	trs;
        unsigned long	drs;
        unsigned long	lrs;
        unsigned long	dt;
    } statm;
};

extern int get_process_status(struct process_status * pr, int pid);

#ifdef __cplusplus
}
#endif

#endif	/* PROCESS_STATUS_H_ */
