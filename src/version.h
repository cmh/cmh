#ifndef VERSION_H_
#define VERSION_H_

#ifdef __cplusplus
extern "C" {
#endif

extern const char * version_string;

#ifdef __cplusplus
}
#endif

#endif	/* VERSION_H_ */
