#ifndef JINVARIANTS_H_
#define JINVARIANTS_H_

/* jinvariants.h -- headers for jinvariants.c
 *
 * Copyright (C) 2006, 2011, 2012 INRIA
 *
 * This file is part of CMH.
 *
 * CMH is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * CMH is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/ .
 */

#ifdef __cplusplus
extern "C" {
#endif

/* Inherit publicly exported prototypes:
 *      h4h10h12h16_from_th2
 *      I2I4I6I10_from_h4h10h12h16
 *      gorenlauter_i1i2i3_from_I2I4I6I10
 *      streng_i1i2i3_from_I2I4I6I10
 *      kohel_j1j2j3_from_I2I4I6I10
 */
#include "cmh.h"

/* This one is only for us */
void invariants_from_th2(mpc_t * jinv, mpc_t * th2, int invariant_set);

#ifdef __cplusplus
}
#endif

#endif
