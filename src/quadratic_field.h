#ifndef QUADRATIC_FIELD_H_
#define QUADRATIC_FIELD_H_

/* quadratic.h -- headers for quadratic.c
  
   Copyright (C) 2010, 2011, 2012 INRIA

   This file is part of CMH.
  
   CMH is free software; you can redistribute it and/or modify it under
   the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   CMH is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
   FOR A PARTICULAR PURPOSE. See the GNU General Public License for
   more details.

   You should have received a copy of the GNU General Public License
   along with this program. If not, see http://www.gnu.org/licenses/ .
*/

#include <gmp.h>
#include <mpfr.h>

#ifdef __cplusplus
extern "C" {
#endif

struct quadratic_number_s {
    mpz_t a;
    mpz_t b;
    mpz_t c;
};
typedef struct quadratic_number_s quadratic_number [1];
typedef struct quadratic_number_s *quadratic_number_ptr;
typedef const struct quadratic_number_s *quadratic_number_srcptr;

void quadratic_number_init (quadratic_number_ptr);
void quadratic_number_clear (quadratic_number_ptr);

int quadratic_number_recognize (quadratic_number_ptr, mpfr_srcptr, mpz_srcptr);

#ifdef __cplusplus
}
#endif

#endif /* QUADRATIC_FIELD_H_ */

