#ifndef IO_H_
#define IO_H_

/* io.h -- headers for io.c
 *
 * Copyright (C) 2012, 2013 INRIA
 *
 * This file is part of CMH.
 *
 * CMH is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * CMH is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/ .
 */

#include <stdio.h>
#include <mpc.h>

#define OUTPUT_FORMAT_CODE 42006

#define CM2_IO_PERIOD      'P'
#define CM2_IO_NEWTON      'N'
#define CM2_IO_TREE        'T'
#define CM2_IO_RECONSTRUCT 'R'
#define CM2_IO_COEFF       'K'
#define CM2_IO_FAILED      'F'
#define CM2_IO_POLYNOMIAL  'H'

extern int io_allow_load;
extern int io_allow_save;

#ifdef __cplusplus
extern "C" {
#endif

const char *get_polyname (int imum);

int make_checkpoint_directory (const struct cm_data * K);

int get_iteration (const struct cm_data * K, const int orbit);
int get_level_producttrees (const struct cm_data * K, const int orbit,
   const int iter);

int save_period (const struct cm_data * K, const int orbit, const int no,
   const int iter, const struct cm_period_data * p);
int load_period (struct cm_period_data *p, const struct cm_data * K,
   const int orbit, const int no, const int iter);
int save_orbit (const struct cm_data * K, const int orbit, const int iter,
   const struct cm_orbit_data * O);
int load_orbit (struct cm_orbit_data *O, const struct cm_data * K,
   const int orbit, const int iter);
int save_producttrees (const struct cm_data * K, const int orbit,
   const int iter, const int level, const int no_pols, const int no_factors,
   mpfrx_t *vals);
int load_producttrees (int *no_pols, int *no_factors, mpfrx_t ***vals,
   const struct cm_data * K, const int orbit,
   const int iter, const int level);
int save_polynomial_real (const struct cm_data * K, const int orbit,
   const int inum, const int iter, mpfrx_srcptr H);
int load_polynomial_real (mpfrx_ptr H, const struct cm_data * K, const int orbit,
   const int inum, const int iter);
int save_quadratic_coefficient (const struct cm_data * K, const int orbit,
   const int inum, const int iter, const int deg, quadratic_number_srcptr q);
int load_quadratic_coefficient (quadratic_number_ptr q,
   const struct cm_data * K, const int orbit,
   const int inum, const int iter, const int deg);
int save_recognition_failed (const struct cm_data * K, const int orbit,
   const int inum, const int iter);
int load_recognition_failed (const struct cm_data * K, const int orbit,
   const int inum, const int iter);
int save_polynomial_quadratic_backend (FILE * f, const struct cm_data * K, const int orbit,
   const int inum);
int save_polynomial_quadratic (const struct cm_data * K, const int orbit,
   const int inum);
int load_polynomial_quadratic (struct cm_data *K, const int orbit,
   const int inum);
int save_polynomials_quadratic (const struct cm_data * K, const char *filename);
int save_polynomials_quadratic_standalone_header (FILE * f, const struct cm_data * K);

#ifdef __cplusplus
}
#endif

#endif  /* IO_H_ */
