/* cputime.c -- timing.
 *
 * Copyright (C) 2010, 2011 INRIA
 *
 * This file is part of CMH.
 *
 * CMH is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * CMH is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/ .
 */

#include <stdlib.h>
#include <sys/types.h>		/* for cputime */
#include <sys/resource.h>	/* for cputime */
#include <sys/time.h>	/* for gettimeofday */
#include "cputime.h"

uint64_t microseconds()
{
    struct rusage res[1];
    getrusage(RUSAGE_SELF, res);
    uint64_t r;
    r = (uint64_t) res->ru_utime.tv_sec;
    r *= (uint64_t) 1000000UL;
    r += (uint64_t) res->ru_utime.tv_usec;
    return r;
}


/* cputime */
double cputime()
{
    double milli = (microseconds() / (uint64_t) 1000);
    return milli / 1.0e3;
}

double hires_wct()
{
    struct timeval tv[1];
    gettimeofday(tv, NULL);
    return (double) tv->tv_sec + 1.0e-6 * tv->tv_usec;
}

