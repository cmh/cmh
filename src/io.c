/* io.c -- checkpoint and restart for cm2.c
 *
 * Copyright (C) 2012, 2013 INRIA
 *
 * This file is part of CMH.
 *
 * CMH is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * CMH is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/ .
 */

/* I/O into files, mainly intended for checkpointing purposes */
#define _GNU_SOURCE
#include <assert.h>
#include <unistd.h>
#include "cm2.h"
#include "io.h"
#include "macros.h"
#include "lprintf.h"

int io_allow_load = 1;
int io_allow_save = 1;

/**************************************************************************/
/*                                                                        */
/* writing data types; return values are booleans 0 or 1                  */
/*                                                                        */
/**************************************************************************/

static int write_int (FILE *f, const int v)
{
   return (fprintf (f, "%d\n", v) > 0);
}


static int write_mpz (FILE *f, mpz_srcptr v)
{
   return (gmp_fprintf (f, "%Zx\n", v) > 0);
}


static int write_quadratic_number (FILE *f, quadratic_number_srcptr v)
{
   return (   write_mpz (f, v->a)
           && write_mpz (f, v->b)
           && write_mpz (f, v->c));
}

static int write_prec (FILE *f, const mpfr_prec_t v)
{
   return (mpfr_fprintf (f, "%Pu\n",  v) > 0);
}


static int write_mpfr (FILE *f, mpfr_srcptr v)
{
   return (   write_prec (f, mpfr_get_prec (v))
           && mpfr_out_str (f, 16, 0, v, GMP_RNDN)
           && (fprintf (f, "\n") > 0));
}


static int write_mpc (FILE *f, mpc_srcptr v)
{
   return (   write_mpfr (f, mpc_realref (v))
           && write_mpfr (f, mpc_imagref (v)));
}


static int write_mpfrx (FILE *f, mpfrx_srcptr v)
{
   int deg = mpfrx_get_deg (v);

   if (!(   write_prec (f, mpfrx_get_prec (v))
         && write_int (f, deg)))
      return 0;
   for (int i = 0; i <= deg; i++)
      if (!write_mpfr (f, mpfrx_get_coeff (v, i)))
         return 0;

   return 1;
}


static int write_period (FILE *f, const struct cm_period_data * p)
   /* Only those components are written that are relevant during the
      lifting phase.                                                 */
{
   int ok, i, j;

   ok  = write_prec (f, p->base_prec);
   ok &= write_int  (f, p->field);
   ok &= write_mpz  (f, p->pm_denom);
   /* The following loops mix the data strangely, but the file is not
      supposed to be human readable anyway.                           */
   for (i = 0; i < 3; i++) {
      for (j = 0; j < 4; j++)
         ok &= write_mpz (f, p->pm_t [i][j]);
      ok &= write_mpc (f, p->b [i]);
      ok &= write_mpc (f, p->j [i]);
   }
   ok &= write_prec (f, p->probably_correct_bits);

   return ok;
}


static int write_orbit (FILE *f, const struct cm_orbit_data * O)
   /* Only those components are written that are relevant during the
      lifting phase.                                                 */
{
   int ok, i;

   ok  = write_prec (f, O->minprec);
   ok &= write_prec (f, O->maxprec);
   for (i = 0; i < O->nperiods; i++) {
      ok &= write_period (f, &(O->periods [i]));
      if (i % 100 == 0)
         lprintf (LOG_NORMAL "Writing conjugate %i\n", i);
   }

   return ok;
}



/**************************************************************************/
/*                                                                        */
/* reading data types; return values are booleans 0 or 1                  */
/*                                                                        */
/**************************************************************************/

static int read_int (FILE *f, int *v)
{
   return (fscanf (f, "%d\n", v) == 1);
}


static int read_mpz (FILE *f, mpz_ptr v)
{
   return (gmp_fscanf (f, "%Zx\n", v) == 1);
}


static int read_quadratic_number (FILE *f, quadratic_number_ptr v)
{
   return (   read_mpz (f, v->a)
           && read_mpz (f, v->b)
           && read_mpz (f, v->c));
}


static int read_prec (FILE *f, mpfr_prec_t *v)
{
   unsigned long int p;
   int ok = fscanf (f, "%lu\n", &p);
   *v = (mpfr_prec_t) p;
   return (ok == 1);
}


static int read_mpfr (FILE *f, mpfr_ptr v)
{
   mpfr_prec_t prec;
   if  (!read_prec (f, &prec))
      return 0;
   mpfr_set_prec (v, prec);

   return (mpfr_inp_str (v, f, 16, MPFR_RNDN) > 0);
}


static int read_mpc (FILE *f, mpc_ptr v)
{
   return (   read_mpfr (f, mpc_realref (v))
           && read_mpfr (f, mpc_imagref (v)));
}


static int read_mpfrx (FILE *f, mpfrx_ptr v)
{
   int deg;
   mpfr_prec_t prec;
   if (!read_prec (f, &prec))
      return 0;
   mpfrx_set_prec (v, prec);
   if (!read_int (f, &deg))
      return 0;
   mpfrx_set_deg (v, deg);
   for (int i = 0; i <= deg; i++)
      if (!read_mpfr (f, mpfrx_get_coeff (v, i)))
         return 0;
   return 1;
}


static int read_period (FILE *f, struct cm_period_data *p)
{
   int ok, i, j;

   ok  = read_prec (f, &(p->base_prec));
   ok &= read_int  (f, &(p->field));
   ok &= read_mpz  (f, p->pm_denom);
   /* The following loops mix the data strangely, but the file is not
      supposed to be human readable anyway.                           */
   for (i = 0; i < 3; i++) {
      for (j = 0; j < 4; j++)
         ok &= read_mpz (f, p->pm_t [i][j]);
      ok &= read_mpc (f, p->b [i]);
      ok &= read_mpc (f, p->j [i]);
   }
   ok &= read_prec (f, &(p->probably_correct_bits));

   return ok;
}


static int read_orbit (FILE *f, struct cm_orbit_data *O)
{
   int ok, i;

   ok  = read_prec (f, &(O->minprec));
   ok &= read_prec (f, &(O->maxprec));
   for (i = 0; i < O->nperiods; i++) {
      ok &= read_period (f, &(O->periods [i]));
      if (i % 100 == 0)
         lprintf (LOG_NORMAL "Reading conjugate %i\n", i);
   }
   lprintf (LOG_NORMAL "Done reading conjugates\n");

   return ok;
}



/**************************************************************************/
/*                                                                        */
/* pretty printing polynomial names                                       */
/*                                                                        */
/**************************************************************************/

const char *get_polyname (int inum)
{
   static const char * names[] = {"H1", "H2hat", "H3hat" };
   return names [inum];
}



/**************************************************************************/
/*                                                                        */
/* helper functions for file names, file existence  and directory         */
/* creation                                                               */
/*                                                                        */
/**************************************************************************/

static char *get_prefix (const struct cm_data * K)
{
   char *s;

   assert (asprintf (&s, "%i_%i_%i", K->DAB [0], K->DAB [1], K->DAB [2]) > 0);

   return (s);
}


static char *get_filename_cii (const struct cm_data * K, const char c,
   const int i1, const int i2)
{
   char *tmp, *s;

   tmp = get_prefix (K);
   assert (asprintf (&s, "%s/%s.%c.%i.%i.tmp", tmp, tmp, c, i1, i2) > 0);
   free (tmp);

   return (s);
}


static char *get_filename_ciii (const struct cm_data * K, const char c,
   const int i1, const int i2, const int i3)
{
   char *tmp, *s;

   tmp = get_prefix (K);
   assert (asprintf (&s, "%s/%s.%c.%i.%i.%i.tmp", tmp, tmp, c,
                     i1, i2, i3) > 0);
   free (tmp);

   return (s);
}


static char *get_filename_ciiii (const struct cm_data * K, const char c,
   const int i1, const int i2, const int i3, const int i4)
{
   char *tmp, *s;

   tmp = get_prefix (K);
   assert (asprintf (&s, "%s/%s.%c.%i.%i.%i.%i.tmp", tmp, tmp, c,
                     i1, i2, i3, i4) > 0);
   free (tmp);

   return (s);
}


int get_iteration (const struct cm_data * K, const int orbit)
   /* Returns the highest iteration for which a temporary file exists
      with the results of Newton iteration; if no file is found, the return
      value is 0.                                                           */
{
   int iter = 20; /* should be enough for the foreseeable future */
   char *filename;
   FILE *f;
   if (!io_allow_load) return 0;
   do {
      filename = get_filename_cii (K, CM2_IO_NEWTON, orbit, iter);
      f = fopen (filename, "r");
      free (filename);
      iter--;
   } while (!f && iter >= 0);
   iter++;
   if (f)
      fclose (f);

   return iter;
}


int get_level_producttrees (const struct cm_data * K, const int orbit,
   const int iter)
   /* Returns the highest level for which the product trees have been
      stored in a file for the given orbit and iteration; if no such file
      exists, -1 is returned.                                             */
{
   int level = 20;
   char *filename;
   FILE *f;
   if (!io_allow_load) return -1;
   do {
      filename = get_filename_ciii (K, CM2_IO_TREE, orbit, iter, level);
      f = fopen (filename, "r");
      free (filename);
      level--;
   } while (!f && level >= 0);
   level++;
   if (f) {
      fclose (f);
      return level;
   }
   else
      return -1;
}


int make_checkpoint_directory (const struct cm_data * K)
{
   char *dirname, *command;
   int res;

   if (!io_allow_save) return 1;

   dirname = get_prefix (K);
   assert (asprintf (&command, "mkdir -p %s", dirname) > 0);
   free (dirname);
   res = system (command);
   free (command);

   return res;
}



/**************************************************************************/
/*                                                                        */
/* writing and reading of checkpoint files                                */
/*                                                                        */
/**************************************************************************/

int save_period (const struct cm_data * K, const int orbit, const int no,
                 const int iter, const struct cm_period_data * p)
   /* saves the data associated to one period in the given iteration
      into a checkpoint file */
{
   int ok;
   char *filename;
   FILE *f;

   if (!io_allow_save) return 1;

   filename = get_filename_ciii (K, CM2_IO_PERIOD, orbit, no, iter);
   f = fopen (filename, "w");
   free (filename);

   if (f) {
      ok = write_period (f, p);
      ok &= (fclose (f) == 0);
      return ok;
   }
   else
      return 0;
}


int load_period (struct cm_period_data *p, const struct cm_data * K,
   const int orbit, const int no, const int iter)
   /* For the return value, see load_orbit */
{
   int ok;
   char *filename;
   FILE *f;

   if (!io_allow_load) return -1;

   filename = get_filename_ciii (K, CM2_IO_PERIOD, orbit, no, iter);
   f = fopen (filename, "r");
   free (filename);

   if (f) {
      ok = read_period (f, p);
      ok &= (fclose (f) == 0);
      return ok;
   }
   else
      return -1;
}


int save_orbit (const struct cm_data * K, const int orbit, const int iter,
                const struct cm_orbit_data * O)
{
   int ok;
   char *filename;
   FILE *f;

   if (!io_allow_save) return 1;

   filename = get_filename_cii (K, CM2_IO_NEWTON, orbit, iter);
   f = fopen (filename, "w");
   free (filename);

   if (f) {
      ok = write_orbit (f, O);
      ok &= (fclose (f) == 0);
      return ok;
   }
   else
      return 0;
}


int load_orbit (struct cm_orbit_data *O, const struct cm_data * K,
   const int orbit, const int iter)
   /* Reads orbit data into O. Inside the function, O may be a component
      of K, so that K is modified along the way; the function itself uses
      K and orbit only to determine the file name.
      Returns -1 if the checkpoint file does not exist, 1 if reading worked
      without a problem and 0 if there was an error while reading; beware
      that in the latter case, partial reading may have destroyed the
      content of O.                                                         */
{
   int ok;
   char *filename;
   FILE *f;

   if (!io_allow_load) return -1;

   filename = get_filename_cii (K, CM2_IO_NEWTON, orbit, iter);
   f = fopen (filename, "r");
   free (filename);

   if (f) {
      ok = read_orbit (f, O);
      ok &= (fclose (f) == 0);
      return ok;
   }
   else
      return -1;
}


int save_producttrees (const struct cm_data * K, const int orbit,
   const int iter, const int level, const int no_pols, const int no_factors,
   mpfrx_t *vals)
{
   /* Saves a level of the subproduct "tree" computed in
      mpi_mpfrx_server_product_and_hecke into a file.    */
   int ok;
   char *filename;
   FILE *f;
   int i;

   if (!io_allow_save) return 1;

   filename = get_filename_ciii (K, CM2_IO_TREE, orbit, iter, level);
   f = fopen (filename, "w");
   free (filename);

   if (f) {
      lprintf (LOG_NORMAL "Writing product trees of level %i in iteration %i\n",
         level, iter);
      ok  = write_int (f, no_pols);
      ok &= write_int (f, no_factors);
      for (i = 0; i < no_pols * no_factors; i++)
         ok &= write_mpfrx (f, vals [i]);
      ok = ok && (fclose (f) == 0);
      return ok;
   }
   else
      return 0;
}


int load_producttrees (int *no_pols, int *no_factors, mpfrx_t ***vals,
   const struct cm_data * K, const int orbit,
   const int iter, const int level)
   /* The double array **vals is created during the reading of the product
      trees in the format required by mpfrx_product_and_hecke, not as the
      simple array used inside that function.
      For the return value, see load_orbit                                 */
{
   int ok;
   char *filename;
   FILE *f;
   int i, j;

   if (!io_allow_load) return -1;

   filename = get_filename_ciii (K, CM2_IO_TREE, orbit, iter, level);
   f = fopen (filename, "r");
   free (filename);

   if (f) {
      lprintf (LOG_NORMAL "Reading product trees of level %i in iteration %i\n",
         level, iter);
      ok  = read_int (f, no_pols);
      ok &= read_int (f, no_factors);
      *vals = (mpfrx_t **) malloc ((*no_pols) * sizeof (mpfrx_t *));
      for (i = 0; i < *no_pols; i++) {
         (*vals) [i] = (mpfrx_t *) malloc ((*no_factors) * sizeof (mpfrx_t));
         for (j = 0; j < *no_factors; j++) {
            mpfrx_init ((*vals) [i][j], 2, 2);
            ok &= read_mpfrx (f, (*vals) [i][j]);
         }
      }
      ok &= (fclose (f) == 0);
      return ok;
   }
   else
      return -1;
}

int save_polynomial_real (const struct cm_data * K, const int orbit,
   const int inum, const int iter, mpfrx_srcptr H)
   /* Saves H into a file named after the other input parameters. */
{
   int ok;
   char *filename;
   FILE *f;

   if (!io_allow_save) return 1;

   filename = get_filename_ciii (K, CM2_IO_RECONSTRUCT, orbit, inum, iter);
   f = fopen (filename, "w");
   free (filename);

   if (f) {
      lprintf (LOG_NORMAL "Writing floating point polynomial %s in iteration %i\n",
         get_polyname (inum), iter);
      ok = write_mpfrx (f, H);
      ok = ok && (fclose (f) == 0);
      return ok;
   }
   else
      return 0;
}


int load_polynomial_real (mpfrx_ptr H, const struct cm_data * K,
   const int orbit, const int inum, const int iter)
   /* For the return value, see load_orbit */
{
   int ok;
   char *filename;
   FILE *f;

   if (!io_allow_load) return -1;

   filename = get_filename_ciii (K, CM2_IO_RECONSTRUCT, orbit, inum, iter);
   f = fopen (filename, "r");
   free (filename);

   if (f) {
      lprintf (LOG_NORMAL "Reading floating point polynomial %s in iteration %i\n",
         get_polyname (inum), iter);
      ok = read_mpfrx (f, H);
      ok &= (fclose (f) == 0);
      return ok;
   }
   else
      return -1;
}


int save_quadratic_coefficient (const struct cm_data * K, const int orbit,
   const int inum, const int iter, const int deg, quadratic_number_srcptr q)
   /* Saves the correctly recognised coefficient in front of X^deg of
      polynomial inum in the given orbit. */
{
   int ok;
   char *filename;
   FILE *f;

   if (!io_allow_save) return 1;

   filename = get_filename_ciiii (K, CM2_IO_COEFF, orbit, inum, iter, deg);
   f = fopen (filename, "w");
   free (filename);

   if (f) {
      ok = write_quadratic_number (f, q);
      ok = ok && (fclose (f) == 0);
      return ok;
   }
   else
      return 0;
}


int load_quadratic_coefficient (quadratic_number_ptr q,
   const struct cm_data * K, const int orbit,
   const int inum, const int iter, const int deg)
   /* For the return value, see load_orbit */
{
   int ok;
   char *filename;
   FILE *f;

   if (!io_allow_load) return -1;

   filename = get_filename_ciiii (K, CM2_IO_COEFF, orbit, inum, iter, deg);
   f = fopen (filename, "r");
   free (filename);

   if (f) {
      ok = read_quadratic_number (f, q);
      ok &= (fclose (f) == 0);
      return ok;
   }
   else
      return -1;
}


int save_recognition_failed (const struct cm_data * K, const int orbit,
   const int inum, const int iter)
   /* Creates an empty file when the recognition of the coefficients as
      quadratic numbers failed in the given iteration and for the given
      polynomial and orbit. */
{
   char *filename;
   FILE *f;

   if (!io_allow_save) return 1;

   filename = get_filename_ciii (K, CM2_IO_FAILED, orbit, inum, iter);
   f = fopen (filename, "w");
   free (filename);

   if (f)
      return (fclose (f) == 0);
   else
      return 0;
}


int load_recognition_failed (const struct cm_data * K, const int orbit,
   const int inum, const int iter)
   /* For the return value, see load_orbit. */
{
   char *filename;
   FILE *f;

   if (!io_allow_load) return -1;

   filename = get_filename_ciii (K, CM2_IO_FAILED, orbit, inum, iter);
   f = fopen (filename, "r");
   free (filename);

   if (f)
      return (fclose (f) == 0);
   else
      return -1;
}


int save_polynomial_quadratic_backend (FILE * f, const struct cm_data * K, const int orbit, const int inum)
   /* Saves the inum-th polynomial of the orbit-th coset in K in the
      format described in README.format into a temporary file.       */
{
    int ok;

    int i;
    struct cm_orbit_data * O = &(K->orbits [orbit]);
    int deg = O->nclasses - (inum == 0 ? 0 : 1);

    lprintf (LOG_NORMAL "Writing symbolic polynomial %s\n",
            get_polyname (inum));
    if (inum == 0)
        ok = fprintf (f, "%i %i\n", O->nperiods, O->nclasses);
    else
        ok = 1;

    ok = ok && fprintf (f, "%i %i %i\n", orbit, inum, deg);
    ok = ok && factor_matrix_fprintf (f, O->Hdenom_fact [inum]);
    ok = ok && fprintf (f, "\n");
    for (i = 0 ; i <= deg; i++) {
        ASSERT_ALWAYS (mpz_cmp_ui (O->H [inum][i]->c, 1) == 0);
        ok = ok && gmp_fprintf (f, "%Zd %Zd\n",
                O->H [inum][i]->a, O->H [inum][i]->b);
    }
    return ok;
}

int save_polynomial_quadratic (const struct cm_data * K, const int orbit,
   const int inum)
   /* Saves the inum-th polynomial of the orbit-th coset in K in the
      format described in README.format into a temporary file.       */
{
   int ok;
   char *filename;
   FILE *f;

   /* Note that even if io_allow_save == 0 , we do save the polynomials
    * here, because it's used as a way to alleviate the memory cost.
    * Keeping everything in memory would be bad.
    */

   filename = get_filename_cii (K, CM2_IO_POLYNOMIAL, orbit, inum);
   f = fopen (filename, "w");
   free (filename);

   if (f) {
       ok = save_polynomial_quadratic_backend(f, K, orbit, inum);
       ok = ok && (fclose (f) == 0);
       return ok;
   }
   else
      return 0;
}


int load_polynomial_quadratic (struct cm_data *K, const int orbit,
   const int inum)
   /* Actually, does not read the full polynomial, but only its denominator,
      which is needed to guess a denominator for the next polynomial.
      K is modified by the function.
      For the return value, see load_orbit.
   */
{
   int ok, size, i;
   char *filename;
   FILE *f;
   mpz_t p;
   unsigned long int exp;

   if (!io_allow_load) return -1;

   filename = get_filename_cii (K, CM2_IO_POLYNOMIAL, orbit, inum);
   f = fopen (filename, "r");
   free (filename);

   if (f) {
      struct cm_orbit_data *O = &(K->orbits [orbit]);
      lprintf (LOG_NORMAL "Reading symbolic polynomial %s\n",
         get_polyname (inum));
      ok = 0;
      /* skip over the beginning of the file */
      if (inum == 0) {
         ok += fscanf (f, "%*i%*i\n");
      }
      ok += fscanf (f, "%*i%*i%*i\n");
      /* read the factor matrix */
      ok += fscanf (f, "%i", &size);
      mpz_init (p);
      mpz_set_ui (O->Hdenom [inum], 1);
      for (i = 0; i < size; i++) {
         ok += gmp_fscanf (f, "%Zi%lu", p, &exp);
         mpz_pow_ui (p, p, exp);
         mpz_mul (O->Hdenom [inum], O->Hdenom [inum], p);
      }
      mpz_clear (p);

      ok = ok && (fclose (f) == 0);
      return ok;
   }
   else
      return -1;
}

int save_polynomials_quadratic_standalone_header (FILE * f, const struct cm_data * K)
    /* concatenates the separately saved polynomial files into one big file */
{
    int ok;

    ok  = fprintf (f, "%d\n", OUTPUT_FORMAT_CODE);
    ok += fprintf (f, "%d %d %d\n", K->DAB[0], K->DAB[1], K->DAB[2]);
    ok += fprintf (f, "%d %d %d\n", K->DAB_r[0], K->DAB_r[1], K->DAB_r[2]);
    ok += fprintf (f, "%d\n", K->ncosets);
    ok += fprintf (f, "%d\n", K->invariant_set);
    return ok;
}

int save_polynomials_quadratic (const struct cm_data * K, const char *filename)
   /* concatenates the separately saved polynomial files into one big file */
{
   int ok;
   FILE *f;

   // if (!io_allow_save) return 1;

   f = fopen (filename, "w");
   if (f) {
       ok = save_polynomials_quadratic_standalone_header(f, K);
       ok = ok && (fclose (f) == 0);

      if (ok) {
         for (int i = 0; i < K->ncosets; i++)
            for (int inum = 0; inum < 3; inum ++) {
               char *file = get_filename_cii (K, CM2_IO_POLYNOMIAL, i, inum);
               char *command;
               assert (asprintf (&command, "cat %s >> %s", file, filename) > 0);
               ok = ok && (system (command) != -1);
               if (!io_allow_save) {
                   /* If we have *not* been instructed to create a temp
                    * file, then arrange so as to not have a temp file
                    * lingering after us.
                    */
                   unlink(file);
               }
               free (file);
               free (command);
            }
         return ok;
      }
      else
         return 0;
   }
   else
      return 0;
}
