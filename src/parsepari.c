/* parsepari.c -- wrapper for integer I/O in pari/gp
 *
 * Copyright (C) 2012, 2018 INRIA
 *
 * This file is part of CMH.
 *
 * CMH is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the
 * Free Software Foundation; either version 3 of the License, or (at your
 * option) any later version.
 *
 * CMH is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/ .
 */

#include <stdio.h>
#include <assert.h>
#include "parsepari.h"


FILE* file;
mpz_t z;

/****************************************************************************/
/*                                                                          */
/* Functions converting between pari and mpz; the following two functions   */
/* are taken from PARITWINE.                                                */
/*                                                                          */
/****************************************************************************/

void mpz_set_GEN (mpz_ptr z, GEN x)
   /* Sets z to x, which needs to be of type t_INT. */

{
   const long lx = lgefint (x) - 2;
   const long sign = signe (x);
   int i;

   assert (sizeof (long) == sizeof (mp_limb_t));

   if (typ (x) != t_INT)
      pari_err_TYPE ("mpz_set_GEN", x);

   if (sign == 0)
      mpz_set_ui (z, 0);
   else {
      mpz_realloc2 (z, lx * BITS_IN_LONG);
      z->_mp_size = sign * lx;
      for (i = 0; i < lx; i++)
         (z->_mp_d) [i] = *int_W (x, i);
   }
}

/****************************************************************************/

GEN mpz_get_GEN (mpz_srcptr z)
   /* Returns the GEN of type t_INT corresponding to z. */

{
   const long lz = z->_mp_size;
   const long lx = labs (lz);
   const long lx2 = lx + 2;
   int i;
   GEN x = cgeti (lx2);

   assert (sizeof (long) == sizeof (mp_limb_t));

   x [1] = evalsigne ((lz > 0 ? 1 : (lz < 0 ? -1 : 0))) | evallgefint (lx2);
   for (i = 0; i < lx; i++)
      *int_W (x, i) = (z->_mp_d) [i];

   return x;
}

/*****************************************************************************/

void parifopen (const char * filename)
{
   /* opens the file "filename" for reading */
   file = fopen (filename, "r");
   mpz_init (z);
}

/*****************************************************************************/

void parifclose (void)
{
   /* closes the file */
   fclose (file);
   mpz_clear (z);
}

/*****************************************************************************/

GEN parifreadint ()
{
   /* reads the next integer from the file */
   mpz_inp_str (z, file, 10);
   return mpz_get_GEN (z);
}

/*****************************************************************************/

GEN parifreadmod (GEN p)
{
   /* reads the next integer from the file and reduces it mod p */
   pari_sp av = avma;
   return gerepileuptoint (av, modii (parifreadint (), p));
}

/*****************************************************************************/

void parifskipint (long n)
{
   /* skips over n integers from the file */
   long i;

   for (i = 0; i < n; i++)
      mpz_inp_str (z, file, 10);
}

/*****************************************************************************/
