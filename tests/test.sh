#!/usr/bin/env bash

function clean_up {
   rm -rf 157_13_3*
}

clean_up
$CMH_BINDIR/../scripts/cmh-classpol.sh -f -p -c 13 3
if test $? -ne 0 ; then
   echo "Error when executing the cmh-classpol.sh script."
   exit 1
fi

cmp 157_13_3.pol $CMH_TESTDIR/example.pol
if test $? -ne 0 ; then
   echo "Wrong polynomial computed by the cmh-classpol.sh script."
   exit 1
else
   clean_up
fi

exit 0
