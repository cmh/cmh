/*

shimura.gp

Copyright (C) 2011, 2014 Emmanuel Thomé
Copyright (C) 2011, 2012, 2013, 2014, 2015, 2016, 2018, 2021, 2022 Andreas Enge

This file is part of CMH.

CMH is free software; you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the
Free Software Foundation; either version 3 of the license, or (at your
option) any later version.

CMH is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License along
with CM; see the file COPYING. If not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

install (Q_denom, G);

'x; 'y; 'z;
   /* order of variables:
      z for the real subfields
      y for the quartic CM fields
      x for their Galois closure  */


/***** complex conjugation and norm *****/

complex_conjugate_element (alpha) =
/* alpha an element of K or Kr in terms of y, or a vector of such elements
   returns the complex conjugate of alpha, also in terms of y */
{
   if (type(alpha) == "t_POLMOD",
      return (Mod (subst (alpha.pol, y, -y), alpha.mod));
   );
   return (subst (alpha, y, -y));
}


complex_norm (alpha) =
/* alpha an element of K or Kr as a t_POLMOD in terms of y
   returns the complex norm of alpha as a polynomial in z, the generator
      of K or Kr */
{
   my (n);
   my (complex_conjugate_element = complex_conjugate_element);

   n  = lift (alpha * complex_conjugate_element (alpha));

   return (z * polcoeff (n, 2) + polcoeff (n, 0));
}


complex_conjugate_ideal (K, af) =
/* K a CM field
   af an ideal of K
   returns the image (in HNF) of af under the complex conjugation
   automorphism y \mapsto -y.                                     */
{
   /* switch to polynomial basis, conjugate, switch back to integral basis,
      hnf                                                                   */
   return (idealhnf (K, Mat (matalgtobasis(K,
      complex_conjugate_element (K.zk*af)))));
};



/***** output and file handling *****/

delete_file (file) =
/* file the name of a file to be deleted, or 0 if nothing is to be done */
{
   if (file != 0,
      system (Strprintf ("rm -f %s", file));
   );
};


output_element (file, v) =
/* v a scalar to be printed
   file the name as a string of the file to print to, or 0 to print to
      stdout                                                               */
{
   if (file == 0,
      print1 (v),
   /* else */
      write1 (file, v);
   );
};


output (file, v) =
/* v a vector of elements to be printed
   file the name as a string of the file to print to, or 0 to print to
      stdout                                                               */
{
   for (i = 1, #v,
      output_element (file, v [i]);
   );
};


print_group (G) =
/* G an abelian group given by its cycle structure
   pretty prints the structure of G
*/
{
   if (G == 0,
      print ("C1"),
   /* else */
      print1 ("C", G [#G]);
      for (i = -(#G-1), -1, print1 (" x C", G [-i]));
      print ();
   );

};


print_period_matrices (cmfield, tofile) =
/* cmfield a quartic CM field
   If tofile==false, prints on screen the (reduced) period matrices in the
      format defined in README.format.
      Otherwise, prints them into the file named D_A_B.in, where D is the
      discriminant of the real-quadratic subfield.                        */

{
   my (K0, A, B, Arred, Brred, K0r, D, file, Omegavec, m, Omega, den, v);

   K0 = cmfield [1];
   A = polcoeff (K0.pol, 1);
   B = polcoeff (K0.pol, 0);
   Arred = cmfield [14];
   Brred = cmfield [15];
   K0r = cmfield [6];
   D = K0.disc;
   if (tofile,
      file = concat ([D, "_", A, "_", B, ".in"]);
      file = concat (Strexpand("$DATA_PREFIX"), file);
      delete_file (file),
   /* else */
      file = 0;
   );

   Omegavec = period_matrices (cmfield);

   /* starting output */
   output (file, [-3, "\n"]);   /* version string */
   output (file, [D, " ", A, " ", B, "\n"]);
   output (file, [K0r.disc, " ", Arred, " ", Brred, "\n"]);
   output (file, [#Omegavec, "\n"]);

   for (l = 1, #Omegavec,
      m = Omegavec [l];
      output (file, ["\n", sum (i = 1, #m, m [i][2]), "\n"]);
      for (k=1, #m,
         output (file, [m [k][2], "\n"]);
         Omega = m [k][1];
         den = Q_denom (Omega);
         output (file, [den, "\n"]);
         v = [Vecrev (Omega [1,1], 4), Vecrev (Omega [1,2], 4),
              Vecrev (Omega [2,2], 4)];
         for (i = 1, 3,
            for (j = 1, 3,
               output (file, [den * v [i][j], " "]);
            );
            output (file, [den * v [i][4], "\n"]);
         );
      );
   );
}


/***** basic CM field handling *****/

canonical_cm_parameters (A, B) =
/* A, B positive integers such that A^2 - 4*B > 0 and
   z^4 + A*z^2 + B is irreducible
   returns A' > 0, B' > 0 such that z^4 + A'*z^2 + B' defines the same
   field, and A' minimal.

   Note that if the Galois group of (y^4+A*y^2+B), this may return very
   inconsistent things.
   */
{
   my (K, b, M, m, param);
   K = bnfinit (y^4+A*y^2+B);
   b = K.zk;
   /* Write a basis for the Q-vector space of purely imaginary algebraic
      numbers, that is, numbers of the form alpha-\bar alpha, expressed
      by a matrix with respect to the integral basis of K.               */
   M = Mat (matalgtobasis (K,
          vector (4, i, b [i] - complex_conjugate_element (b [i]))));
   /* Intersect with OK and write as algebraic integers again. */
   b = matbasistoalg (K, Vec (matrixqz (M, -2)));
   /* Minimise the trace form on the Z-module. */
   m= qfminim (matrix (#b, #b, i, j, -nfelttrace (K, b[i]*b[j])/2));
   /* If there are several minimal integer directions, choose the optimal
      one. Example: 95 532 versus 95 2128. */
   param = vecsort (vector (#m [3], i,
      [-1/2 * nfelttrace (K, (b * m[3][,i])^2), nfeltnorm (K, b * m[3][,i])]),
      cmp);
   return (param [1]);
}


init_cmfield_basic (A, B) =
/* A, B positive integers such that A^2 - 4*B > 0 and
   z^2 + A*z + B is irreducible
   returns a CM field structure as a vector containing
       1 K0: the real subfield of K
       2 K: the CM field
       3 Krel: K as a relative extension of K0
       4 eps0: a fundamental unit of K0 that is not totally negative
       5 unitindex: the index of N_{K/K0}(U) in U0+
       6 K0r: the real subfield of the reflex field
       7 Kr: the reflex field
       8 Krrel: Kr as a relative extension of K0r
       9 Phi: an element of K whose square is a totally negative number
         in K0. A CM type is implicitly defined from Phi, by choosing the
         embeddings which send this element to complex numbers on the
         positive imaginary axis (there are only one or two possible
         values for this element, modulo equivalence of CM types,
         and multiplication by rational constants).
      10 noC: size of the Shimura group
      11 Lrel: the Galois closure L of K and Kr, as a relative extension of K
      12 Lrrel: L as a relative extension of Kr
         If K is already Galois, Lrel and Lrrel are set to 0.
      13 sigm: If K is galois, a generator of the Galois group of Kr.
         Otherwise, an isomorphism from Lrrel to Lrel.
      14 Arred,
      15 Brred: reduced, canonical parameters for the reflex field
      16 lambda: sqrt (K0.disc) as an element of Krel
      17 omeg: the second element of the integral basis of K0, as an
         element of Krel:
         lambda / 2 for K0.disc even, (lambda + 1) / 2 for K0.disc odd
*/

{
   my (K0, Krel, K, eps0, eps, unitindex, Ar, Br, K0r, Krrel, Kr,
       Phi, noC, Lrel, Lrrel, sigm, Arred, Brred, lambda, omeg, tmp);

   if (A^2 - 4*B <= 0,
      error ("*** Error in main: Equation does not define a quartic CM field");
   );

   tmp = canonical_cm_parameters (A, B);
   if ((A != tmp [1] || B != tmp [2]),
      warning ([A, B], " not minimal, use ", tmp, " instead!");
   );

   /* create CM field and subfields */
   K0 = sufficiently_accurate_bnfinit (z^2 + A*z + B, 1);

   Krel = rnfinit (K0, y^2-z, 1);
   lambda = nfroots (K0, x^2-K0.disc)[2];
   omeg = nfeltdiv (K0, nfeltadd (K0, lambda, K0.disc % 2), 2);
   lambda = rnfeltup (Krel, lambda);
   omeg = rnfeltup (Krel, omeg);

   K = bnfinit (nfinit (Krel), 1);
      /* K = bnfinit (y^4 + A*y^2 + B, 1); */

   /* units */
   eps0 = K0.fu [1];
   eps = K.fu [1];
   if (norm (eps0) == -1,
      unitindex = 1,
   /* else */
      if (bnfsignunit (K0)[1,1] == -1, eps0 = -eps0);
          /* forces eps0 to be totally positive */
      tmp = rnfeltup (Krel, eps0);
      if (tmp == eps || tmp == -eps,
         unitindex = 2,
      /* else */
         unitindex = 1;
      );
   );

   /* size of Shimura group */
   noC = K.no / K0.no;
   if (norm (eps0) == 1,
      noC = noC * 2 / unitindex;
   );

   /* cm types, reflex fields and Galois closures */
   Phi = y;
      /* The other possible CM type is defined by
         -y*(y^2+(-Tr_{K0/Q} (y^2))/2)). */
   if (galoisinit (K.pol),
      K0r = K0;
      Krrel = Krel;
      Kr = K;

      Lrel = 0;
      Lrrel = 0;
      sigm = nfgaloisconj (Kr)[3];
      if (sigm == y || sigm == -y,
         error ("*** Error in init_cmfield: sigma of order 1 or 2");
      ),
   /* else */
      /* Do not reduce the parameters for the reflex field, since it
         complicates the Galois closure computations. */
      Ar = 2*A;
      Br = A^2-4*B;
      tmp = canonical_cm_parameters (Ar, Br);
      Arred = tmp [1];
      Brred = tmp [2];
      K0r = bnfinit (z^2 + Ar*z + Br);
      Krrel = rnfinit (K0r, y^2-z);
      Kr = bnfinit (nfinit (Krrel));

      /* Force the definition of the closure which is consistent with
       * our notational choice */
      Lrel = rnfinit (K, x^2-Mod(2*y,K.pol)*x+Mod(2*y^2+A,K.pol));
      Lrrel = rnfinit (Kr, x^2-Mod(y,Kr.pol)*x+Mod((y^2+A)/2,Kr.pol));
      /* forces Lrel.polabs == Lrrel.polabs, so we can move consistently up and down */
      if (Lrel.polabs == Lrrel.polabs,
         sigm = x,
      /* else */
         warning ("in init_cmfield: different Galois closures, using an isomorphism");
         sigm = nfisisom (Lrrel.polabs, Lrel.polabs)[1];
      );
   );

   return ([K0, K, Krel, eps0, unitindex, K0r, Kr, Krrel, Phi, noC,
            Lrel, Lrrel, sigm, Arred, Brred, lambda, omeg]);
};


print_cmfield (cm) =
/* cm a cm field
   prints some information on class groups and so on */
{
   my (K0, K, eps0, unitindex, K0r, Kr, noC, Lrel, Arred, Brred,
       CKdata, Sdata);

   K0 = cm [1];
   K = cm [2];
   eps0 = cm [4];
   unitindex = cm [5];
   K0r = cm [6];
   Kr = cm [7];
   noC = cm [10];
   Lrel = cm [11];
   Arred = cm [14];
   Brred = cm [15];
   if (#cm > 17,
      CKdata = cm [18];
      Sdata = cm [19];
   );

   print ("G = ", if (Lrel == 0, "C4", "D4"));
   print ("DAB = ", K0.disc, " ", polcoeff (K0.pol, 1), " ", polcoeff (K0.pol, 0));
   print ("DABr = ", K0r.disc, " ", Arred, " ", Brred);
   print ("h0 = ", K0.no);
   print ("h1 = ", noC);
   print1 ("Cl0 = ");
   print_group (K0.clgp [2]);
   print1 ("Cl = ");
   print_group (K.clgp [2]);
   print ("h0r = ", K0r.no);
   print ("h1r = ", Kr.no / K0r.no);
   print1 ("Cl0r = ");
   print_group (K0r.clgp [2]);
   print1 ("Clr = ");
   print_group (Kr.clgp [2]);

   if (#cm > 15,
      print1 ("CK = ");
      print_group (CKdata [1]);
      print1 ("S = ");
      print_group (Sdata [1]);
   );

   print ("eps0norm = ", nfeltnorm (K0, eps0));
   print ("unitindex = ", unitindex);
};


explode_cmfield (Aloc, Bloc) =
/* calls init_cmfield and creates global variables from its result;
   useful for testing purposes                                      */
{
   cm = init_cmfield (Aloc, Bloc);
   K0 = cm [1];
   K = cm [2];
   Krel = cm [3];
   eps0 = cm [4];
   unitindex = cm [5];
   K0r = cm [6];
   Kr = cm [7];
   Krrel = cm [8];
   Phi = cm [9];
   noC = cm [10];
   Lrel = cm [11];
   Lrrel = cm [12];
   sigm = cm [13];
   Arred = cm [14];
   Brred = cm [15];
   lambda = cm [16];
   omeg = cm [17];

   if (#cm > 17,
      CKdata = cm [18];
      Sdata = cm [19];
   );

   A = Aloc;
   B = Bloc;
   Ar = 2*A;
   Br = A^2 - 4*B;
};



/***** n-systems *****/

strong_approximation (K0, a, nfac, s) =
   /* K0 a number field
      a an integral ideal of K0
      nfac the factorisation of an integral ideal n of K0
      s a vector of signs
      Return alpha in K0 with signs s under the real embeddings such that
         alpha*a is integral and coprime to nfac. */
{
   my (p, t, tsize, ty);

   /* We look for alpha with v_p (alpha) = - v_p (a) for all p | n.
      Compute the target valuations in a factor matrix t. */
   tsize = matsize (nfac)[1];
   t = matrix (tsize, 2, i, j,
      p = nfac [i, 1];
      if (j == 1,
         p,
      /* else */
         -idealval (K0, a, p)));
   /* To use idealchinese, we need elements in ty, indexed following t,
      with valuation in the p given by t. We use the feature documented
      in idealprimedec that v_p (p.gen [2]) == 1.
      Then looking for alpha with v_p (alpha - ty) >= v_p (ty) + 1
      implies that v_p (alpha) = v_p (ty). */
   ty = vector (tsize, i, nfeltpow (K0, t [i, 1].gen [2], t [i, 2]));
   for (i = 1, tsize, t [i, 2] = t [i, 2] + 1);
   return (idealchinese (K0, [t, s], ty));
}

semiprimitive_polynomial (cm, af, xi, n, nfac) =
/* cm a quartic CM field
   (af, xi) a polarised ideal of K
   n an integral ideal of K0
   nfac the factorisation of nf
   twists a 4-element vector of elements of OK0 that are coprime to nf
      and have signs [-1, -1], [-1, 1], [1, -1] and [1, 1] in this order
   Return a quadratic polynomial [A, B, C] that is semiprimitive modulo nf
      as in [EnSt22, Prop. 3.6], that is:
      A, B, C in OK0, A >> 0, gcd (A, B, C, nf) = 1,
      and if z0 is a root of A, then [1, z0] is an OK0-basis of an ideal
      representing the same polarised ideal class as (af, xi). */
{
   my (K0, Krel, lambda, omeg, a, a0, zvec, z0, y0, xitarget, xiz0,
       A, B, C, g, mul, signs);

   K0 = cm [1];
   Krel = cm [3];
   lambda = cm [16];
   omeg = cm [17];

   /* Compute an equivalent polarised ideal as
      (OK0 + z0 * OK0, xitarget). */
   a = rnfsteinitz (K0, rnfidealabstorel (Krel, af));
   a0 = a [2][2];
   zvec = vector (2, i,
      rnfeltreltoabs (Krel, rnfbasistoalg (Krel, a [1][,i])));
   z0 = zvec [2] / zvec [1];
   xitarget = complex_conjugate_element (zvec [1]) * zvec [1] * xi;
   y0 = bnfisprincipal (K0, a0);
   z0 = z0 * rnfeltup (Krel, y0 [2]);

   /* z0 is defined up to units of K0; we need to modify it such that the
      xi computed from z0 equals xitarget. The quotient of the xis is a
      totally real unit; it is enough to multiply z0 by it. */
   xiz0 = ((z0 - complex_conjugate_element (z0)) * lambda)^(-1);
   z0 = z0 * xiz0 / xitarget;

   /* Compute an integral minimal polynomial of z0. */
   C = rnfeltnorm (Krel, z0);
   B = -rnfelttrace (Krel, z0);
   A = denominator (idealnorm (K0, idealadd (K0, B, C)));
   B *= A;
   C *= A;

   /* Make gcd (A, B, C) coprime to n and A totally positive. */
   g = idealadd (K0, A, idealadd (K0, B, C));
   mul = strong_approximation (K0, g, nfac, nfeltsign (K0, A));
   A = nfeltmul (K0, A, mul);
   B = nfeltmul (K0, B, mul);
   C = nfeltmul (K0, C, mul);

   return ([A, B, C]);
}


coprime_from_semiprimitive_polynomial (K0, Q, n, nfac) =
/* K0 a real quadratic field
   Q a semiprimitive polynomial modulo n with entries in OK0
   n an integral ideal of K0
   nfac the factorisation of n
   Return an equivalent semiprimitive polynomial with gcd (A, n) = 1
      according to [EnSt22, Prop. 3.8].
*/
{
   my (A, B, C, Ap, Bp, Cp, n2, sqrtD, p1, a, b, c, Cc, y);

   [A, B, C] = Q;

   /* If necessary, handle the case of ideals above 2 in n first. */
   n2 = idealadd (K0, n, 2);
   if (n2 != 1,
      /* In the lucky case, one of A, B or C is coprime to n2. */
      if (idealadd (K0, A, n2) == 1,
         Ap = A;
         Bp = B;
         Cp = C,
      /* else */ if (idealadd (K0, C, n2) == 1,
         Ap = C;
         Bp = -B;
         Cp = A,
      /* else */ if (idealadd (K0, B, n2) == 1,
         Ap = A - B + C;
         Bp = 2*A - B;
         Cp = A,
      /* In the remaining case, 2=p1*p2 is necessarily split, that is,
         D=1 mod 8, n=2, one of p1 or p2 divides A and the other one
         divides C. We have an explicit expression for p1. */
         sqrtD = nfroots (K0, x^2 - K0.disc) [1];
         p1 = idealadd (K0, 2, (sqrtD + 1) / 2);
         /* Swap A and C if necessary so that A is coprime to p1. */
         if (idealadd (K0, A, p1) != 1,
            [A, B, C] = [C, -B, A]);
         /* We also have an explicit expression for a matrix
            M = [a, b; c, d] with a = d and entries a, b, c in OK0,
            of determinant 1, that keeps A modulo p1 and swaps A
            and C modulo p2. */
         a = (1 - sqrtD) / 2;
         b = (sqrtD - 3) / 2;
         c = (1 + sqrtD) / 2;
         Ap = nfeltmul (K0, a, nfeltmul (K0, A, a) - nfeltmul (K0, B, c))
              + nfeltmul (K0, C, nfeltpow (K0, c, 2));
         Cp = nfeltmul (K0, a, nfeltmul (K0, C, a) - nfeltmul (K0, B, b))
              + nfeltmul (K0, A, nfeltpow (K0, b, 2));
         Bp = -2 * nfeltmul (K0,
                 a, nfeltmul (K0, A, b) + nfeltmul (K0, C, c))
              + nfeltmul (K0, B, 1 + 2 * nfeltmul (K0, b, c)))));
      [A, B, C] = [Ap, Bp, Cp]);

   /* Now modulo each prime dividing n, a matrix [1, 0; c, 1] with
      c \in {0, 1, -1} can be taken; it is of determinant 1 and transforms
      A to A'=A-c*B+c^2*C:
      If p \nmid A (in particular if p | 2), take c = 0.
      Otherwise it is easy to verify that one of 1 or -1 works. */
   y = vector (matsize (nfac)[1], i,
      p1 = nfac [i, 1];
      if (idealval (K0, A, p1) == 0,
         0,
      /* else */ if (idealval (K0, A - B + C, p1) == 0,
         1,
      /* else */
         -1)));
   c = idealchinese (K0, nfac, y);
   Cc = nfeltmul (K0, C, c);
   Ap = nfeltmul (K0, Cc - B, c) + A;
   Bp = B - 2 * Cc;
   Cp = C;

   return ([Ap, Bp, Cp]);
}


n_system (cm, orbit, n) =
/* cm a quartic CM field
   orbit an orbit under the type norm subgroup of polarised ideal classes
      of the form (af, xi)
   n an integral ideal of K0
   Return an n-system modulo n for the orbit.
*/
{
   my (K0, K, Krel, nfac, nsystem, Q1, Qi, z1, zi, delta1, deltai, eps,
       B1, nsize, Ai, Bi, cong, b);

   K0 = cm [1];
   K = cm [2];
   Krel = cm [3];

   nfac = idealfactor (K0, n);

   /* Compute a system of semiprimitive forms modulo n. */
   nsystem = vector (#orbit, i,
      semiprimitive_polynomial (cm, orbit [i][1], orbit [i][2], n, nfac));

   /* We have seen cases where all entries of the n-system were divisible
      by the same rational integer; this is allowed and does not change the
      outcome, but rather ugly. Dividing by it makes the n-system smaller
      and prettier. */
   nsystem = nsystem / content (nsystem);

   if (n != 1,
      /* Make the A coprime to n. */
      for (i = 1, #orbit,
         nsystem [i] =
            coprime_from_semiprimitive_polynomial (K0, nsystem [i],
               n, nfac));

      /* Make the forms equiprimitive following [EnSt22], Lemma 4.8. */
      Q1 = nsystem [1];
      B1 = Q1 [2];
      z1 = good_root (cm, Q1);
      delta1 = nfeltadd (K, 2 * nfeltmul (K, rnfeltup (Krel, Q1 [1]), z1),
                            rnfeltup (Krel, B1));
      for (i = 2, #orbit,
         Qi = nsystem [i];
         zi = good_root (cm, Qi);
         deltai = nfeltadd (K,
                            2 * nfeltmul (K, rnfeltup (Krel, Qi [1]), zi),
                            rnfeltup (Krel, Qi [2]));
         eps = rnfeltdown (Krel, nfeltdiv (K, delta1, deltai));
         for (j = 1, 3,
            nsystem [i][j] = nfeltmul (K0, nsystem [i][j], eps)));

      /* Enforce the congruence condition on the B following the proof of
         [EnSt22], Theorem 4.7.
         Look for an algebraic integer b such that
            b \equiv (B - B1) / (2 * A) modulo n,
         then translate the form by b. */
      nsize = matsize (nfac)[1];
      for (i = 2, #orbit,
         Qi = nsystem [i];
         Ai = Qi [1];
         Bi = Qi [2];
         cong = nfeltdiv (K0, nfeltadd (K0, Bi, - B1), 2 * Ai);
         cong = vector (nsize, i, cong);
         b = idealchinese (K0, nfac, cong);
         Ai = nfeltmul (K0, Ai, b);
         nsystem [i][2] = nfeltadd (K0, Bi, - 2 * Ai);
         nsystem [i][3] = nfeltadd (K0,
            nfeltmul (K0, nfeltadd (K0, Ai, -Bi), b), nsystem [i][3]));
   );

   return (nsystem);
}


/***** Symplectic bases *****/

good_root (cm, Q) =
/* cm a quartic CM field
   Q a quadratic polynomial over OK0 (as a vector [A, B, C])
      representing a polarised ideal class
   Return a root z0 of Q in K for which the associated CM type is
      Phi. */
{
   my (K0, K, Krel, Phi, lambda, A, B, C, z0, z1);

   K0 = cm [1];
   K = cm [2];
   Krel = cm [3];
   Phi = cm [9];
   lambda = cm [16];

   [A, B, C] = apply (x -> rnfeltup (Krel, x), Q);
   z0 = nfroots (K, A*x^2 + B*x + C) [1];
   z1 = complex_conjugate_element (z0);
   /* We need either z0 or its complex conjugate; one of them belongs
      to Phi, the other one to -Phi. */
   if (nfeltsign (K0, rnfeltdown (Krel,
                  nfeltmul (K, Phi, nfeltmul (K, lambda, z0 - z1))))
      != [1, 1],
      z0 = z1);

   return (z0);
}


symplectic_basis (cm, Q) =
/* cm a quartic CM field
   Q a quadratic polynomial over OK0 (as a vector [A, B, C])
      representing a polarised ideal class
   Return a symplectic basis of the class of Q in polynomial form. */
{
   my (K0, omeg, z0, basis);

   K0 = cm [1];
   omeg = cm [17];

   z0 = good_root (cm, Q);

   basis = lift ([z0*omeg, z0, -1, (K0.disc % 2)-omeg]);

   return (basis);
}


symplectic_bases (cm) =
/* cm a quartic CM field
   Return the associated symplectic bases as a vector (with one entry for
   each coset) of vectors of pairs [basis, flag]. */
{
   my (K0, Krel, t, nsystems, bases, basis, flag);

   K0 = cm [1];
   Krel = cm [3];

   t = triples (cm);

   nsystems = vector (#t, i, n_system (cm, t [i], 1));
   bases = vector (#t, i,
      vector (# t[i], j,
         basis = symplectic_basis (cm, nsystems [i][j]);
         flag = t [i][j][3];
         [basis, flag]));

   return (bases);
}


/***** Type norms *****/

reduce_type_norm (cm, tn) =
/* cm as output by init_cmfield
   tn a tuple [a, n] as output by type_norm
   returns an equivalent, but smaller representative of the given class
      in the Shimura group
*/
{
   my (K0, K, tnred, alpha, u, pow);

   K0 = cm [1];
   K = cm [2];

   /* First reduce the ideal and keep track of the second component. */
   tnred = idealred (K, [tn [1], 1]);
   alpha = complex_norm (nfbasistoalg (K, tnred [2]));
   tnred [2] = Mod (tn [2] / alpha, K0.pol);

   /* Then reduce the second component modulo N_{K/K0}(U) = <eps0^2> in the
      primitive, not Q(zeta_5) case, see Lemma II.3.3 of [Streng10]. */
   /* determine the optimal unit for reduction */
   u = tnred [2]
       / nfbasistoalg (K0,
            bnfisprincipal (K0, tnred [2], 3) [2]);
   /* determine the power of K0.fu [1] and round to a multiple of 2 */
   pow = bnfisunit (K0, u) [1];
   pow = sign (pow) * (abs (pow) - pow % 2);
   if (pow != 0,
      tnred [2] /= K0.fu [1]^pow;
   );

   return (tnred);
}


type_norm (cmfield, ar) =
/* cmfield: as output by init_cmfield
   ar: an ideal of Kr
   returns m(ar) (in the notation of BrGrLa11), that is, a pair [a, n]
      with a the type norm of ar and n = N_{Kr/Q} (ar)                 */
{
   my (Kr, Lrel, Lrrel, sigm, a, br, b, n);

   Kr = cmfield [7];
   Lrel = cmfield [11];
   Lrrel = cmfield [12];
   sigm = cmfield [13];

   if (Lrel == 0, /* Galois case */
      a = idealmul (Kr, ar, nfgaloisapply (Kr, sigm, ar)),
   /* else non Galois case */
      br = rnfidealup (Lrrel, ar);
      b = lift (subst (br, x, Mod (sigm, Lrel.polabs)));
      a = rnfidealnormrel (Lrel, b);
   );
   n = idealnorm (Kr, ar);

   return ([a, n]);
};


type_norm_reduced (cm, ar) =
/* input as for type_norm
   output is additionally reduced by integers
*/
{
   return (reduce_type_norm (cm, type_norm (cm, ar)));
}



/***** symbolic treatment of embedding of K0r */

to_standard_basis_K0r (f, Ar) =
/* f an element of K0r given as a linear polynomial in z
   Ar the linear coefficient of K0r.pol
   returns [a, b] such that f = a + b*sqrt (B),
      where B = (Ar^2 - 4*B) / 16 = K0r.pol.disc / 16,
      and z is interpreted as (-Ar - sqrt (K0r.pol.disc)/2.
      Then when the complex embedding of y is that of maximal, positive
      imaginary part, the embedding of a + b*sqrt (B) is that with the
      positive root of B.
      Notice also that if f has integral coefficients, then the return vector
      is also integral.
*/
{
   my (f0, f1);

   f0 = polcoeff (f, 0);
   f1 = polcoeff (f, 1);

   return ([f0 - (Ar/2) * f1, -2*f1]);
}


cmp_K0r (f, g, Ar, B) =
/* f, g two elements of K0r given as linear polynomials in z
   Ar the linear coefficient of K0r.pol in front of z
   B the constant coefficient of K0.pol
   returns -1, 0, 1 depending on whether f <, =, > g after the embedding of
      K0r into R that sends z to the smaller root of K0r.pol
*/
{
   my (h);

   if (f == g,
      return (0);
   );

   h = to_standard_basis_K0r (f - g, Ar);

   return (cmp_zero_K0r (h, B));
}


cmp_zero_K0r (h, B) =
/* h an element of K0r given as a two-element vector such that
      h = h [1] + h [2] * sqrt (B)
   B the constant coefficient of K0.pol
   returns -1, 0, 1 depending on whether h <, =, > 0 after the embedding of
      K0r into R that sends B to its positive root
*/
{
   if (h [1] <= 0 && h [2] <= 0,
      if (h [1] == 0 && h [2] == 0,
         return (0)
      ,
         return (-1);
      );
   );
   if (h [1] >= 0 && h [2] >= 0,
      return (1);
   );
   /* one of h1 and h2 is positive, the other one negative */
   if (h [1]^2 > B * h [2]^2,
      return (sign (h [1]));
   ,
      return (sign (h [2]));
   );
}


floor_K0r (f, Ar, B) =
/* f an element of K0r given as a linear polynomial in z
   Ar the linear coefficient of K0r.pol in front of z
   B the constant coefficient of K0.pol
   returns the rational integer, rounded down, obtained when replacing z in f
      by the smaller root of K0r.pol
*/
{
   my (h, a, b, res);

   h = to_standard_basis_K0r (f, Ar);
      /* now f = h[1] + h[2] * sqrt (B); we use the positive root */

   /* We use floor(a) + floor(b) <= floor(a+b) <= floor(a) + floor(b) + 1. */
   a = h [1] \ 1;
   b = sqrtint ((h [2]^2 * B) \ 1);
   if (h [2] < 0,
      b = -b-1;
   );
   res = a + b;
   if (cmp_zero_K0r ([res + 1 - h [1], -h[2]], B) <= 0,
      return (res + 1);
   ,
      return (res);
   );
}



/***** Siegel space *****/
/* Matrices in Sp_4 (\Z) are given as 2x2 matrices with entries
   that are 2x2 matrices */

minkowski_reduction_K0r (M) =
/* M a real, symmetric, positive definite 2x2 matrix with entries in K0r,
      given as t_POLMOD in z
   returns U such that U*M*transpose (U) = [a, b; b, c] satisfies
      0 <= 2*b <= a <= c   */
{
   my (K0rpol, Ar, B, S, U, R, ok, q);

   K0rpol = M [1, 1].mod;
   Ar = polcoeff (K0rpol, 1);
   B = poldisc (K0rpol) / 16;

   S = [0, 1; 1, 0];
      /* Exchanges a and c. */

   U = matid (2);
   ok = 0;
   until (ok,
      ok = 1;
      /* Obtain -a < 2*b <= a. */
      q = floor_K0r (lift (M [1,2] / M [1, 1]) + 1/2, Ar, B);
      R = [1, 0; -q, 1];
      M = R * M * mattranspose (R);
      U = R * U;
      /* Obtain a <= c. */
      if (cmp_K0r (lift (M [1, 1] - M [2,2]), 0, Ar, B) > 0,
         ok = 0;
         M = S * M * S;
         U = S * U;
      );
   );
   /* Force b >= 0. */
   if (cmp_K0r (lift (M [1, 2]), 0, Ar, B) < 0,
      R = [1, 0; 0, -1];
      M = R * M * R;
      U = R * U;
   );

   return (U);
}


real_reduction_K0r (M, Ar, B) =
/* M a symmetric 2x2 matrix with entries in K0r, given as linear polynomials
      in z
   Ar the linear coefficient of K0r.pol
   B the constant coefficient of K0.pol
   returns T such that M + T = [a, b; b, c] satisfies
      -1/2 <= a, b, c < 1/2 after the embedding that sends z to the smaller
      real root of K0r.pol
*/
{
   my (t1, t2, t3);

   t1 = -floor_K0r (M [1, 1] + 1/2, Ar, B);
   t2 = -floor_K0r (M [1, 2] + 1/2, Ar, B);
   t3 = -floor_K0r (M [2, 2] + 1/2, Ar, B);

   return ([t1, t2; t2, t3]);
}


siegel_transform_apply (T, M) =
/* T = [A, B; C, D] a matrix in Sp_4 (\Z)
   M a complex, symmetric 2x2 matrix
   returns T \circ M = (A M + B)*(C M + D)^(-1)   */
{
   return ((T [1,1] * M + T [1,2]) * (T [2,1] * M + T [2,2])^(-1));
}


gottschling_reduction_Kr (M, Ar, B) =
/* M a period matrix with entries in Kr, given as t_POLMOD
   Ar the linear coefficient of K0r.pol
   B the constant coefficient of K0.pol
   returns [N, minnormdet], where N = [A, B; C, D] is the one of the 19
      matrices in Sp_4 (\Z) determined in Gottschling59 (and given
      explicitly on page 134 of Dupont06) such that |det (C*M+D)| is minimal;
      and minnormdet is the square of this minimal value as a t_POLMOD
      representing an element of K0r */
{
   my (a, b, c, d, normdet, minnormdet, index);

   a = vector (19, i, matrix (2, 2));
   a [16] = matid (2);
   a [17] = matid (2);
   a [18] = matid (2);
   a [19] = -matid (2);

   b = vector (19, i, -matid (2));
   b [18] = matrix (2, 2);
   b [19] = matrix (2, 2);

   c = vector (19, i, matid (2));
   c [16][2,2] = 0;
   c [17][1,1] = 0;
   c [18] = [1, -1; -1, 1];
   c [19] = [1, -1; -1, 1];

   d = vector (19, i, matrix (2, 2));
   d [ 2][1,1] =  1;
   d [ 3][1,1] = -1;
   d [ 4][2,2] =  1;
   d [ 5][2,2] = -1;
   d [ 6] =  matid (2);
   d [ 7] = -matid (2);
   d [ 8] = [-1,  0;  0,  1];
   d [ 9] = [ 1,  0;  0, -1];
   d [10] = [ 0,  1;  1,  0];
   d [11] = [ 0, -1; -1,  0];
   d [12] = [ 1,  1;  1,  0];
   d [13] = [-1, -1; -1,  0];
   d [14] = [ 0,  1;  1,  1];
   d [15] = [ 0, -1; -1, -1];
   d [16][2,2] = 1;
   d [17][1,1] = 1;
   d [18] =  matid (2);
   d [19] = -matid (2);

   for (i = 1, 19,
      normdet = complex_norm (matdet (c [i] * M + d [i]));
      if (i == 1 || cmp_K0r (normdet, minnormdet, Ar, B) < 0,
         minnormdet = normdet;
         index = i;
      );
   );

   return ([[a [index], b [index]; c [index], d [index]], minnormdet]);
}


siegel_reduction_Kr (M, Krpol) =
/* M a period matrix with entries in Kr
   Krpol the polynomial defining Kr
   returns the reduced representative of M in the fundamental domain of
      Sp_4 (Z) with respect to the embedding that sends y to the largest root
      of Krpol
*/
{
   my (Ar, K0rpol, B, U, T, Nmin, ok, Mreal, Mimag, tmp);

   Ar = polcoeff (Krpol, 2);
   K0rpol = z^2 + Ar * z + polcoeff (Krpol, 0);
   B = poldisc (K0rpol) / 16;
   M = Mod (M, Krpol);

   until (ok,
      tmp = lift (M);
      Mimag = matrix (2, 2, i, j,
         Mod (polcoeff (tmp [i, j], 3) * z + polcoeff (tmp [i, j], 1),
              K0rpol));
         /* the matrix with entries in K0r such that y*Mimag = i*imag (MKr) */
      U = minkowski_reduction_K0r (Mimag);
      M = U * M * mattranspose (U);

      tmp = lift (M);
      Mreal = matrix (2, 2, i, j,
         polcoeff (tmp [i, j], 2) * z + polcoeff (tmp [i, j], 0));
      T = real_reduction_K0r (Mreal, Ar, B);
      M = M + T;

      Nmin = gottschling_reduction_Kr (M, Ar, B);
      ok = (cmp_K0r (Nmin [2], 1, Ar, B) >= 0);
      if (!ok,
         M = siegel_transform_apply (Nmin [1], M);
      );
   );

   return (lift (M));
}



/***** Period matrices *****/

period_matrix (cmfield, basis) =
/* cmfield a quartic CM field
   basis the symplectic basis of an ideal of K in polynomial form
   returns a period matrix with entries in Kr
   FIXME: so far, only the dihedral case is implemented */
{
   my (K, Kr, Lrrel, a, asigma, E, V, Z, den);

   K = cmfield [2];
   Kr = cmfield [7];
   Lrrel = cmfield [12];

   /* We take x as the generating element of Lrrel over Kr. We used to
    * have x==-y, but newer text in the article now sets x==y.
    *
    * For expressing y^sigma, we have y^sigma = y^r - y ;

    * previously this was the pari object y+x (y representing the
    * generating element of Kr, hence the mathematical object y^r ; x
    * representing the generating element of Lrrel over Kr, i.e. -y).

    * Now that we change the meaning, it's rather y-x.
    */
      /* basis as elements of K */
   if(Lrrel==0,
        /* Galois */
       sigm = cmfield [13];
       a = vector(4,i,Mod(basis[i],K.pol));
       asigma = vector(4,i,Mod(subst(basis[i],y,sigm),K.pol));
   ,
        /* non-Galois */
       a = vector (4, i, Mod (subst (basis [i], y, x), Lrrel [1]));
          /* basis as elements of Lrrel = L/Kr */
       asigma = vector (4, i, Mod (subst (basis [i], y, y-x), Lrrel [1]));
          /* image of a under the non-trivial Galois automorphism of L/Kr */
   );

   E = matrix (2, 2, i, j, if (i == 1, a [j], asigma [j]));
   V = matrix (2, 2, i, j, if (i == 1, a [j+2], asigma [j+2]));
   Z = simplify (lift (V^(-1) * E));
   den = denominator (Z);
   Z = (((1 / den) % Kr.pol) * (den * Z)) % Kr.pol;

   /* In the cyclic case, note that we still have something wrt Kr.pol
    * which is the minimal polynomial of y, not of y+y^sigma ! */

   return (Z);
}


reduced_period_matrix (cmfield, basis) =
/* cmfield a quartic CM field
   basis the symplectic basis of an ideal of K in polynomial form
   returns a period matrix with entries in Kr that is reduced into
      the fundamental domain, where we assume that the CM type is
      given by y
   FIXME: so far, only the dihedral case is implemented */
{
   my (K0, A, B, Kr, Z);

   K0 = cmfield [1];
   A = polcoeff (K0.pol, 1);
   B = polcoeff (K0.pol, 0);
   Kr = cmfield [7];

   if (cmfield [9] != y,
      error ("*** reduced_period_matrix only works for the standard CM type");
   );

   Z = period_matrix (cmfield, basis);

   return (siegel_reduction_Kr (Z, Kr.pol));
}


unique_period_matrix_in_conjugation_orbit (Omega) =
/* Omega a period matrix with entries in Kr, given as polynomials in y
 * returns one of Omega or -\bar Omega (which yields theta function values
 * that are complex conjugates of those corresponding to Omega),
 * according to an arbitrary criterion: the first non-zero coefficient of
 * even exponent encountered in Omega is made positive.
 */
{
   my (coeff);

   for (i = 1, 2,
      for (j = 1, 2,
         for (k = 0, 1,
            coeff = polcoeff (Omega [i,j], 2*k);
            if (coeff != 0,
               if (coeff > 0,
                  return (Omega);
               ,
                  return (-subst (Omega, y, -y));
               );
            );
         );
      );
   );

   return (Omega);
}


period_matrices (cmfield) =
/* cmfield a quartic CM field
   returns period matrices for factors of the class polynomial
      Precisely, the return value is a vector with as many elements as there
      are cosets of the Shimura subgroup (or factors of the class polynomial);
      each entry is a list with as many entries as there are period
      matrices; each of these entries is a two-element vector, the first
      element being the (reduced) period matrix, the second one being 1 or 2
      for a real or a pair of complex conjugate roots of the class polynomial
*/
{
   my (bases, Omegavec);
   bases = symplectic_bases (cmfield);
   Omegavec = vector (#bases, i, vector (#bases [i], j,
      [unique_period_matrix_in_conjugation_orbit
          (reduced_period_matrix (cmfield, bases [i][j][1])),
       bases [i][j][2]]));

   /* sort to obtain well-defined output */
   Omegavec = vecsort (vector (#Omegavec, i,
                       vecsort (Omegavec [i], cmp)), cmp);

   return (Omegavec);
}



/***** Other CM computations *****/

is_totally_positive (K0, xi) =
/* K0: a real quadratic field in the variable z
   xi: an element of K0, given as an expression in z
   returns 1 if xi is totally positive, 0 otherwise   */
{
    return (nfelttrace (K0, xi) >= 0 && nfeltnorm (K0, xi) >= 0);
};


is_totally_positive_imaginary (K0, Krel, Phi, xi) =
/* K0 the real subfield of the CM field
   Krel the CM field as a relative extension of K0
   Phi an element defining a CM type phi
   xi an element of K
   checks whether phi (xi) lies on the positive imaginary axis
   for all phi \in Phi -- which is equivalent to saying that xi/Phi is a
   totally positive element of K0.
 */
{
    return (is_totally_positive (K0, rnfeltdown (Krel, xi/Phi)));
};


explode_classgroup_quotient (K, orders, gens) =
/* K a number field
   orders and gens determine a quotient of the class group of K given
     by the generators gens and their orders, and no further relations
   returns a vector containing all elements of the class group quotient;
      each entry is itself a vector with two components: an ideal and its
      decomposition (as a column vector) over the given generators */
{
   my (res, h, pow);

   res = vector (prod (i = 1, #orders, orders [i]));
   res [1] = [matid (poldegree (K.pol)), vectorv (#orders)];

   h = 1; /* current cardinality of res */
   for (i = 1, #orders,
      /* treat generator i and its powers */
      pow = gens [i];
      for (j = 1, orders [i] - 1,
         /* treat power j of generator i */
         for (k = 1, h,
            res [j*h + k] = [idealmul (K, res [k][1], pow, 1),
                             res [k][2]];
            res [j*h + k][2][i] = j;
         );
         pow = idealmul (K, pow, gens [i], 1);
      );
      h = h * orders [i];
   );

   return (res);
};


explode_classgroup (K) =
/* K a number field
   returns a vector containing all class group elements of K
      each entry is itself a vector with two components: the ideal and its
      decomposition (as a column vector) over the class group generators */
{
   return (explode_classgroup_quotient (K, K.clgp.cyc, K.clgp.gen));
};


explode_abstractgroup (v) =
/* v a vector of the elementary divisors of an abstract group
   returns a vector containing all elements of the abstract group
      each entry is itself a column vector w of the same length as v,
      with 0 <= w [i] < v [i] */
{
   my (res, h, i);

   res = vector (prod (i = 1, #v, v [i]));
   res [1] = vectorv (#v);
   h = 1; /* current cardinality of res */
   for (i = 1, #v,
      /* treat generator i and its powers */
      for (j = 1, v [i] - 1,
         /* treat power j of generator i */
         for (k = 1, h,
            res [j*h + k] = res [k];
            res [j*h + k][i] = j;
         );
      );
      h = h * v [i];
   );

   return (res);
};


cmp_norm (v1, v2) =
/* v1, v2 entries of a vector as returned by explode_classgroup
   returns -1, 0, 1 depending on whether v1 <, =, > v2,
      where the order is given first by the norm of the ideal, and,
      for equal norm, lexicographically with respect to the class group
      decomposition
*/
{
   my (n1, n2);

   n1 = prod (i = 1, 4, v1 [1][i, i]);
   n2 = prod (i = 1, 4, v2 [1][i, i]);
   if (n1 < n2,
      return (-1);
   , if (n1 > n2,
      return (1);
   ););

   for (i = 1, #v1 [2],
      if (v1 [2][i] < v2 [2][i],
         return (-1);
      ,
      if (v1 [2][i] > v2 [2][i],
         return (1);
      ););
   );

   return (0);
};


cmp_weight (v1, v2, w) =
/* v1, v2 entries of a vector as returned by explode_abstractgroup
   returns -1, 0, 1 depending on whether v1 <, =, > v2,
      where the order is given first by the product of the entries weighted
      by w (a vector of the same lenght as v1 and v2), and, for equal weight,
      lexicographically
*/
{
   my (n1, n2);

   n1 = sum (i = 1, #v1, v1 [i] * w [i]);
   n2 = sum (i = 1, #v2, v2 [i] * w [i]);
   if (n1 < n2,
      return (-1);
   , if (n1 > n2,
      return (1);
   ););

   for (i = 1, #v1,
      if (v1 [i] < v2 [i],
         return (-1);
      ,
      if (v1 [i] > v2 [i],
         return (1);
      ););
   );

   return (0);
};


find_basepoint (cmfield) =
/* cmfield as output by init_cmfield
   returns a vector [b, xi, bdlog] such that b is an ideal of OK,
      xi as in Section 3.1, p. 45 of Streng10,
      and bdlog the decomposition of b over the classgroup of OK
*/
{
   my (K0, K, Krel, Phi, dlogconj, dlogdiff, Clexp,
       b, bdlog, prin, xi0, units, xi);

   K0 = cmfield [1];
   Krel = cmfield [3];
   K = cmfield [2];
   Krel = cmfield [3];
   Phi = cmfield [9];
   dlogconj = Mat (vector (#K.clgp [2], i,
      bnfisprincipal (K, complex_conjugate_ideal (K, K.clgp [3][i]), 0)));
   dlogdiff = bnfisprincipal (K, K.diff, 0);
   Clexp = vecsort (explode_classgroup (K), cmp_norm);

   for (i = 1, #Clexp,
      bdlog = Clexp [i][2];
      prin = bdlog + dlogconj * bdlog + dlogdiff;
      for (j = 1, #prin,
         prin [j] %= K.clgp [2][j];
      );
      if (!prin,
         b = Clexp [i][1];
         prin = bnfisprincipal (K, idealinv (K, idealmul (K, K.diff,
                   idealmul (K, b, complex_conjugate_ideal (K, b)))), 3) [2];
         xi0 = nfbasistoalg (K, prin);
         units = [1, -1, K.fu [1].pol, -K.fu [1].pol];
         for (j = 1, 4,
            xi = (units [j] * xi0) % K.pol;
            /* It sometimes happens (for [5,5,5]) that the ideal
             * (D*a*abar)^-1 is a principal ideal of OK, but generated by
             * something which is not totally imaginary. This is because
             * for D=5 we have extra units */
            if (polcoeff(lift(rnfeltabstorel(Krel, xi)), 0) == 0,
                if (is_totally_positive_imaginary (K0, Krel, Phi, xi),
                   return ([b, xi, bdlog]);
                );
            );
         );
      );
   );
};



/* Shimura group computations
 *
 * We manipulate two groups:
 * CK: The Shimura group.
 * S:  The subgroup of CK which is generated by the image of the class
 *     group of the reflex field under the type norm map.
 *
 * CK enjoys the following exact sequence, where U and U0 are the
 * unit groups of OK and OK0, respectively:
 * 1 \to U0+/N_{K/K0}(U) \to CK \to Cl(OK) \to Cl+(OK0) \to 1
 *
 * In the primitive case different from Q(zeta_5), we have U=U0=<-1><eps>
 * by Lemma II.3.3 of [Streng10], where we can assume eps not totally
 * negative.
 * So if eps has norm -1, then U0+/N_{K/K0}(U)=1;
 * otherwise U0+/N_{K/K0}(U)=1={1,eps}.
 *
 * In order to obtain the structure of CK, the procedure goes as follows:
 *
 * 0: Needed basic equipment on CK:
 *    - basic product operation.
 *    - identity element.
 *    - equality test.
 *    - reduction operation.
 *    - factorback routine.
 * 1: Compute the kernel of the map Cl(OK) \to Cl+(OK0):
 *    - apply bnfisprincipal in the narrow class group Cl+(OK0) on the
 *      images of the generators of Cl(OK).
 *    - knowing the Smith invariants of Cl+(OK0), deduce combinations of
 *      these generators, which yield zero image (hnf computation). These
 *      combinations thus generate the kernel.
 * 2: Get relations among the ideals generating this kernel.
 *    - use the Smith invariants of Cl(OK) to deduce relations among the
 *      above generators. (hnf again).
 * 3: Obtain generators for CK:
 *    - Lift arbitrarily the above generators to pairs in CK (pick
 *      arbitrary totally positive generators). If the group
 *      U0+/N_{K/K0}(U) is not trivial, add an extra generator (OK,eps0).
 *      Together, the corresponding pairs are called the ``primary''
 *      generators of CK, and denoted mapker_lifts.
 *    - The relation matrix computed in (2) is not complete, becaude not
 *      everything which maps to 0 in Cl(OK) is necessarily 0 in CK. Thus
 *      compute all combinations, and possibly add an extra fixing
 *      (OK,eps0) component. We thus form an expanded relation matrix.
 * 4: Finish:
 *    - Use the above relation matrix to compute the Smith invariants of
 *      CK, as well as its generators.
 *
 * An important by-product of this computation is a generalized dlog
 * procedure. Obtaining the dlog of a given element a of CK goes as
 * follows:
 *
 * 0: map to Cl(OK)
 *    - strip off the second component.
 *    - apply bnfisprincipal.
 * 1: convert to combination of the generators of the kernel.
 *    - a simple linear system. We expect the existence of an integer
 *      solution.
 * 2: fix to get equality in CK.
 *    - use the previous exponents to form a combination of the elected
 *      (primary) generators of CK.
 *    - add a possible (OK,eps0) component if equality is not reached.
 * 3: convert to Smith generators.
 *    - This uses one of the change of basis matrices from the Smith form
 *      computation.
 *
 * The computation of S is not considerably simpler, although it proceeds
 * through classical group theory operations.
 * 1: Compute generators:
 *    - The type norm implementation is completely sufficient.
 * 2: Get relations.
 *    - Compute the dlogs of the images. Form a matrix.
 *    - Add a diagonal block with the Smith invariants.
 *    - Do an hnf computation to get relations.
 * 3: Get invariants.
 *    - Compute the Smith form of that matrix to get the Smith invariants
 *      of S.
 *    - Keep track of the change of basis matrix for later use.
 * 4: Get coset representatives.
 *    - The hnf computation from step 2 actually produces generators for
 *      the quotient group. These are given by the columns of the change
 *      of basis matrix, on the part where the dlog of the result is not
 *      zero.
*/

/* {{{ Shimura group: basic stuff */
shimura_group_identity_element(cm) =
{
    my (K0);
    K0 = cm[1];
    return ([matid (4), Mod(1, K0.pol)]);
}

shimura_group_arbitrary_lift_of_ideal(cm, idl) =
/* cm: as output by init_cmfield
   idl: an ideal of K (= cm[2])
   returns an arbitrary lift of a as an element of the Shimura group
   */
{
    my (K, K0, Krel, eps0, idlnorm, index, g, good);
    K = cm [2];
    K0 = cm[1];
    Krel = cm [3];
    eps0 = cm [4];
    idlnorm=rnfidealnormrel(Krel,rnfidealabstorel(Krel,idl));
    index=bnfisprincipal(K0,idlnorm, 3);
    g=K0.zk*index[2];
    good=select(x->is_totally_positive(K0,x*g),[1,-1,eps0,-eps0]);
    g*=good[1];
    return ([idl,Mod(g,K0.pol)]);
}

shimura_group_is_element(cm, a) =
/* check what its name suggests */
{
    my (K0,K0narrow,Krel,idl,g,idlnorm);
    K0 = cm[1];
    K0narrow = bnrinit (K0, [1, [1, 1]], 1);
    Krel = cm [3];
    idl=a[1];
    g=a[2];
    idlnorm=rnfidealnormrel(Krel,rnfidealabstorel(Krel,idl));
    return (is_totally_positive(K0, g) && bnrisprincipal(K0narrow, idealdiv(K0,idlnorm,g), 0) == 0);
}

shimura_group_element_mul(cm, a, b) =
{
    my (K,aidl,ag,bidl,bg);
    K=cm[2];
    aidl=a[1];
    ag=a[2];
    bidl=b[1];
    bg=b[2];
    return ([idealmul(K,aidl,bidl),ag*bg]);
}

shimura_group_element_div (cm, a, b) =
{
   return ([idealdiv (cm [2], a [1], b [1]), a [2] / b [2]]);
}

shimura_group_element_pow(cm, a, n) =
{
    my (K0,K,aidl,ax,ag,nx,i,np);
    K0=cm[1];
    K=cm[2];
    if(n==0, return(shimura_group_identity_element(cm)););
    if(type(a)=="t_VEC",
        if(n*sign(n)>100,
            warning("Prefer precomputations using shimura_group_element_powtable for raising to the power ", n);
            error("no, really, it's not serious");
        );
        /* normal code -- we actually compute the power using the
         * generator, and perhaps later we might consider reducing.
         * Note that doing this is inherently dangerous. We might fall
         * short of precision when handling a beast such as ag^n !!
         */
        aidl=a[1];
        ag=a[2];
        ax=[idealpow(K,aidl,n),ag^n];
        if(n*sign(n)>100,
            warning("resulting log-trace of g^n: ", round(log(abs(nfelttrace(K0, ax[2])))));
        );
    ,
        /* Otherwise a is in fact a list of powers. */
        np=#a;
        /* Elements of this list correspond to reduced versions of:
            [1] a^1
            [2] a^2
            [3] a^4
            [i] a^(2^(i-1))
            [np] a^(2^(np-1))
        */
        nx=n*sign(n);
        if (2^np<=nx,error("*** precomputed powers too short", np, " ", nx););
        ax=shimura_group_identity_element(cm);
        for(i=1,np,
            if (bittest(nx, i-1),
                ax=shimura_group_element_mul(cm, ax, a[i]););
        );
        if(n<0,ax=[idealpow(K,ax[1],-1),ax[2]^-1];);
        ax=shimura_group_element_reduce(cm, ax);
    );
    return (ax);
}

shimura_group_element_is_identity (cm, a) =
/* cm: as output by init_cmfield
   returns whether a is the identity of the Shimura group */
{
   my (K0, K, aidl, ag, prin, w, unit);

   K0 = cm [1];
   K = cm [2];
   aidl = a [1];
   ag = a [2];

   /* Test whether aidl is principal and force the computation of a
      generator w if it exists. */
   [prin, w] = bnfisprincipal (K, aidl, 3);
   if (prin, return (0));

   /* Check whether there is a generator v of complex norm ag; this is
      equivalent to ag differing from the complex norm of w by the square
      of a unit, since K0 and K have the same fundamental unit. */
   unit = complex_norm (nfbasistoalg (K, w)) / ag;
   return (bnfisunit (K0, unit) [1] % 2 === 0);
}

shimura_group_element_eq (cm, a, b) =
/* cm: as output by init_cmfield
   returns whether a and b are equal in the Shimura group */
{
   return (shimura_group_element_is_identity (cm,
     shimura_group_element_div (cm, a, b)));
}

quadratic_field_unit_reduce(K0, g, m)=
/* K0: a real quadratic field.
 * g: an element of K0 of type t_POLMOD
 * m: any integer, indicating a power of the fundamental unit in K0.
 */
{
    my (e, ltrace_g, ltrace_gx, g1, g2, x, k);
    e = K0.fu[1];
    e = sign(nfelttrace(K0,e))*e;
    ltrace_g=log(abs(nfelttrace(K0, g)));
    /* Use this very rough approximation only when the trace of the
     * element to be reduced dominates.
     */
    if(ltrace_g > 4*m*log(abs(nfelttrace(K0, e))),
        ltrace_gx=ltrace_g;
        x=0;
        while(abs(ltrace_gx-ltrace_g) < 4,
            if(x,x*=2,x=1);
            ltrace_gx=log(abs(nfelttrace(K0, g*e^(m*x))));
        );
        k=round(x*ltrace_g/(ltrace_gx-ltrace_g));
        g1=(g/e^(m*k));
    , /* else */
        g1=g;
    );
    g1=Mod(g1, K0.pol);
    my (vg,ve);
    vg=vector(2);
    for(i=1,2,
       /* example 35,48 reaches here (in case default precision has been
          * set to 32) */
       while(abs(subst(g1.pol,z,K0.roots[i]))==0,
          op = default(realprecision);
          np = 2*op;
          warning("*** Raising precision: ", op, " -> ", np);
          default(realprecision, np);
          K0 = bnfinit(K0.pol);
       );
       vg[i] = log(abs(subst(g1.pol,z,K0.roots[i])));
    );
    ve=vector(2,i,m*log(abs(subst(e.pol,z,K0.roots[i]))));
    /* We want to approximate the value x such that (g-xe,e)=0 -- which
     * is of course simply x=(g,e)/(e,e) */
    x=round((vg[1]*ve[1]+vg[2]*ve[2])/(ve[1]^2+ve[2]^2));
    g2=g1/e^(m*x);
    return(g2);
}

shimura_group_element_reduce(cm, a) =
/* cm: as output by init_cmfield
   gives an equivalent, but smaller representative of the given class in
   the Shimura group
   */
{
    my (K, Krel, K0, idl, g, ridl, s, t);
    K=cm[2];
    Krel = cm [3];
    K0=cm[1];
    idl=a[1];
    g=a[2];
    ridl = idealred (K, [idl, 1]);
    s = nfbasistoalg (K, ridl [2]);
    t = (s * complex_conjugate_element (s)).pol;
    t = if (poldegree (t) > 0, polcoeff (t, 2) * z, 0) + polcoeff (t, 0);
    g=Mod (g/t, K0.pol);
    g=quadratic_field_unit_reduce(K0, g, 2);
    return ([ridl [1], g]);
}

shimura_group_factorback(cm, f, e) =
/* f and e are lists of the same length (well, only the length of f
 * counts). f is a list of elements of the Shimura group (*), and e
 * contains exponents.
 *
 * (*) As an alternate syntax, and as per the special handling in
 * shimura_group_element_pow, f may also be a list of lists of powers of
 * elements of the Shimura group. Such lists may be precomputed using
 * shimura_group_element_powtable. This allows dramatically faster
 * computations for large components in the class group. OTOH, it's
 * useful only for repeated calls to the factorback routine with the same
 * set of generators.
 */
{
    my (n,x,y);
    n=#f;
    x=shimura_group_identity_element(cm);
    for(i=1,n,
            y=shimura_group_element_pow(cm,f[i],e[i]);
            x=shimura_group_element_mul(cm,x,y);
    );
    return (x);
}

shimura_group_element_powtable(cm, a, n) =
/* given an element, and maximum power n, precompute reduced
 * representatives for a^(2^k) for all k
 * such that 2^k <= n ; this used for later computation of powers in a
 * reduced fashion.
 */
{
    my (ax, k);
    ax=List();
    k=1;
    listput(ax,a);
    while(2*k<=n,
        k*=2;
        a=shimura_group_element_mul(cm,a,a);
        a=shimura_group_element_reduce(cm,a);
        listput(ax,a););
    return(ax);
}

/* }}} */

shimura_group_kernel_of_norm_map(cm) =/*{{{*/
/* This is a helper for the next function */
/* First task: compute the kernel of the norm map from the class group of
 * K to the narrow class group of K0.
 * We compute three representing elements for this kernel.
 *  - mapker_gens_abs: abstract coordinate vectors representing the
 *  generators of the kernel as elements of the class group.
 *  - mapker_gens: precomputed ideals corresponding to these generators
 *  - mapker_smith: elementary divisors, which also give the relation
 *  matrix for the kernel.
 *
 * Computing the full smith form is quite probably excessive, but it
 * seems that some optimizations we make downhill are only valid if we've
 * payed attention to doing the full stuff here.
 */
{
    my (K, K0, Krel, n);
    my (K0narrow, nn0, i, j, idl, ridl, idlnorm);
    my (m, v, r, md, mu, mv, mui, ms);
    my (krn, krn1, krel);
    my (mapker_smith, mapker_gens_abs, mapker_gens);

    K0=cm[1];
    K=cm[2];
    Krel = cm [3];
    n=#K.clgp.cyc;

    K0narrow = bnrinit (K0, [1, [1, 1]], 1);
    nn0=#K0narrow.clgp.cyc;
    m=matrix(nn0,n+nn0);
    for(i=1,nn0,m[i,n+i]=K0narrow.clgp.cyc[i];);
    for(i=1,n,
        idl=K.clgp.gen[i];
        ridl=rnfidealabstorel(Krel,idl);
        idlnorm=rnfidealnormrel(Krel,ridl);
        v=bnrisprincipal(K0narrow,idlnorm);
        for(j=1,nn0,m[j,i]=v[1][j];);
    );
    /* Take the part of the hnf which creates the zeroes */
    v=mathnf(m,1)[2];
    /* Notice that the coefficients of combinations appear in columns */
    krn=matrix(n,n,i,j,v[i,j] % K.clgp.cyc[i]);
    /* Keep the matrix invertible as an integer matrix */
    for(i=1,#K.clgp.cyc,if(krn[i,i]==0,krn[i,i]=K.clgp.cyc[i];););
    /* Now all ideals in kgens are known to map via the norm to the
     * trivial class in the narrow class group.
     * It's not exactly sufficient, because we'd like to know the
     * relations among these elements in the ambient group Cl(K). Again
     * a hnf computation
     */
    m=concat(krn,matdiagonal(K.clgp.cyc));
    v=mathnf(m,1)[2];
    krel=matrix(n,n,i,j,v[i,j]);
    /* present our kernel in smith form */
    ms=matsnf(krel,1);
    mu=ms[1];
    mv=ms[2];
    md=ms[3];
    /* we have mu*krel*mv==md */
    r=#select(i->md[i,i]!=1,vector(matsize(md)[1],i,i));
    mapker_smith=vector(r,i,md[i,i]);
    /* within the torsion-free Z^n of which mapker is a quotient,
     * let h1,...,hn be the hnf generators and s1...sn the snf
     * generators. An element which is written \sum a_i h_i may also be
     * written \sum b_i s_i, with b = mu * a. Whence we can obtain the
     * (short) smith generators by inverting mu.
     */
    mui=mu^-1;
    /* we have kgens * mui = the generators, whose orders are in md
     * we'll only pick the r first generators */
    krn1=krn*mui;
    mapker_gens_abs=matrix(n,r,i,j,krn1[i,j] % K.clgp.cyc[i]);
    /* The columns of krn are columns of n coefficients, whose
     * coordinates correspond to multiplies of the generators of K.clgp.
     * Each column is one of the elementary generators of the kernel of
     * the map Cl(OK) \to Cl+(OK0). The orders of elements are given by
     * the diagonal relation matrix krel
     */
    mapker_gens=vector(r,j,idealred(K,idealfactorback(K,K.clgp.gen,mapker_gens_abs[,j])));
    return ([mapker_gens, mapker_gens_abs, mapker_smith]);
}
/*}}}*/

shimura_group_structure(cm) = /* {{{ */
/* cm: as output by init_cmfield
 * This returns information identifying the structure of the Shimura
 * group. Namely, the following properties of the group are returned:
 * [1] Smith invariants d1, ..., dk, with 1|dk|d_{k-1}|...|d1
 * [2] generators a1,...,ak, with order(ai)=di
 * The rest is auxiliary data used for conversions.
 * [3] generators b1,...,bn (with n>=k) (see also below).
 * [4] conversion matrix P which gives the bnfisprincipal results
 *     for the b1[1]...bn[1] (in columns). More precisely, arbitrary
 *     lifts for the class group generators having been chosen, this
 *     matrix indicates how a given element of the class group may
 *     be lifted into she Shimura group in a way which is consistent
 *     with these arbitrary choices. If q is such that P*q = x, then
 *     an ideal for which bnfprincipal(I)==x may be consistently
 *     lifted to (b1...bn)*q.
 * [5] unimodular conversion matrix U such that
 *     (a1...ar,1..1)=(b1..bn)*(U^-1)
 */
/* recall the exact sequence:
 *
 * 1 \to U0+/N_{K/K0}(U) \to CK \to Cl(OK) \to Cl+(OK0) \to 1
 *
 * We first compute the kernel (generators, relations) of the map
 *  Cl(OK) \to Cl+(OK0)
 */
{
    my (K,K0,eps0,m,v,CKsmith);
    my (mapker_lifts,mapker_liftsx,id,mo,ms,mu,mv,md,r,CKgens,mui);
    my (i,j, tmp);
    my (mapker_gens, mapker_gens_abs, mapker_smith);

    K0=cm[1];
    K=cm[2];
    eps0 = cm [4];

    tmp = shimura_group_kernel_of_norm_map(cm);
    mapker_gens = tmp[1];
    mapker_gens_abs = tmp[2];
    mapker_smith = tmp[3];

    if(matsize(mapker_gens_abs) != [#K.clgp.cyc, #mapker_smith], error("size check"););
    if(#mapker_gens != #mapker_smith, error("size check"););


    /* Now we're done for the kernel of the map Cl(OK) \to Cl+(OK0),
     * which is also the image of the map C(K) -> Cl(OK).
     *
     * Next comes the task of lifting these generators and relations
     * arbitrarily, and recognize the lifts of relations (hence elements
     * of the kernel of the map C(K) -> Cl(OK)) as images of the map of
     * the unit part to C(K).
     */

    /* mapker_lifts are lifts to the shimura class group of the
     * generators in mapker_gens */

    mapker_lifts=vector(#mapker_smith,i,shimura_group_arbitrary_lift_of_ideal(cm,mapker_gens[i]));

    /* Write a relation matrix by lifting the relation matrix from the
     * next map */
    if(nfeltnorm(K0,eps0)<0,
        /* This is rather trivial if the unit part is trivial, of course. */
        m=matdiagonal(mapker_smith);
    ,
        /* otherwise there's more info to put in */
        m=matdiagonal(concat(mapker_smith, [2]));
        mapker_lifts=concat(mapker_lifts,[[idealhnf(K,1),eps0]]);
    );
    /* Now fix each relation ! We need some equivalent of factorback on
     * the Shimura group. Cf related function. */
    /* First, we need to precompute tables of each of these. The
     * precomputation needs only go up to the order of the generators. */
    mapker_liftsx=vector(#mapker_lifts,i,shimura_group_element_powtable(cm, mapker_lifts[i], m[i,i]));
    id=shimura_group_identity_element(cm);
    for(j=1,#mapker_smith,
        v=shimura_group_factorback(cm,mapker_liftsx,m[,j]);
        /* There isn't so much choice, so either we have 1, or we don't */
        if(!shimura_group_element_eq(cm,v,id),
            if(nfeltnorm(K0,eps0)<0,error("argh"););
            m[#mapker_smith+1,j]=1;
        );
    );
    ms=matsnf(m,1);
    mu=ms[1];
    mv=ms[2];
    md=ms[3];
    /* mu * m * mv = matdiagonal(md) */
    r=#select(i->md[i,i]!=1,vector(matsize(md)[1],i,i));
    CKsmith=vector(r,i,md[i,i]);
    /* There's an additional trick. The entries in mu^-1 may exceed the
     * orders of the elements in mapker_liftsx. We have C^m=0, thus
     * C^(u^-1*u*m)=C^(u^-1)^d=0, but u^-1 isn't necessarily reduced mod
     * m. So we should consider whether there's an equivalent set of
     * generators C^(u^-1-m*z), with the entries in u^-1+m*z properly
     * reduced. So we need to solve m*z=u^-1
     */
    /* We previously contented ourselves with mapker being presented in
     * hnf form only, not snf. The code below might perhaps be optimized
     * now that mapker is presented in snf form.
     */
    mui=mu^-1;
    /* the next four lines are just optimization */
    my (kk);
    kk=matsolve(m,mu^-1);
    kk=matrix(#m,#m,i,j,round(kk[i,j]));
    mui=mu^-1-m*kk;     /* apply */

    CKgens=vector(r,j,shimura_group_factorback(cm,mapker_liftsx,mui[,j]));

    /* now remove the last element in mapker_liftsx, we don't need it. */
    mapker_liftsx=vector(#mapker_smith,i,mapker_liftsx[i]);
    /* Do we need this ?
    CKgensx=vector(#CKgens,i,shimura_group_element_powtable(cm, CKgens[i], CKsmith[i]));
    */
    return([CKsmith,CKgens,mapker_liftsx,mapker_gens_abs,mapker_smith,mu]);
}

/* }}} */

/* {{{ Shimura group: complex conjugation action */
shimura_group_element_conjugate(cm, a)=
/* return the complex conjugate (abar, x) of (a,x) */
{
    my (K);
    K = cm [2];
    return([complex_conjugate_ideal(K, a[1]),a[2]]);
}

shimura_group_precompute_conjugation_action(cm)=
/* return a vector indicating the coefficients of the complex conjugation
 * action on the smith generators of the CKdata group. Note that since
 * complex conjugation commutes with group morphisms here, we know that its
 * matrix is diagonal. As in other cases, we're not returning the members
 * as INTMOD's, but it's trivially fixed by the caller if needed.
 */
{
    my(CKdata,CKgens,CKsmith,n,m);
    CKdata = cm [18];
    CKgens=CKdata[2];
    CKsmith=CKdata[1];
    n=#CKsmith;
    m=matrix(n,n);
    for(i=1,n,
        my (a,v);
        a=shimura_group_element_conjugate(cm,CKgens[i]);
        v=shimura_group_element_dlog(cm,a);
        for(j=1,n,m[i,j]=v[j];);
     );
     if(!matisdiagonal(m),error("argh"););
     return(vector(n,i,m[i,i]));
}
/* }}} */

shimura_group_element_dlog(cm, a) = /* {{{ */
/* given a cm and the corresponding Shimura group info as computed
 * by the above function, returns the decomposition of a w.r.t the
 * elementary divisors of the Shimura group structure.
 */
{
    my (K, CKsmith, m,mapker_liftsx,CKgens);
    my (mapker_gens_abs, mapker_smith);
    my (q,i,n,r,r1,ax,qs,qx,CKdata);
    my (m,j,tmp,mh,mh0);
    /* first step: write the ideal part with respect to the basis of the
     * kernel of the norm map to the narrow class group */
    CKdata = cm [18];
    K = cm [2];
    CKsmith=CKdata[1];
    CKgens=CKdata[2];
    mapker_liftsx=CKdata[3];
    mapker_gens_abs=CKdata[4];
    mapker_smith=CKdata[5];
    n=#K.clgp.cyc;
    r=#mapker_smith;
    r1=#CKsmith;

    if(matsize(mapker_gens_abs) != [n, r], error("size check"););
    if(#mapker_liftsx != r, error("size check"););

    /* ok, I'm going very awkward routes for doing the membership test.
     * Presently I can't come up with a nice & quick way of handling all
     * of the following cases correctly. There should be one, though. All
     * I want is to project from an ambient Z-module to a quotient...
     * 82,792 235,8448 33,267 234,11
     * 2 hnfs + 1 multi-gcd for one membership test seem a bit expensive.
     */
    /* work around gp-2.5 not having matconcat */
    m=matrix(n,1+n+r);
    tmp=bnfisprincipal(K,a[1], 0);
    for(i=1,n,m[i,1]=tmp[i]);
    for(i=1,n,for(j=1,r,m[i,1+j]=mapker_gens_abs[i,j]););
    for(i=1,n,m[i,i+1+r]=K.clgp.cyc[i];);
    /* m=matconcat([bnfisprincipal(K,a[1])[1],mapker_gens_abs,matdiagonal(K.clgp.cyc)]);
     */
    my (tmp);
    tmp=mathnf(m,1);
    if(matdet(tmp[1])*prod(i=1,r,mapker_smith[i]) != K.clgp.no, error("Failed dlog computation"););
    mh=matrix(r+1,r+1,i,j,tmp[2][i,j]);
    mh0=matrix(1,r+1,i,j,mh[i,j]);
    tmp=mathnf(mh0,1);
    if(tmp[1]!=1,error("argh");); /* should never happen */
    q=(mh*tmp[2][,r+1]);
    q=vector(r,i,q[1+i]);
    qx=vector(r);
    for(i=1,r,qx[i]=(-q[i]) % mapker_smith[i];);
    /* recreate the ideal, using mapker_liftsx */
    ax=shimura_group_factorback(cm,mapker_liftsx,qx);
    /* Have we recovered a ? */
    tmp = shimura_group_element_eq(cm,a,ax);
    if(#CKdata[6]==r+1,
        qx=concat(qx,[!tmp]);
    ,
        if(!tmp,error("Unexpected error, we should have recovered a"););
    );
    /* we have written our element of CK as a product of the primary
     * generators. Now let's modify the expression so as to have
     * something relative to the true generators of CK.
     */
    qs=CKdata[6]*Col(qx);
    return(vector(#CKsmith,i,qs[i] % CKsmith[i]));
}

/* }}} */

shimura_group_type_norm_subgroup(cm) = /* {{{ */
/* computes the subgroup of the Shimura subgroup which is spanned by the
 * image of the class group of the reflex CM field under the reflex type
 * norm. Returns:
 * [1] the smith invariants of that group,
 * [2] corresponding generators (as members of the reflex class group),
 * [3] generators of cosets in the ambient group,
 * [4] dlog vectors for the subgroup generators as combination of the
 *     ambient group generators.
 * The quotient being a 2-group, all the coset generators have order 2
 * (i.e., if x coset generators are returned, there are 2^x cosets in
 * total).
 */
{
    my (CKsmith, CKgens, T, CKdata, r, Kr, n, m0, m, v, u, tmp, h, pick);
    my (ms, mu, mv, md);
    my (Hr, Hsmith_invs, mui, Sgens, coset_gens);
    CKdata = cm[18];
    CKsmith=CKdata[1];
    CKgens=CKdata[2];
    r = #CKsmith;
    Kr = cm [7];
    n = #Kr.clgp.cyc;
    m0 = matrix(r,n);
    T=vector(n,j,type_norm(cm,Kr.clgp.gen[j]));
    for(j=1,n, v=shimura_group_element_dlog(cm,T[j]); for(i=1,r,m0[i,j]=v[i];););
    m=concat(m0, matdiagonal(CKsmith));
    /* These form the subgroup generators. We need to know the relations
     * between these generators: hnf. We're only interested in the
     * transformation matrix. The hnf itself gives the quotient, but
     * we're not too interested at this point (except if we want to
     * verify that it's a 2-group). */
    my(tmp);
    tmp=mathnf(m,1);
    h=tmp[1];
    u=tmp[2];
    /* Still, the info on the coset representatives is there to pick, so
     * there's really no reason not to. We have CKgens^m = [primary
     * generators of S, and 0], thus CKgens^(m*u) = CKgens^h. The coset
     * generators are thus the members of the CKgens array which get
     * raised to a non trivial power in order to belong to S.
     */
    pick=select(i->h[i,i]==2,vector(r,i,i));
    coset_gens=vector(#pick,j,CKgens[pick[j]]);

    u=matrix(n,n,i,j,u[i,j]);
    /* Now this square matrix is the relation matrix among my generators.
     * the Smith form eventually gives me the structure. */
    ms=matsnf(u,1);
    mu=ms[1];
    mv=ms[2];
    md=ms[3];
    Hr=#select(i->md[i,i]!=1,vector(matsize(md)[1],i,i));
    Hsmith_invs=vector(Hr,i,md[i,i]);
    mui=mu^-1;
    /* We give the generators as ideals of the reflex class group. */
    Sgens=vector(Hr,j,idealfactorback(Kr,Kr.gen,mui[,j]));
    /* We may quite easily provide the matrix which gives the subgroup
     * generators as combinations of the ambient group generators (such
     * that CKgens^m = Sgens */
    my (tmat);
    tmat=matrix(r,n,i,j,m[i,j])*mui;
    tmat=matrix(r,Hr,i,j,tmat[i,j] % CKsmith[i]);
    return ([Hsmith_invs, Sgens, coset_gens, tmat]);
}

/* }}} */

/* {{{ Shimura group: miscellaneous */

shimura_group_allproducts(cm, g, m) =
/* cm: as output by init_cmfield
   g: a list of elements of the Shimura group.
   m: a list of exponents.
   returns the list of all \prod g[j]^x[j], where 0<=x[j]<m[j]
 */
{
    my (L,i);
    L=List();
    listput(L,shimura_group_identity_element(cm));
    for(i=1,#g,
            my(Lx,a,n,j,k,p);
            Lx=L;
            a=g[i];
            n=m[i];
            k=1;
            while(k<n,
                for(j=1,#L,
                    p=shimura_group_element_mul(cm, L[j], a);
                    p=shimura_group_element_reduce(cm, p);
                    listput(Lx,p);
                    /* write1("/dev/stderr","."); */
                );
                k+=1;
                a=shimura_group_element_mul(cm, a, g[i]);
                a=shimura_group_element_reduce(cm, a);
                );
            /* write1("/dev/stderr","\n"); */
            L=Lx;
    );
    return (L);
}
/* }}} */

/* }}} */

sufficiently_accurate_bnfinit(pol, flag) = {
    my (K0, op, np);
   default (realprecision, floor (sqrt (poldisc(pol))));
   K0 = bnfinit (pol, 1);
   /* make sure eps0 can be properly mapped to complexes */
   /* There's something fishy with 103,60. The present loop causes it to
    * reach precision 1600, which looks very odd */
   for(i=1,2,
       while(abs(subst((K0.fu[1]).pol,z,K0.roots[i]))==0,
           my (op,np);
           op = default(realprecision);
           np = 2*op;
           warning("*** Raising precision: ", op, " -> ", np);
           default(realprecision, np);
           K0 = bnfinit(pol, 1);
       );
   );
   return(K0);
}

abelian_group_name(invs) =
{
   my (st);
   if(#invs==0,return("C1"););
   st="";
   for(i=1,#invs,
    if(length(st)>0,
    st=concat(concat(st," x C"), invs[i]);
   ,
    st=concat("C", invs[i]);
    );
   );
   return(st);
}

/* {{{ CM field basics */
init_cmfield (A, B) =
/* works as init_cmfield_basic, but adds the following to the output vector:
   18 CKdata: abstract data for working with the Shimura group
   19 Sdata: abstract data for working with the subgroup of Shimura
      group which is reached by the reflex typenorm map.
 */
{
   my (cm, CKdata, Sdata);
   cm = init_cmfield_basic(A, B);
   if(cm[11]==0,
     print("Galois group = C4");
   ,
     print("Galois group = D4");
   );
   gettime();
   /* create the preliminary list */
   print("Class group of K = ", abelian_group_name(cm[2].clgp.cyc));
   print("Class group of Kr = ", abelian_group_name(cm[7].clgp.cyc));
   CKdata=shimura_group_structure(cm);
   print("Shimura group = ", abelian_group_name(CKdata[1]));
   cm = concat (cm, [CKdata]);
   Sdata=shimura_group_type_norm_subgroup(cm);
   print("Type norm subgroup = ", abelian_group_name(Sdata[1]));
   cm = concat (cm, [Sdata]);

   return (cm);
}
/* }}} */


/* {{{ Enumeration of CM orbits */
reduce_basepoint(cm, bp)=
{
    my (K0, K, Krel, Phi, k, unitindex, xi, scale);
    K0 = cm [1];
    K = cm [2];
    Krel = cm [3];
    unitindex = cm [5];
    Phi = cm [9];
    xi=bp[2];
    k = 2/unitindex;
    scale =rnfeltdown(Krel,Mod(xi/Phi,K.pol));
    scale = quadratic_field_unit_reduce(K0,scale, k);
    xi=(Phi*rnfeltup(Krel,scale)).pol;
    return([bp[1], xi]);
}

triple_new(cm, bp, sh, flag) =
/* cm: as output by init_cmfield
 * bp: a base point for the action of the Shimura group as found by
 *    find_basepoint
 * sh: an element of the Shimura group
 * returns a triple [a, xi, flag] = sh*bp
 *
 * Watch out for a typo in BrGrLa11: The action of sh on bp is by
 * multiplying the first components, but _dividing_ the second ones.
 */
{
   my (K, Krel, a, xi);
   K = cm [2];
   Krel = cm [3];
   a = idealmul (K, bp [1], sh [1]);
   xi = (bp [2] / rnfeltup (Krel, sh [2])).pol; /* .pol to remove the modulus */
   return ([a, xi, flag]);
}

triples (cm) =
/* cm a quartic CM field
   returns the triples [ideal, norm, flag] determining the CM abelian
     surfaces as a vector (with one entry for each coset) of lists */

{
   my (A, B, K0r, Arred, Brred, K0, K, unitindex, Phi,
       D, cosets, m, noC, bp, trip, basis, den, flag);

   K0 = cm [1];
   K0r = cm [6];
   A = polcoeff (K0.pol, 1);
   B = polcoeff (K0.pol, 0);
   Arred = cm [14];
   Brred = cm [15];
   K = cm [2];
   unitindex = cm [5];
   Phi = cm [9];
   noC = cm [10];
   D = K0.disc;

   my (CKdata,CKgens,Sdata,coset_gens,Sgens,bases,Sgensx,X,CKsmith,xL,lambda,
      idx);
   CKdata = cm [18];
   CKgens = CKdata[2];
   Sdata = cm [19];
   coset_gens = Sdata[3];
   /* We know that the quotient of the Shimura group by the type norm
    * subgroup is a 2-group */
   X = shimura_group_allproducts(cm, coset_gens, vector(#coset_gens,i,2));

   CKsmith=CKdata[1];
   xL = explode_abstractgroup(Sdata[1]);

   lambda = shimura_group_precompute_conjugation_action(cm);

   bp = find_basepoint (cm);
   bp = reduce_basepoint (cm, bp);

   my (coset_lists,steps,steps_values);
   coset_lists=List();
   steps=Set();

   for (l = 1, #X,
      my (s,h,xh);
      /* The triples in this coset are of the form:
       * (bp[1],bp[2]) . (s[1],s[2]) * (a[1],a[2])
       * where a runs through the subgroup S of the Shimura group.
       * Thus the ideal part of a given element is (dropping [1]'s):
       * (bp*s*a). The conjugate may thus be written as:
       * (bp*s*(bpbar*sbar/bp/s)*abar).
       *
       * Thus given an abstract representation xa of a, and given a
       * (matrix) multiplier lambda which corresponds to the complex
       * conjugation ; given again an abstract representation xh for the
       * Shimura group element (bpbar*sbar/bp/s,1), we need thus consider
       * only one representative of each equivalence class of S modulo
       * the relation: xa <--> xh + lambda * xa
       *
       * Points which are stable under xa <--> xh + lambda * xa
       * correspond to real roots of the class polynomial. These receive
       * the ``flag'' value of 1.
       *
       * Other points correspond to conjugate complex roots. Naturally we
       * keep only one representative of such pair, and the ``flag''
       * value is 2.
       */
      /* So we need first to compute xh. Not too difficult. */
      s=X[l];
      h=idealmul(K, bp[1], s[1]);
      h=idealdiv(K, complex_conjugate_ideal(K, h), h);
      h=[h, Mod(1, K0.pol)];
      xh=shimura_group_element_dlog(cm, h);
      /* next, iterate through all abstract representations of the
       * subgroup elements. Look for those for which the conjugate is
       * also present.
       */
      my (S,pick,xa_S,xa,xa2);
      S=Set();
      pick=List();
      for(i=1,#xL,
        xa_S = xL[i]~;
        /* xL[i] is a vector of exponents w.r.t the generators of the
         * subgroup S. Here, we want them as members of CK. The
         * conversion matrix is Sdata[4] */
        xa = xa_S * mattranspose(Sdata[4]);
        if (#xa_S,
            xa=vector(#CKsmith,k,Mod(xa[k],CKsmith[k]));
        , /* else */
            xa=vector(#CKsmith); /* degenerate case */
        );
        xa2=vector(#xa,i,xh[i] + xa[i]*lambda[i]);
        if(xa==xa2,     /* a real root */
            /* write1("/dev/stderr", "r"); */
            listput(pick, [lift(xa_S), 1]);
            S = setunion (S, Set ([xa]));       /* in fact useless */
        , /* else: */   /* complex conjugate pair */
            if (!setsearch (S, xa),
                /* write1("/dev/stderr", "c"); */    /* not yet met */
                S = setunion (S, Set ([xa, xa2]));
                listput(pick, [lift(xa_S), 2]);
            ,
                /* write1("/dev/stderr", "-"); */     /* already met */
            );
        );
      );
      /* write1("/dev/stderr", "\n"); */
      /* Last thing. It's easy to enumerate members of a group when they
       * come in increasing lexicographical order. Here, we have gaps.
       * Therefore we precompute the steps. */
      steps=setunion(steps, Set(vector(#pick-1,i,pick[i+1][1]-pick[i][1])));
      steps=setunion(steps, Set([pick[1][1]]));
      listput(coset_lists, pick);
    );
   Sgens=vector(#Sdata[2],i,type_norm(cm,Sdata[2][i]));
   Sgensx=vector(#Sgens,i,shimura_group_element_powtable(cm, Sgens[i], Sdata[1][i]));
   steps_values=vector(#steps,i,shimura_group_factorback(cm, Sgensx, eval(steps[i])));

   bases=vector(#coset_lists);

   /* starting output */
   for (l = 1, #coset_lists,
      my (C, b, a, p);
      C=coset_lists[l];
      b=vector(#Sdata[1]);
      a = X[l]; /* starts this coset */
      bases[l] = List();
      for (k=1, #C,
         p = C[k][1] - b;
         flag = C[k][2];
         if(p != 0,
             idx = setsearch(steps, p);
             if (!idx, error("argh: ", p, " not found ??"););
             a = shimura_group_element_mul(cm, a, steps_values[idx]);
             a = shimura_group_element_reduce(cm, a);
             b = C[k][1];
         );
         trip = triple_new (cm, bp, a, flag);
         listput(bases[l], trip);
      );
   );
   return(bases);
}

/* }}} */
